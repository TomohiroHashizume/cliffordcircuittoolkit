#include <chrono>
#include <cmath>
#include <fstream>
#include <math.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include <cstdlib>
#include <vector>

namespace MonnaMap {
   std::vector<int> b_nary(int i,int b,int maxpow);
   int inverse_b_nary(std::vector<int> vec,int b,int maxpow);
   int intlog(int num,int b);
   std::vector<int> monnamap(int L,int b);
}
