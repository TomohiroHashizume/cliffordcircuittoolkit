#!/bin/bash
#SBATCH -n 1
#SBATCH --mem=3000M
#SBATCH --partition="qoqms_intel"
#SBATCH --time=5-00:00:00
#SBATCH --job-name=make_state
#SBATCH --get-user-env
#SBATCH --array=1-10%10
#SBATCH --output=./IO/%x_%A_%a.out
#SBATCH --error=./IO/%x_%A_%a.err

source ~/.bashrc
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1

inttype=${1}
LogL=${2}
ss_tag=${3}
init_state=${4}

duration=${5}
testmode=${6}

L=$(( 2 ** ${LogL} ))
savedir="./data/${inttype}/${init_state}/${L}/${ss_tag}/"
dbname="${savedir}states.db"

if [ ! -d ${savedir} ]; then
   mkdir -p ${savedir}
fi

upto=1
for (( traj=0; traj<${upto};traj++))
do
   fname="${savedir}${traj}.out"
   if [ ! -f ${fname}_l ]; then
      echo ./evolve ${dbname} ${fname} ${inttype} ${L} ${ss_tag} ${init_state} ${traj} ${duration}
      ./evolve ${dbname} ${fname} ${inttype} ${L} ${ss_tag} ${init_state} ${traj} ${duration}
   fi
done
