#include <chrono>
#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>

#include <sqlite3.h>
#include "EvolveClifford_evolve.h"

int main(int argc,char *argv[])
{
   std::string dirname="./out/";
   std::string inttype="PWR2_ss_powerlaw";
   int L = std::atoi(argv[1]);
   int s_tag=20000;
   std::string init="Z";

   int duration=1000;

   int proj_ptag=0;
   double proj_p=proj_ptag/10000.;
   double ss=s_tag/10000.;

   int seed=0;
   int verbose=1;


   int numtimesteps=0;
   numtimesteps=duration; // technically (2*(CliffordGates::int_log2(L)-1)+1)

   std::cout << "Number of timesteps is " << numtimesteps << std::endl;

   long int maxnumgates=1e10;
   int boundary=L;
   bool deterministic=false;
   bool track=false;
   bool save=false;

   auto start_time = std::chrono::high_resolution_clock::now();

   std::cout << "Now computing.. " << std::endl;
   int int_log2L=CliffordGates::int_log2(L);
   for(int ind=0;ind<10;ind++)
   {
      int traj=std::rand();
      std::string fname=dirname+std::to_string(ind)+".out";
      CliffordGates::EvolveClifford_evolve evolve(L,init,ind,verbose);
      evolve.set_ss_exp(ss);
      evolve.evolve(fname,inttype,numtimesteps,maxnumgates,proj_p,boundary,deterministic,track);
      evolve.export_state(fname);
   }

   auto current_time = std::chrono::high_resolution_clock::now();
   std::cout << "Program terminated after " << 
      std::chrono::duration_cast<std::chrono::seconds>(current_time - start_time).count() << " seconds" << std::endl;
}
