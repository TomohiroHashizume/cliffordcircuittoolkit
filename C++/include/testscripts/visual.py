import matplotlib.pyplot as plt

f=open('./out/0.out')
#f=open('./file.out')
mat=[]
for l in f :
   mat.append(list(map(int,l[:-1].split(','))))

f.close()
plt.figure(figsize=(10,10))
plt.imshow(mat)
plt.savefig('out.png')
plt.show()
