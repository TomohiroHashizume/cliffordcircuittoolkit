import numpy as np
import sys 
sys.path.append('../../../../pythonmodules')
from clifford_modules import *

for traj in range(100) :
   f=open('./out/{}.out'.format(traj))
#f=open('./file.out')
   mat=[]
   for l in f :
      mat.append(list(map(int,l[:-1].split(','))))

   mat=np.array(mat)
   L=mat.shape[0]//2
   mat=mat[L:,:]
   sub=np.arange(L//2,dtype=int)
   print(sub)
   mat=get_subsys_stab(mat,sub)
   print(get_rank(mat)-len(sub))
