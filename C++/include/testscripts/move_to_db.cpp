#include <chrono>
#include <iostream>
#include <fstream> 
#include <stdio.h>
#include <string>
#include <vector>
#include <sqlite3.h>

#include "EvolveClifford.h"
#include "EvolveClifford_evolve.h"

int callback(void *NotUsed, int argc, char **argv, char **azColName){
   return 0;
}

int db_build(std:: string dbname)
{
   sqlite3 *db;
   int rc; 
   char *zErrMsg = 0;

   std::string sql;
   rc = sqlite3_open_v2(dbname.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX , nullptr);

   /* Create SQL statement */
   sql = "CREATE TABLE IF NOT EXISTS STATES("  \
   "L                  INTEGER     NOT NULL,"  \
   "SS_TAG             INTEGER     NOT NULL,"  \
   "INTTYPE            TEXT        NOT NULL,"  \
   "INIT_STATE         TEXT        NOT NULL,"  \
   "TRAJ               INTEGER     NOT NULL,"  \
   "STATE_L            TEXT        NOT NULL,"  \
   "STATE_P            TEXT        NOT NULL);";

   /* Execute SQL statement */
   rc = sqlite3_exec(db, sql.c_str(),callback,0,&zErrMsg);
   sqlite3_close_v2(db);
   return 0;
}

bool check_exist(
      std:: string dbname, 
      int L, int ss_tag, std::string inttype, 
      std::string init_state, int traj)
{
   sqlite3 *db;
   int rc; 
   char *zErrMsg = 0;

   std::string sql;
   sqlite3_stmt* pStmt=NULL;

   rc = sqlite3_open_v2(dbname.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX , nullptr);
   int iResult = SQLITE_ERROR;

   /* Create SQL statement */
   sql = "SELECT L FROM STATES WHERE "  \
   "L=?                AND "  \
   "SS_TAG=?           AND "  \
   "INTTYPE=?          AND "  \
   "INIT_STATE=?       AND "  \
   "TRAJ=?;";

   iResult = sqlite3_prepare_v2(db,sql.c_str(), -1, &pStmt, nullptr);

   sqlite3_bind_int64(pStmt,1,L);
   sqlite3_bind_int64(pStmt,2,ss_tag);
   sqlite3_bind_text(pStmt,3,inttype.c_str(),inttype.size(),SQLITE_STATIC);
   sqlite3_bind_text(pStmt,4,init_state.c_str(),init_state.size(),SQLITE_STATIC);
   sqlite3_bind_int64(pStmt,5,traj);

   bool out=false;
   if (sqlite3_step(pStmt) == SQLITE_ROW )
   {
      // std::string state_str= (const char *) sqlite3_column_blob(pStmt, 0);
      // std::cout << state_str << std::endl;
      out=true;
   }
   else 
   {
      out=false;
   }

   sqlite3_finalize(pStmt);
   sqlite3_close_v2(db);
   return out;
}

void add_to_table(
      std:: string dbname, 
      int L, int ss_tag, std::string inttype, 
      std::string init_state, int traj, std::string state_l, std::string state_p)
{
   sqlite3 *db;
   int rc; 
   char *zErrMsg = 0;

   std::string sql;
   sqlite3_stmt* pStmt=NULL;

   rc = sqlite3_open_v2(dbname.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX , nullptr);
   int iResult = SQLITE_ERROR;

   /* Create SQL statement */
   sql = "INSERT INTO STATES(L,SS_TAG,INTTYPE,INIT_STATE,TRAJ,STATE_L,STATE_P) VALUES(?,?,?,?,?,?,?);";
   iResult = sqlite3_prepare_v2(db,sql.c_str(), -1, &pStmt, nullptr);

   sqlite3_bind_int64(pStmt,1,L);
   sqlite3_bind_int64(pStmt,2,ss_tag);
   sqlite3_bind_text(pStmt,3,inttype.c_str(),inttype.size(),SQLITE_STATIC);
   sqlite3_bind_text(pStmt,4,init_state.c_str(),init_state.size(),SQLITE_STATIC);
   sqlite3_bind_int64(pStmt,5,traj);
   sqlite3_bind_text(pStmt,6,state_l.c_str(),state_l.size(),SQLITE_STATIC);
   sqlite3_bind_text(pStmt,7,state_p.c_str(),state_p.size(),SQLITE_STATIC);

   while(sqlite3_step(pStmt) == SQLITE_BUSY){}

   std::cout << "state added" << std::endl;

   // sqlite3_reset(pStmt);
   // sqlite3_clear_bindings(pStmt);

   sqlite3_finalize(pStmt);
   sqlite3_close_v2(db);
}


int main(int argc,char *argv[])
{

   if (argc != 8)
   {
      throw 3;
   }

   std::cout << "SQLite3 version = " << sqlite3_version << std::endl;
   std::string dbname=argv[1];
   std::string fname=argv[2];
   std::string inttype=argv[3];

   int L = std::atoi(argv[4]);
   int ss_tag=std::atoi(argv[5]);
   std::string init=argv[6];

   int traj=std::atoi(argv[7]);

   auto start_time = std::chrono::high_resolution_clock::now();

   // data base initialization 
   db_build(dbname);

   if ( ! check_exist(
      dbname, 
      L, ss_tag, inttype, init, traj) )
   {
      std::ifstream in_l(fname+"_l");
      if (in_l) 
      {
         in_l.seekg(0, std::ios::end);
         size_t len_l = in_l.tellg();
         in_l.seekg(0);
         std::string out_state_l(len_l + 1, '\0');
         in_l.read(&out_state_l[0], len_l);

         std::ifstream in_p(fname+"_p");
         if (in_p)
         {
            in_p.seekg(0, std::ios::end);
            size_t len_p = in_p.tellg();
            in_p.seekg(0);
            std::string out_state_p(len_p + 1, '\0');
            in_p.read(&out_state_p[0], len_p);

            add_to_table(
            dbname, 
            L, ss_tag, inttype, init, traj, out_state_l, out_state_p);
         }

         std::cout << "State Added to database" << std::endl;
      }
   }

   // Pass to database 
   auto current_time = std::chrono::high_resolution_clock::now();
   std::cout << "Program terminated after " << 
      std::chrono::duration_cast<std::chrono::seconds>(current_time - start_time).count() << " seconds" << std::endl;
}
