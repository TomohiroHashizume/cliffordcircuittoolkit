def BetheLattice( coord_num, depth_in) :
   coordination_number=coord_num;
   num_shells=depth_in;

   sites=[]
   site_labels=[]
   site1_vec=[]
   site2_vec=[]

   for i in range(coord_num+1) :
      sites.append(i);
      site_labels.append(str(i));


   tot_sites=1;
   numsites_prev=coordination_number;
   tot_sites+=numsites_prev;

   for i in range(2,num_shells+1) :
      for j in range(numsites_prev) :
         for k in range(coordination_number-1) :
            currsite=tot_sites+j*(coordination_number-1)+k
            sites.append(currsite)
            site_labels.append(site_labels[tot_sites-numsites_prev+j]+str(k))

      numsites_prev=numsites_prev*(coordination_number-1);
      tot_sites+=numsites_prev;

   print("There are",len(sites),"sites")
   print("There are",sites,"sites")


   for i in range(coordination_number) :
      site1s=[];
      site2s=[];

      site1s.append(0);
      site2s.append(i+1);

      for j in range(2,num_shells+1,2) :
         if (j==2) :
            tot_num=1;
            tot_num+=coordination_number;
         else :
            tot_num+=coordination_number*((coordination_number-1)**(j-3));
            tot_num+=coordination_number*((coordination_number-1)**(j-2));

         for k in range(coordination_number*((coordination_number-1)**(j-1))) :
            site1=tot_num+k
            branch_num=k%(coordination_number-1)
            site2=0
            if ((branch_num+i+1)==coordination_number): 
               site2=tot_num-coordination_number*((coordination_number-1)**(j-2))+ k//(coordination_number-1)
            else :
               site2=tot_num+coordination_number*((coordination_number-1)**(j-1)) \
                     + k*(coordination_number-1)+(branch_num+i)%coordination_number 

            if (site2<len(sites)) :
               site1s.append(site1)
               site2s.append(site2)

            else :
               print("site2 is greater than sites.size()")
               print(site2,"is greater than",len(sites))

      site1_vec.append(site1s.copy());
      site2_vec.append(site2s.copy());

   for i in range(len(sites)) :
      print(sites[i])

   for i in range(len(site1_vec)) :
      for j in range(len(site1_vec[i])) :
            #print(i,site_labels[site1_vec[i][j]],site_labels[site2_vec[i][j]])
            print(i,site1_vec[i][j],site2_vec[i][j])

BetheLattice(3,4)
