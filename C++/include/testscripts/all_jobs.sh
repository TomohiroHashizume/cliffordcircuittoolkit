#!/bin/bash
maxcorelimit=10

LogL=6
L=$(( 2 ** ${LogL} ))
inttype="PWR2_ss_powerlaw_kac"
s_tag=0
init_state="Z"
duration=2

if [ -f "CHP.out" ]; then
   rm "CHP.out"
fi

echo ${inttype},${init_state},${duration}
echo "s_tag=${s_tag}"
bash calc_EE.sh ${inttype} ${LogL} ${s_tag} ${init_state} \
   ${duration} ${testmode}
