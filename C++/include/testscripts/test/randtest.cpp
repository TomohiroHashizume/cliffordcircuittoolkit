#include <iostream>
#include <random>

class randomnumber
{
   public: 
      randomnumber()
      {
         std::random_device rd;
         generator.seed(rd()); 
      };

      double get_rn()
      {
         return rand01(generator);
      }

   private :
      std::mt19937 generator; 
      std::uniform_real_distribution<double> rand01{0.,1.};
};

int main(int argc,char *argv[])
{
   randomnumber randnum;
   for(int i=0;i<100;i++)
      std::cout << randnum.get_rn() << std::endl;
}
