#include "EvolveClifford_evolve.h"
#include <chrono>
#include <iostream>
#include <random>
#include <sqlite3.h>
#include <stdio.h>
#include <string>
#include <vector>

int callback(void *NotUsed, int argc, char **argv, char **azColName){
   return 0;
}

bool check_exist(
      std:: string dbname, 
      int L, int proj_ptag, std::string inttype, 
      int traj, std::string init_state)
{
   sqlite3 *db;
   int rc; 
   char *zErrMsg = 0;

   std::string sql;
   sqlite3_stmt* pStmt=NULL;

   rc = sqlite3_open_v2(dbname.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX , nullptr);
   int iResult = SQLITE_ERROR;

   /* Create SQL statement */
   sql = "SELECT L FROM STATES WHERE "  \
   "L=? AND "  \
   "SS_TAG=?           AND "  \
   "INTTYPE=?          AND "  \
   "TRAJ=?             AND "  \
   "INIT_STATE=?;";

   iResult = sqlite3_prepare_v2(db,sql.c_str(), -1, &pStmt, nullptr);

   sqlite3_bind_int64(pStmt,1,L);
   sqlite3_bind_int64(pStmt,2,proj_ptag);
   sqlite3_bind_text(pStmt,3,inttype.c_str(),inttype.size(),SQLITE_STATIC);
   sqlite3_bind_int64(pStmt,4,traj);
   sqlite3_bind_text(pStmt,5,init_state.c_str(),init_state.size(),SQLITE_STATIC);

   bool out=false;
   if (sqlite3_step(pStmt) == SQLITE_ROW )
   {
      // std::string state_str= (const char *) sqlite3_column_blob(pStmt, 0);
      // std::cout << state_str << std::endl;
      out=true;
   }
   else 
   {
      out=false;
   }

   sqlite3_finalize(pStmt);
   sqlite3_close_v2(db);
   return out;
}

int main(int argc,char *argv[])
{
   if (argc != 9)
   {
      throw 3;
   }

   std::string dbname=argv[1];
   std::string fname=argv[2];
   std::string inttype=argv[3];
   int L = std::atoi(argv[4]);
   int s_tag=std::atoi(argv[5]);
   std::string init=argv[6];

   int traj=std::atoi(argv[7]);
   int duration=std::atoi(argv[8]);

   int proj_ptag=0;
   double proj_p=proj_ptag/10000.;
   double ss=s_tag/10000.;

   int verbose=1;


   int numtimesteps=0;
   numtimesteps=duration; // technically (2*(CliffordGates::int_log2(L)-1)+1)

   std::cout << "Number of timesteps is " << numtimesteps << std::endl;

   long int maxnumgates=1e10;
   int boundary=L;
   bool deterministic=false;
   bool track=false;
   bool save=false;

   auto start_time = std::chrono::high_resolution_clock::now();

   if ( ! check_exist(
      dbname, 
      L, s_tag, inttype, traj, init) )
   {
      std::cout << "Now computing.. " << std::endl;
      int int_log2L=CliffordGates::int_log2(L);
      if (inttype.find("BetheLattice") != std::string::npos)
      {
         L=CliffordGates::get_BetheLatticeLength(3,int_log2L);
      }

      CliffordGates::EvolveClifford_evolve evolve(L,init,-1,verbose);
      if (inttype.find("BetheLattice") != std::string::npos)
      {
         evolve.set_BetheLattice(3,int_log2L);
      }

      // evolve.set_stop_negative_tpi(true);
      evolve.set_ss_exp(ss);
      evolve.evolve(fname,inttype,numtimesteps,maxnumgates,proj_p,boundary,deterministic,track);
      evolve.export_state(fname);
   }

   auto current_time = std::chrono::high_resolution_clock::now();
   std::cout << "Program terminated after " << 
      std::chrono::duration_cast<std::chrono::seconds>(current_time - start_time).count() << " seconds" << std::endl;
}
