#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <math.h>
#include <random>
#include <stdio.h>
#include <string>
#include <sstream>
#include <cstdlib>
#include <vector>
#include "EvolveClifford.h"

extern "C" void dgesvd_( char* jobu, char* jobvt, int* m, int* n, double* a,
      int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt,
      double* work, int* lwork, int* info );

namespace CliffordGates {
   void EvolveClifford::initialize()
   {
      CliffordGates::get_all_elms(Allone,Alltwo,Allgates);
      try
      {
         this->initialize_state();
         is_initialized=true;
      }
      catch(...)
      {
         is_initialized=false;
      }

      if (seed<0)
      {
         std::cerr << "Warning: seed is negative, so we use randomd evice" << std::endl;
         std::random_device rd;
         generator.seed(rd()); 
      }
      else 
      {
         generator.seed(seed);
      }
   }

   void EvolveClifford::initialize_state()
   {
      if ( (init != "X") && (init != "Y") && (init != "Z") )
      {
         std::cerr << "initial condition is not valid!" << std::endl;
         throw 3;
      }

      std::vector<std::vector<bool>> init_state_temp;
      for (int i=0; i<2*Length+1; i++)
      {
         std::vector<bool> tmpvec;
         for (int j=0; j<2*Length+1; j++)
         {
            tmpvec.push_back(false);
         }
         init_state_temp.push_back(tmpvec);
      }

      for (int i=0; i<Length; i++)
      {
         for (int j=0; j<Length; j++)
         {
            if (i==j)
            {
               init_state_temp[i][j] = true;
            }
         }
      }

      for (int i=Length; i<2*Length; i++)
      {
         for (int j=Length; j<2*Length; j++)
         {
            if (i==j)
            {
               init_state_temp[i][j] = true;
            }
         }
      }


      if ( (init=="X") || (init=="Y") )
      {
         if (init=="X")
         {
            std::cout << "initial state is " << init << std::endl;
         }
         for (int i=0;i<Length;i++)
         {
            CliffordGates::apply_Hadamard(init_state_temp,i);
         }
         if (init == "Y")
         {
            std::cout << "initial state is " << init << std::endl;
            for (int i=0;i<Length;i++)
            {
               CliffordGates::apply_PhaseGate(init_state_temp,i);
            }
         }
      }

      init_state=init_state_temp;
      state_now=init_state_temp;
   }

   void EvolveClifford::initialize_state(std::string init_state_str)
   {
      std::vector<std::vector<bool>> init_state_temp;


      if ( init_state_str.length() < Length )
      {
         std::cerr << "Length of product state string is wrong " << init_state_str.length() << " < " << Length << std::endl;
         throw 3;
      }

      for (int i=0; i<2*Length+1; i++)
      {
         std::vector<bool> tmpvec;
         for (int j=0; j<2*Length+1; j++)
         {
            tmpvec.push_back(false);
         }
         init_state_temp.push_back(tmpvec);
      }

      for (int i=0; i<Length; i++)
      {
         for (int j=0; j<Length; j++)
         {
            if (i==j)
            {
               init_state_temp[i][j] = true;
            }
         }
      }

      for (int i=Length; i<2*Length; i++)
      {
         for (int j=Length; j<2*Length; j++)
         {
            if (i==j)
            {
               init_state_temp[i][j] = true;
            }
         }
      }


      for(int i=0; i< Length; i++)
      {
         if (  init_state_str[i] == 'X')
         {
            CliffordGates::apply_Hadamard(init_state_temp,i);
         }
         else if ( init_state_str[i] == 'Y')
         {
            CliffordGates::apply_Hadamard(init_state_temp,i);
            CliffordGates::apply_PhaseGate(init_state_temp,i);
         }
      }

      init_state=init_state_temp;
      state_now=init_state_temp;

      is_initialized=true;
   }

   void EvolveClifford::thermalize()
   {
      long numelm=(Allgates.size());
      std::uniform_int_distribution<long> intdist(0,numelm-1);

      for (int i=0;i<4*Length;i++)
      {
         int d=1;
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);

               int randnum = intdist(generator);
               std::vector<std::string> gates = Allgates[randnum];
               CliffordGates::apply_RandomUnitary(init_state,site1,site2,gates);
            }
         }
      }
      this -> check_maximally_entangled();
   }

   void EvolveClifford::make_EPRpairs(int num_pairs_in)
   {
      num_pairs=num_pairs_in;
      if ( num_pairs<1 )
      {
         num_pairs=Length/2;
      }

      Length=Length-num_pairs;
      for(int i=0; i<num_pairs;i++)
      {
         CliffordGates::apply_Hadamard(init_state,i);
         CliffordGates::apply_CNOT(init_state,i,i+Length);
         CliffordGates::apply_Hadamard(init_state,i+Length);
      }
      state_now=init_state;
      this -> check_maximally_entangled();
   }

   void EvolveClifford::make_EPRpairs_uudd(int num_pairs_in)
   {
      num_pairs=num_pairs_in;
      if ( num_pairs<1 )
      {
         num_pairs=Length/2;
      }

      Length=Length-num_pairs;
      for(int i=0; i<num_pairs;i++)
      {
         CliffordGates::apply_Hadamard(init_state,i);
         CliffordGates::apply_CNOT(init_state,i,i+Length);
         // CliffordGates::apply_Hadamard(init_state,i+Length);
      }
      state_now=init_state;
      this -> check_maximally_entangled();
   }

   bool EvolveClifford::check_maximally_entangled()
   {
      bool maximally_entangled = true;
      for(int Lsize=1;Lsize<=num_pairs;Lsize++)
      {
         std::cout << Length << "," << Length_Orig << std::endl;
         std::cout << CliffordGates::get_entropy_comp(init_state,Length_Orig,Lsize) << std::endl;
         maximally_entangled *= ! (CliffordGates::get_entropy_comp(init_state,Length_Orig,Lsize)<Lsize);
      }
      return maximally_entangled;
   }
}
