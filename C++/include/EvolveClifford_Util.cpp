#include <algorithm>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include <cstdlib>
#include <vector>
#include "EvolveClifford_Util.h"

#define DEBUG 0

namespace CliffordGates {
   void print_state_to_file(std::vector<std::vector<bool>> &state, std::string fname)
   {
      std::ofstream outfile;
      outfile.open(fname);
      for(int i=0; i<state.size()-1; i++)
      {
         for(int j=0; j<(state[i].size()-2); j++)
         {
            outfile << state[i][j] << "," ;
         }
         outfile << state[i][state[i].size()-2] << std::endl;
      }
      outfile.close();
   }

   void print_intlist_to_file(std::vector<std::vector<int>> &intlist, std::string fname)
   {
      std::ofstream outfile;
      outfile.open(fname);
      for(int i=0; i<intlist.size(); i++)
      {
         for(int j=0; j<(intlist[i].size()-1); j++)
         {
            outfile << intlist[i][j] << "," ;
         }
         outfile << intlist[i][intlist[i].size()-1] << std::endl;
      }
      outfile.close();
   }

   std::string print_state_to_string(std::vector<std::vector<bool>> &state)
   {
      std::stringstream out;

      for(int i=0; i<state.size()-1; i++)
      {
         for(int j=0; j<(state[i].size()-2); j++)
         {
            out << state[i][j] << "," ;
         }
         out << state[i][state[i].size()-1] << std::endl;
      }

      return out.str();
   }

   void print_state(std::vector<std::vector<bool>> &state)
   {
      for(int i=0; i<state.size(); i++)
      {
         for(int j=0; j<(state[i].size()-1); j++)
         {
            std::cout << state[i][j] << "," ;
         }
         std::cout << state[i][state[i].size()-1] << std::endl;
      }
   }

   void print_state(std::vector<std::vector<int>> &state)
   {
      for(int i=0; i<state.size(); i++)
      {
         for(int j=0; j<(state[i].size()-1); j++)
         {
            std::cout << state[i][j] << "," ;
         }
         std::cout << state[i][state[i].size()-1] << std::endl;
      }
   }

   void print_vector(std::vector<int> &vec)
   {
      for(int i=0; i<(vec.size()-1); i++)
      {
         std::cout << vec[i] << "," ;
      }
      std::cout << vec[vec.size()-1] << std::endl;
   }

   void print_stab_phases_to_file(std::vector<std::vector<bool>> &stab_phases_out, std::string fname)
   {
      std::ofstream outfile;
      outfile.open(fname);
      for(int i=0; i<stab_phases_out.size(); i++)
      {
         for(int j=0; j<(stab_phases_out[i].size()-1); j++)
         {
            outfile << stab_phases_out[i][j] << "," ;
         }
         outfile << stab_phases_out[i][stab_phases_out[i].size()-1] << std::endl;
      }
      outfile.close();
   }


   std::vector<std::vector<bool>> get_stabilizer(std::vector<std::vector<bool>> state)
   {
      int Length=state.size()/2;
      std::vector<std::vector<bool>> bestate;
      for(int i=Length;i<2*Length;i++)
      {
         std::vector<bool> tmp;
         for(int j1=0;j1<2*Length;j1++)
         {
            tmp.push_back(state[i][j1]);
         }
         bestate.push_back(tmp);
      }
      return bestate;
   }

   std::vector<std::vector<bool>> get_subsystem_stabilizer(const std::vector<std::vector<bool>> state, 
         int Length, std::vector<int> subsystem)
   {
      if (Length < 0) Length=state.size();

      std::vector<std::vector<bool>> state_out;
      for (int i=0; i<state.size();i++)
      {
         std::vector<bool> curr_row;
         for(int j=0; j<subsystem.size();j++)
         {
            curr_row.push_back(state[i][subsystem[j]]);
         }

         for(int j=0; j<subsystem.size();j++)
         {
            curr_row.push_back(state[i][Length+subsystem[j]]);
         }

         state_out.push_back(curr_row);
      }
      return state_out;
   }

   std::vector<std::vector<int>> from_string_to_state(std::string str_state)
   {
      std::istringstream ss_line(str_state);
      std::string stab_line;
      std::string int_now;

      std::vector<std::vector<int>> state_out;

      bool verbose=false;
      if (verbose)
      {
         std::cout << "state start: "<< std::endl;
      }

      while(std::getline(ss_line, stab_line, '\n'))
      {
         std::istringstream ss_int(stab_line);
         std::vector<int> stab_now;
         while(std::getline(ss_int, int_now, ','))
         {
            stab_now.push_back(std::atoi(int_now.c_str()));
         }

         if (verbose)
         {
            std::cout << stab_now.size() << std::endl;
         }

         state_out.push_back(stab_now);
      }

      std::cout << state_out.size() << std::endl;

      if (verbose)
      {
         std::cout << "end"<< std::endl;
      }
      // print_state(state_out);

      return state_out;
   }

   int g(bool x1,bool z1,bool x2, bool z2)
   {
      if ( (x1==z1) && (! x1))
      {
         return int(x1);
      }
      else if ((x1==z1) && (x1))
      {
         return int(z2-x2);
      }
      else if ((x1) && (! z1))
      {
         return int(z2*(2*x2-1));
      }
      else if ((! x1) && (z1))
      {
         return int(x2*(1-2*z2));
      }
      else 
      {
         std::cerr << "CliffordGates::g something is wrong" << std::endl;
         throw 1;
      }
   }

   void rowsum(std::vector<std::vector<bool>> &state, int p,int q, bool measure)
   {
      int Length=(state.size()-1)/2;

      if (measure)
      {
         int tot=2*state[p][Length*2] + 2*state[q][Length*2];
         for(int i=0;i<Length;i++)
         {
            tot += g(state[q][i],state[q][Length+i],state[p][i],state[p][Length+i]);
         }

         if (tot%4 == 0)
         {
            state[p][Length*2]=0;
         }
         else if (tot%4 == 2)
         {
            state[p][Length*2] = 1;
         }
      }

      for(int i=0;i<Length;i++)
      {
         state[p][i] = ( state[q][i] != state[p][i]) ;
         state[p][Length+i] = ( state[q][Length+i] != state[p][Length+i]) ;
      }
   }

   void apply_Hadamard(std::vector<std::vector<bool>> &state, int site)
   {
      // std::cout<< "H" << " " << site << std::endl;
      int Length=(state.size()-1)/2;
      for(int i=0; i<2*Length;i++)
      {
         state[i][state[i].size()-1]= state[i][state[i].size()-1] != (state[i][site] && state[i][Length+site]);
         bool tmp1=state[i][site];
         bool tmp2=state[i][Length+site];

         state[i][site]=tmp2;
         state[i][Length+site]=tmp1;
      }
         
      if (DEBUG > 0)
      {
         std::ofstream outfile;
         outfile.open("CHP.out", std::ios_base::app);
         outfile << "H " << site << "\n"; 
         outfile.close();
      }

   }

   void apply_PhaseGate(std::vector<std::vector<bool>> &state, int site)
   {
      // std::cout<< "P" << " " << site << std::endl;
      int Length=(state.size()-1)/2;
      for(int i=0; i<state.size();i++)
      {
         state[i][state[i].size()-1]= state[i][state[i].size()-1] != ( state[i][site] && state[i][Length+site] );
         state[i][Length+site]= (state[i][Length+site] != state[i][site]) ;
      }
         
      if (DEBUG > 0)
      {
         std::ofstream outfile;
         outfile.open("CHP.out", std::ios_base::app);
         outfile << "P " << site << "\n"; 
         outfile.close();
      }
   }

   void apply_CNOT(std::vector<std::vector<bool>> &state, int site1, int site2)
   {
      // std::cout<< "C" << " " << site1 << " " << site2 << std::endl;
      int Length=(state.size()-1)/2;
      for(int i=0; i<state.size();i++)
      {
         state[i][state[i].size()-1] = 
            (
            state[i][state.size()-1] !=
            ( state[i][site1] && state[i][Length+site2] && 
            (state[i][site2] != state[i][Length+site1] != true)
            )
            );

         state[i][site2] = (state[i][site2] != state[i][site1]) ;
         state[i][Length+site1] = (state[i][Length+site1] != state[i][Length+site2]);
      }

      if (DEBUG > 0)
      {
         std::ofstream outfile;
         outfile.open("CHP.out", std::ios_base::app);
         outfile << "C " << site1 << " " << site2 << "\n"; 
         outfile.close();
      }
   }

   bool apply_Pmeasure(std::vector<std::vector<bool>> &state, int site,bool measure)
   {
      int Length=(state.size()-1)/2;
      int p=0;
      
      for(int i=0;i<Length;i++)
      {
         if (state[Length+i][site])
         {
            p=Length+i;
            break;
         }
      }

      if (p > 0)
      {
         for(int i=0;i<2*Length;i++)
         {
            if (state[i][site])
            {
               if (i != p)
               {
                  CliffordGates::rowsum(state,i,p,measure);
               }
            }
         }

         for(int i=0;i<state[p-Length].size();i++)
         {
            bool tmp = state[p][i];
            state[p-Length][i] = tmp ;
         }
         for(int i=0;i<state[p-Length].size();i++)
         {
            state[p][i] = false;
         }

         if ( (std::rand()/(RAND_MAX + 1.)) < 0.5  )
         {
            state[p][2*Length] = false;
         }
         else 
         {
            state[p][2*Length] = true;
         }

         state[p][Length+site] = true;
         return state[p][2*Length];
      }
      else
      {
         if (measure)
         {
            for(int i=0;i<state[2*Length].size();i++)
            {
               state[2*Length][i] = false;
            }

            for(int i=0;i<Length;i++)
            {
               if (state[i][site])
               {
                  CliffordGates::rowsum(state,2*Length,i+Length,measure);
               }
            }
         }
         return state[2*Length][2*Length];
      }
   }

   void flip(std::vector<std::vector<bool>> &state, int site)
   {
      apply_Hadamard(state,site);
      apply_PhaseGate(state,site);
      apply_PhaseGate(state,site);
      apply_Hadamard(state,site);
   }

   std::vector<std::string> get_gates(std::string elm)
   {
      std::vector<std::string> out;
      std::string strnow="";
      std::string::iterator itr=elm.begin();
      while (itr !=elm.end())
      {
         std::string charnow(1,*itr);

         if ( charnow.compare("+") == 0 )
         {
            out.push_back(strnow);
            strnow="";
         }
         else
         {
            strnow += charnow;
         }
         ++itr;
      }

      if (! strnow.empty())
      {
         out.push_back(strnow);
      }

      if (out.size() < 1)
      {
         throw 2;
      }
      return out;
   }

   int apply_PmeasureX(std::vector<std::vector<bool>> &state, int site,bool measure=false)
   {
      int Length=(state.size()-1)/2;
      int p=0;

      for(int i=0;i<Length;i++)
      {
         if (state[Length+i][Length+site])
         {
            p=Length+i;
            break;
         }
      }

      if (p > 0)
      {
         for(int i=0;i<2*Length;i++)
         {
            if (state[i][Length+site])
            {
               if (i != p)
               {
                  CliffordGates::rowsum(state,i,p,measure);
               }
            }
         }

         for(int i=0;i<state[p-Length].size();i++)
         {
            state[p-Length][i] = state[p][i];
            state[p][i] = false;
         }

         if ( ( std::rand()/(RAND_MAX + 1.) ) < 0.5  )
         {
            state[p][2*Length] = 0;
         }
         else 
         {
            state[p][2*Length] = 1;
         }

         state[p][site] = true;
         return state[p][2*Length];
      }
      else
      {
         if (measure)
         {
            for(int i=0;i<state[2*Length].size();i++)
            {
               state[2*Length][i] = false;
            }

            for(int i=0;i<Length;i++)
            {
               if (state[i][Length+site])
               {
                  CliffordGates::rowsum(state,2*Length,i+Length,measure);
               }
            }
         }

         return state[2*Length][2*Length];
      }
   }

   void get_all_elms(std::vector<std::string> &One_Site, std::vector<std::string> &Two_Site,
         std::vector<std::vector<std::string>> &All_Gates)
   {

      bool just_two=false;
      //just_two!=just_two;
      One_Site.clear();
      Two_Site.clear();
      All_Gates.clear();
      std::vector<std::string> A = {"I","H","HPH"};
      std::vector<std::string> B = {"H1CH0H1CH0H1C","C0H1C","H0P0CH0H1C","H0CH0H1C"};
      std::vector<std::string> C = {"I","HPPH"};
      std::vector<std::string> D = {"CH0H1CH0H1CH1","H0CH0H1CH1","H0H1P1CH0H1CH1","H0H1CH0H1CH1"};
      std::vector<std::string> E = {"I","P","PP","PPP"};

      for ( int ai=0; ai < A.size(); ai++ )
      {
         std::string Aelm=A[ai];
         for( int bi=0; bi < B.size(); bi++)
         {
            std::string Belm = B[bi];
            for( int ci=0; ci < C.size(); ci++ )
            {
               std::string Celm = C[ci];
               for( int di=0; di < D.size(); di++ )
               {
                  std::string Delm = D[di];
                  for( int ei=0; ei < E.size(); ei++ )
                  {
                     std::string Eelm = E[ei];
                     for ( int ai1 =0; ai1 < A.size(); ai1++ )
                     {
                        std::string Aelm1=A[ai1];
                        for( int ci1=0; ci1 < C.size(); ci1++ )
                        {
                           std::string Celm1=C[ci1];
                           for( int ei1=0; ei1 < E.size(); ei1++ )
                           {
                              std::string Eelm1=E[ei1];
                              if ((Belm=="H1CH0H1CH0H1C") &&  (Delm=="CH0H1CH0H1CH1")) 
                              {
                                 One_Site.push_back(Aelm+"+"+Belm+"+"+Celm+"+"+Delm+"+"+Eelm+"+"+Aelm1+"+"+Celm1+"+"+Eelm1);
                              }
                              else
                              {
                                 Two_Site.push_back(Aelm+"+"+Belm+"+"+Celm+"+"+Delm+"+"+Eelm+"+"+Aelm1+"+"+Celm1+"+"+Eelm1);
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      for ( int ai=0; ai < A.size(); ai++ )
      {
         std::string Aelm=A[ai];
         for( int ci=0; ci < C.size(); ci++)
         {
            std::string Celm = C[ci];
            for( int di=0; di < D.size(); di++ )
            {
               std::string Delm = D[di];
               for( int ei=0; ei < E.size(); ei++ )
               {
                  std::string Eelm = E[ei];
                  for ( int ai1 =0; ai1 < A.size(); ai1++ )
                  {
                     std::string Aelm1=A[ai1];
                     for( int ci1=0; ci1 < C.size(); ci1++ )
                     {
                        std::string Celm1=C[ci1];
                        for( int ei1=0; ei1 < E.size(); ei1++ )
                        {
                           std::string Eelm1=E[ei1];
                           Two_Site.push_back(Aelm+"+"+Celm+"+"+Delm+"+"+Eelm+"+"+Aelm1+"+"+Celm1+"+"+Eelm1);
                        }
                     }
                  }
               }
            }
         }
      }

      for(int i=0;i<Two_Site.size();i++)
      {
         All_Gates.push_back(CliffordGates::get_gates(Two_Site[i]));
      }

      if (!just_two)
      {
         for(int i=0;i<One_Site.size();i++)
         {
            All_Gates.push_back(CliffordGates::get_gates(One_Site[i]));
         }
         std::cout << All_Gates.size() << " elements of 11520 elements" << std::endl;
      }
      else
      {
         std::cout << All_Gates.size() << " elements of "<< 11520-576 <<" elements" << std::endl;
      }
   }

   void Apply_ACE(std::string gate,std::vector<std::vector<bool>> &state,int site,bool is_conj=false)
   {
      std::string::iterator itr=gate.begin();
      while (itr !=gate.end())
      {
         std::string s(1,*itr);
         if (s == "H")
         {
            CliffordGates::apply_Hadamard(state,site);
         }
         if (s == "P")
         {
            CliffordGates::apply_PhaseGate(state,site);
            if (is_conj)
            {
               CliffordGates::apply_PhaseGate(state,site);
               CliffordGates::apply_PhaseGate(state,site);
            }
         }
         itr++;
      }
   }

   void Apply_BD(std::string gate,std::vector<std::vector<bool>> &state,int site1,int site2,bool is_conj=false)
   {
      std::string::iterator itr=gate.begin();
      while (itr !=gate.end())
      {
         std::string s1(1,*itr);
         if (s1 == "H")
         {
            itr++;
            std::string s2(1,*itr);
            if (s2 == "0")
            {
               CliffordGates::apply_Hadamard(state,site1);
            }
            else if (s2 == "1")
            {
               CliffordGates::apply_Hadamard(state,site2);
            }
         }
         else if (s1 == "P")
         {
            itr++;
            std::string s2(1,*itr);
            if (s2 == "0")
            {
               CliffordGates::apply_PhaseGate(state,site1);
               if (is_conj)
               {
                  CliffordGates::apply_PhaseGate(state,site1);
                  CliffordGates::apply_PhaseGate(state,site1);
               }
            }
            else if (s2 == "1")
            {
               CliffordGates::apply_PhaseGate(state,site2);
               if (is_conj)
               {
                  CliffordGates::apply_PhaseGate(state,site2);
                  CliffordGates::apply_PhaseGate(state,site2);
               }
            }
         }
         else if (s1 == "C")
         {
            CliffordGates::apply_CNOT(state,site2,site1);
         }
         itr++;
      }
   }

   void apply_RandomUnitary(std::vector<std::vector<bool>> &state, int site1, int site2, 
         std::vector<std::string> gates,bool is_conj)
   {
      if (gates.size() == 8)
      {
         CliffordGates::Apply_ACE(gates[0],state,site2,is_conj);
         CliffordGates::Apply_BD(gates[1],state,site1,site2,is_conj);
         CliffordGates::Apply_ACE(gates[2],state,site1,is_conj);

         CliffordGates::Apply_BD(gates[3],state,site1,site2,is_conj);
         CliffordGates::Apply_ACE(gates[4],state,site2,is_conj);

         CliffordGates::Apply_ACE(gates[5],state,site1,is_conj);
         CliffordGates::Apply_ACE(gates[6],state,site1,is_conj);

         CliffordGates::Apply_ACE(gates[7],state,site2,is_conj);
      }
      else if (gates.size() == 7)
      {
         CliffordGates::Apply_ACE(gates[0],state,site1,is_conj);
         CliffordGates::Apply_ACE(gates[1],state,site1,is_conj);

         CliffordGates::Apply_BD(gates[2],state,site1,site2,is_conj);
         CliffordGates::Apply_ACE(gates[3],state,site2,is_conj);

         CliffordGates::Apply_ACE(gates[4],state,site1,is_conj);
         CliffordGates::Apply_ACE(gates[5],state,site1,is_conj);

         CliffordGates::Apply_ACE(gates[6],state,site2,is_conj);
      }
      else
      {
         std::cout << "error on the application on gates" << std::endl;
         std::cout << gates.size() << std::endl;
         std::exit(1);
      }
   }

   void swap(std::vector<std::vector<int>> &mat, int row1, int row2, int col)
   {   
      for (int i = 0; i < col; i++) 
      {   
         int temp = mat[row1][i]; 
         mat[row1][i] = mat[row2][i]; 
         mat[row2][i] = temp; 
      }   
   }  

   void swap(std::vector<std::vector<bool>> &mat, int row1, int row2, int col)
   {   
      for (int i = 0; i < col; i++) 
      {   
         bool temp = mat[row1][i]; 
         mat[row1][i] = mat[row2][i]; 
         mat[row2][i] = temp; 
      }   
   }  

   int get_rank(std::vector<std::vector<int>> mat,bool verbose)
   {
      int nrows=mat.size();
      int ncols=mat[0].size();
      int rank=0;
      int k,r,i;

      for(int k=0;k<ncols;k++)
      {
         i=-1;
         for(r=rank;r<nrows;r++)
         {
            if( ! mat[r][k] )
            {
               continue;
            }
            if(i==-1)
            {
               i=r;
               rank++;

            }
            else
            {
               for(int j=0;j<ncols;j++)
               {
                  mat[r][j] = (mat[r][j] != mat[i][j]);
               }

            }
         }

         if( i>= rank)
         {
            CliffordGates::swap(mat,i,rank-1,ncols);
         }
      }

      return rank;
   }

   int get_rank(std::vector<std::vector<bool>> mat,bool verbose)
   {
      int nrows=mat.size();
      int ncols=mat[0].size();
      int rank=0;
      int k,r,i;

      for(int k=0;k<ncols;k++)
      {
         i=-1;
         for(r=rank;r<nrows;r++)
         {
            if( ! mat[r][k] )
            {
               continue;
            }
            if(i==-1)
            {
               i=r;
               rank++;

            }
            else
            {
               for(int j=0;j<ncols;j++)
               {
                  mat[r][j] = (mat[r][j] != mat[i][j]);
               }

            }
         }

         if( i>= rank)
         {
            CliffordGates::swap(mat,i,rank-1,ncols);
         }
      }
      return rank;
   }

   int get_entropy(std::vector<std::vector<bool>> state,const int Length,const int boundary,bool verbose)
   {
      std::vector<std::vector<bool>> bestate;
      for(int i=Length;i<2*Length;i++)
      {
         std::vector<bool> tmp;
         for(int j1=0;j1<boundary;j1++)
         {
            tmp.push_back(state[i][j1]);
         }

         for(int j2=0;j2<boundary;j2++)
         {
            tmp.push_back(state[i][Length+j2]);
         }
         bestate.push_back(tmp);
      }
      int ent = CliffordGates::get_rank(bestate,verbose)-boundary;
      return ent;
   }

   int get_entropy_comp(std::vector<std::vector<bool>> state,const int Length, const int boundary,bool verbose)
   {
      std::vector<std::vector<bool>> bestate;
      for(int i=Length;i<2*Length;i++)
      {
         std::vector<bool> tmp;
         for(int j1=1;j1<=boundary;j1++)
         {
            tmp.push_back(state[i][Length-j1]);
         }

         for(int j2=1;j2<=boundary;j2++)
         {
            tmp.push_back(state[i][2*Length-j2]);
         }
         bestate.push_back(tmp);
      }

      int ent = CliffordGates::get_rank(bestate,verbose)-boundary;
      return ent;
   }

   int get_tpi_l(const std::vector<std::vector<bool>> state,const int Length, int subsyssize, bool verbose)
   {
      if (subsyssize <= 0)
      {
         subsyssize=(Length/4);
      }

      std::vector<std::vector<bool>> bestate=get_stabilizer(state);

      std::vector<int> subA;
      std::vector<int> subAB;
      std::vector<int> subAC;
      std::vector<int> subABC;
      for(int ind=0;ind<int(subsyssize);ind++)
      {
         subA.push_back(ind);
         subAB.push_back(ind);
         subAC.push_back(ind);
         subABC.push_back(ind);
      }

      std::vector<int> subB;
      std::vector<int> subBC;
      for(int ind=int(Length/4);ind<int(Length/4+subsyssize);ind++)
      {
         subB.push_back(ind);

         subAB.push_back(ind);
         subABC.push_back(ind);

         subBC.push_back(ind);
      }

      std::vector<int> subC;
      for(int ind=int(Length/2);ind<int(Length/2+subsyssize);ind++)
      {
         subC.push_back(ind);
         subAC.push_back(ind);
         subABC.push_back(ind);

         subBC.push_back(ind);
      }

      /* Debug */
      /*
      print_vector(subA);
      print_vector(subB);
      print_vector(subC);
      print_vector(subAB);
      print_vector(subBC);
      print_vector(subAC);
      print_vector(subABC);
      */ 

      int entA   = CliffordGates::get_rank(get_subsystem_stabilizer(bestate,Length,subA),  verbose)-subA.size();
      int entAB  = CliffordGates::get_rank(get_subsystem_stabilizer(bestate,Length,subAB), verbose)-subAB.size();
      int entAC  = CliffordGates::get_rank(get_subsystem_stabilizer(bestate,Length,subAC), verbose)-subAC.size();
      int entABC = CliffordGates::get_rank(get_subsystem_stabilizer(bestate,Length,subABC),verbose)-subABC.size();

      int entB   = CliffordGates::get_rank(get_subsystem_stabilizer(bestate,Length,subB),  verbose)-subB.size();
      int entBC  = CliffordGates::get_rank(get_subsystem_stabilizer(bestate,Length,subBC), verbose)-subBC.size();

      int entC   = CliffordGates::get_rank(get_subsystem_stabilizer(bestate,Length,subC),  verbose)-subC.size();

      int tpi=(entA+entB-entAB)+(entA+entC-entAC)-(entA+entBC-entABC);
      return tpi;
   }

   int get_tpi_p(const std::vector<std::vector<bool>> state,const int Length, int subsyssize, int base, bool verbose)
   {
      if (subsyssize < 0)
      {
         subsyssize=(Length/4);
      }

      std::vector<int> geo=MonnaMap::monnamap(Length,base);
      std::vector<std::vector<bool>> bestate=get_stabilizer(state);

      std::vector<int> subA;
      std::vector<int> subAB;
      std::vector<int> subAC;
      std::vector<int> subABC;
      for(int ind=0;ind<int(subsyssize);ind++)
      {
         subA.push_back(  geo[ind]);
         subAB.push_back( geo[ind]);
         subAC.push_back( geo[ind]);
         subABC.push_back(geo[ind]);
      }

      std::vector<int> subB;
      std::vector<int> subBC;
      for(int ind=int(Length/4);ind<int(Length/4+subsyssize);ind++)
      {
         subB.push_back(geo[ind]);

         subAB.push_back(geo[ind]);
         subABC.push_back(geo[ind]);

         subBC.push_back(geo[ind]);
      }

      std::vector<int> subC;
      for(int ind=int(Length/2);ind<int(Length/2+subsyssize);ind++)
      {
         subC.push_back(  geo[ind]);
         subAC.push_back( geo[ind]);
         subABC.push_back(geo[ind]);
         subBC.push_back( geo[ind]);
      }

      int entA   = CliffordGates::get_rank(get_subsystem_stabilizer(bestate,Length,subA),  verbose)-subA.size();
      int entAB  = CliffordGates::get_rank(get_subsystem_stabilizer(bestate,Length,subAB), verbose)-subAB.size();
      int entAC  = CliffordGates::get_rank(get_subsystem_stabilizer(bestate,Length,subAC), verbose)-subAC.size();
      int entABC = CliffordGates::get_rank(get_subsystem_stabilizer(bestate,Length,subABC),verbose)-subABC.size();
      int entB   = CliffordGates::get_rank(get_subsystem_stabilizer(bestate,Length,subB),  verbose)-subB.size();
      int entBC  = CliffordGates::get_rank(get_subsystem_stabilizer(bestate,Length,subBC), verbose)-subBC.size();
      int entC   = CliffordGates::get_rank(get_subsystem_stabilizer(bestate,Length,subC),  verbose)-subC.size();

      int tpi=(entA+entB-entAB)+(entA+entC-entAC)-(entA+entBC-entABC);
      return tpi;
   }


   std::vector<int> intlogspace(int end,int numsample)
   {
      int log10end = int( std::log10(end) );
      double dl10=double(log10end)/numsample;
      std::vector<int> out;
      out.push_back(1);
      for(int i=1;i<numsample;i++)
      {
         int currval = int(std::pow(10.,double(i)*dl10));
         if (currval != out[out.size()-1])
         {
            out.push_back(currval);
         }
      }
      return out;
   }

   int intpow(int x, int p)
   {
     if (p == 0) return 1;
     if (p == 1) return x;
     return x * intpow(x, p-1);
   }

   int int_log2(int num)
   {
      int count=0;
      while(num > 1)
      {
         num /= 2;
         count++;
      }
      return count;
   }

   std::vector<int> binarize(int num,int maxnum)
   {
      int curr_place_pow=int_log2(maxnum);
      std::vector<int> out;
      while (curr_place_pow > 0)
      {
         curr_place_pow--;
         int currnum=std::pow(2,curr_place_pow);
         if (num >= currnum)
         {
            num -= currnum;
            out.push_back(1);
         }
         else
         {
            out.push_back(0);
         }
      }

      return out;
   }

   int read_binary_flipped(std::vector<int> in)
   {
      int out=0;
      for (int i=0;i<in.size();i++)
      {
         out+=in[i]*std::pow(2,i);
      }
      return out;
   }

   int read_binary(std::vector<int> in)
   {
      int out=0;
      for (int i=0;i<in.size();i++)
      {
         out+=in[i]*std::pow(2,in.size()-1-i);
      }
      return out;
   }

   std::vector<int> rotate_vec(std::vector<int> in)
   {

      std::vector<int> out;
      for(int i=1;i<in.size();i++)
      {
         out.push_back(in[i]);
      }
      out.push_back(in[0]);

      std::cout << std::endl;
      std::cout << "in" << std::endl;
      std::cout << read_binary(in) << std::endl;
      std::cout << std::endl;
      for(int i=0;i<(in.size());i++)
      {
         std::cout << in[i] << std::endl;
      }
      std::cout << std::endl;
      std::cout << "out" << std::endl;
      std::cout << read_binary(out) << std::endl;
      std::cout << std::endl;
      for(int i=0;i<(out.size());i++)
      {
         std::cout << out[i] << std::endl;
      }
      std::cout << std::endl;

      return out;
   }


   int rotate_bit(int num,int maxnum)
   {
      return read_binary(rotate_vec(binarize(num,maxnum)));
      // return read_binary_flipped(binarize(num,maxnum));
   }

   void projective_measure(std::vector<std::vector<bool>> &state,double proj_p,int site1,int site2)
   {
      if ( ( std::rand()/(RAND_MAX + 1.))  < proj_p )
      {
         CliffordGates::apply_Pmeasure(state,site1);
      }

      if ( ( std::rand()/(RAND_MAX + 1.))  < proj_p )
      {
         CliffordGates::apply_Pmeasure(state,site2);
      }
   }

   int get_BetheLatticeLength(int coord_num, int depth_in)
   {

      std::vector<int> sites;
      if ((sites.size() > 0 ) ||  (depth_in < 1) )
      {
         throw 2;
      }

      int coordination_number=coord_num;
      int num_shells=depth_in;

      for(int ind=0;ind<coord_num+1;ind++)
      {
         sites.push_back(ind);
         // site_labels.push_back(std::to_string(ind));
      }

      int tot_sites=1;
      int numsites_prev=coordination_number;
      tot_sites+=numsites_prev;

      for(int i_ind=2;i_ind<=(num_shells);i_ind++)
      {
         for(int j_ind=0;j_ind<numsites_prev;j_ind++)
         {
            for(int k_ind=0;k_ind<(coordination_number-1);k_ind++)
            {
               int currsite=tot_sites+j_ind*(coordination_number-1)+k_ind;
               sites.push_back(currsite);
               // site_labels.push_back(site_labels[tot_sites-numsites_prev+j_ind]+std::to_string(k_ind));
            }
         }

         numsites_prev=numsites_prev*(coordination_number-1);
         tot_sites+=numsites_prev;
      }

      return tot_sites;
   }

   std::vector<int> calc_IABM(std::vector<std::vector<bool>> state,int Length, std::vector<int> Bsites,bool verbose)
   {
      for(int site=0; site<Length;site++)
      {
         if (! (std::find(Bsites.begin(), Bsites.end(), site) != Bsites.end()))
         {
            apply_Pmeasure(state,site);
            if (verbose)
            {
               std::cout << "measured: " << site << std::endl;
            }
         } 
      }

      std::vector<int> out;
      for(int ind1=0;ind1<Bsites.size();ind1++)
      {
         std::vector<std::vector<bool>> state_temp=state;
         int Bsite=Bsites[ind1];
         for(int ind2=0;ind2<Bsites.size();ind2++)
         {
            int Bsite_temp=Bsites[ind2];
            if (Bsite != Bsite_temp)
            {
               apply_Pmeasure(state_temp,Bsite_temp);
               if (verbose)
               {
                  std::cout << Bsite << " measured: " << Bsite_temp << std::endl;
               }
            } 
         }

         state_temp=get_stabilizer(state_temp);
         int entA = CliffordGates::get_rank(get_subsystem_stabilizer(state_temp,-1,{Bsite} ))-1;
         int entB = CliffordGates::get_rank(get_subsystem_stabilizer(state_temp,-1,{Length}))-1;
         int entAB=CliffordGates::get_rank(get_subsystem_stabilizer(state_temp,-1,{Bsite,Length}))-2;
         if(verbose) std::cout << Bsite << ":" << entA << "," << entB << "," << entAB << std::endl;

         out.push_back(entA+entB-entAB);
      }
      return out;
   }
}
