#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <string>

namespace CliffordGates {
   class EvolveClifford_eta : public EvolveClifford_evolve
   {
      public :
         EvolveClifford_eta(){};
         EvolveClifford_eta(int Length_in, int seed_in=0, int verbose_in=0)
         {
            init="Z";
            Length=Length_in;
            seed=seed_in;
            verbose=verbose_in;
            this->initialize();
         }

         void evolve_eta(std::string inttype_in, long int numtimesteps_in, long int maxnumgates_in, 
               double proj_p_in, int boundary_in, bool deterministic_in, bool track_in);
         void make_EPRpairs_eta();
         void decode(int numsamples);
         void output_stab_phases(std::string fname){ print_stab_phases_to_file(stab_phases,fname); }

      private :
         void make_measurements();
         std::vector<std::vector<bool>> stab_phases;
   };

   void EvolveClifford_eta::evolve_eta( std::string inttype_in, long int numtimesteps_in,long int maxnumgates_in, 
         double proj_p_in, int boundary_in,bool deterministic_in,bool track_in)
   {
      inttype=inttype_in;
      std::string inttype_Orig=inttype;
      deterministic=deterministic_in;
      proj_p=proj_p_in;
      boundary=boundary_in;
      numtimesteps=numtimesteps_in;
      maxnumgates=maxnumgates_in;
      track=track_in;

      if (! is_initialized)
      {
         std::cerr << "initialization: something is wrong" << std::endl;
         throw 3;
      }

      std::cout << "proj_p=" << proj_p << std::endl;


      if (depth > 0)
      {
         std::cout << "depth = " << depth << std::endl;
      }

      if (boundary<=0)
      {
         boundary=Length/2;
      }

      numgates=0;
      int out=0;

      curr_state=init_state;

      auto start_time = std::chrono::high_resolution_clock::now();

      int Length_Orig=Length;

      Length = Length/2;

      this->make_sites_vec();
      for(long int i=1;i<numtimesteps;i++)
      {
         out = this->evolve_one_timestep(i);
         this -> make_measurements();

         if ( inttype.substr(0,14) == "Riffle_reverse" )
         {
            Riffle_sites_vec_reverse();
         }
         else if ( inttype.substr(0,6) == "Riffle" )
         {
            Riffle_sites_vec();
         }

         if ( i%100 == 0)
         {
            std::cout << i <<" of "<< numtimesteps << " with " << numgates << " gates " << " applied " << std::endl << std::flush;
            auto current_time = std::chrono::high_resolution_clock::now();
            std::cout <<
               std::chrono::duration_cast<std::chrono::seconds>(current_time - start_time).count() << " seconds past" << std::endl;
         }
      }

      long int i = numtimesteps;
      if(i > 0)
      {
         out = this->evolve_one_timestep(i);

         if ( inttype.substr(0,14) == "Riffle_reverse" )
         {
            Riffle_sites_vec_reverse();
         }
         else if ( inttype.substr(0,6) == "Riffle" )
         {
            Riffle_sites_vec();
         }
      }

      inttype=inttype+"_backward";
      i = numtimesteps;
      if(i > 0)
      {
         out = this->evolve_one_timestep(i);

         if ( inttype.substr(0,14) == "Riffle_reverse" )
         {
            Riffle_sites_vec();
         }
         else if ( inttype.substr(0,6) == "Riffle" )
         {
            Riffle_sites_vec_reverse();
         }
      }

      while(i>1)
      {
         i--;
         out = this->evolve_one_timestep(i);

         if ( inttype.substr(0,14) == "Riffle_reverse" )
         {
            Riffle_sites_vec();
         }
         else if ( inttype.substr(0,6) == "Riffle" )
         {
            Riffle_sites_vec_reverse();
         }

         if ( i%100 == 0)
         {
            std::cout << i <<" of "<< numtimesteps << " with " << numgates << " gates " << " applied " << std::endl << std::flush;
            auto current_time = std::chrono::high_resolution_clock::now();
            std::cout <<
               std::chrono::duration_cast<std::chrono::seconds>(current_time - start_time).count() << " seconds past" << std::endl;
         }
      }


      inttype=inttype_Orig;
      Length = Length*2;
      state_now=curr_state;
   }

   // may be I should call it encode.
   void EvolveClifford_eta::make_EPRpairs_eta()
   {
      for(int site0=0;site0<Length;site0++)
      {
         apply_Hadamard(init_state,site0);
         apply_PhaseGate(init_state,site0);
         apply_PhaseGate(init_state,site0);
         apply_Hadamard(init_state,site0);
      }

      for(int site0=0; site0<(Length/2);site0++)
      {
         CliffordGates::apply_Hadamard(init_state,site0);
         CliffordGates::apply_CNOT(init_state,site0,site0+Length/2);
         apply_Hadamard( init_state,site0+Length/2);
         apply_PhaseGate(init_state,site0+Length/2);
         apply_PhaseGate(init_state,site0+Length/2);
         apply_Hadamard( init_state,site0+Length/2);
      }

      state_now=init_state;
   }

   void EvolveClifford_eta::decode(int numsamples)
   {
      std::vector<std::vector<bool>> stab_phases_temp;

      for(int site0=0; site0<(Length/2);site0++)
      {
         apply_Hadamard( state_now,site0+Length/2);
         apply_PhaseGate(state_now,site0+Length/2);
         apply_PhaseGate(state_now,site0+Length/2);
         apply_Hadamard( state_now,site0+Length/2);
         CliffordGates::apply_CNOT(state_now,site0,site0+Length/2);
         CliffordGates::apply_Hadamard(state_now,site0);
      }

      // std::cout << " BOOL CHECK " << std::endl;
      // std::cout << (true != true) <<std::endl;
      // std::cout << (true != false) <<std::endl;
      // std::cout << (false != true) <<std::endl;
      // std::cout << (false != false) <<std::endl;
      // std::cout << " BOOL CHECK " << std::endl;
      // std::cout << (true  && true) <<std::endl;
      // std::cout << (true  && false) <<std::endl;
      // std::cout << (false && true) <<std::endl;
      // std::cout << (false && false) <<std::endl;

      for(int sample=0; sample<numsamples;sample++)
      {
         std::vector<bool> temp;
         std::vector<std::vector<bool>> measured_state = state_now;

         for(int site0=0;site0<Length;site0++)
         {
            temp.push_back(CliffordGates::apply_Pmeasure(measured_state,site0,true));
         }
         
         stab_phases_temp.push_back(temp);
      }
      stab_phases=stab_phases_temp;
   }

   void EvolveClifford_eta::make_measurements()
   {
      int maxlen=Length;
      for(int site=0;site<maxlen;site++)
      {
         if ( rand01(generator)  < proj_p )
         {
            CliffordGates::apply_Pmeasure(curr_state,site,true);
         }
      }
   }
}
