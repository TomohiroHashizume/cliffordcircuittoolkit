#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <string>
#include "EvolveClifford_evolve.h"

namespace CliffordGates {
      void EvolveClifford_evolve::set_track_mi_M(bool track_mi_M_in)
      {
         track_mi_M=track_mi_M_in;
         mi_M_sites.clear();
         if (Length<33)
         {
            for(int iii=0;iii<Length;iii++)
            {
               mi_M_sites.push_back(iii);
            }
         }
         else
         {
            for(int iii=0;iii<10;iii++)
            {
               mi_M_sites.push_back(iii);
            }

            for(int iii=-10;iii<0;iii++)
            {
               mi_M_sites.push_back(Length+iii);
            }
         }
      }

   void EvolveClifford_evolve::set_track_mi_M_monna(bool track_mi_M_in)
   {
      std::vector<int> geo=MonnaMap::monnamap(Length,2);
      track_mi_M=track_mi_M_in;
      mi_M_sites.clear();
      if (Length<33)
      {
         for(int iii=0;iii<Length;iii++)
         {
            mi_M_sites.push_back(geo[iii]);
         }
      }
      else
      {
         for(int iii=0;iii<20;iii++)
         {
            mi_M_sites.push_back(geo[iii]);
         }
      }
   }


   int EvolveClifford_evolve::evolve_one_timestep(long long int t)
   {
      int out=0;
      if (inttype=="PWR2")
      {
         out = this->evolve_PWR2(t);
      }
      else if (inttype=="PWR2_obc_bylayer")
      {
         out = this->evolve_PWR2_obc_bylayer(t);
      }
      else if (inttype=="PWR2_obc_double_bylayer")
      {
         out = this->evolve_PWR2_obc_double_bylayer(t);
      }
      else if (inttype=="PWR2_2")
      {
         out = this->evolve_PWR2_2(t);
      }
      else if (inttype=="PWR2layer")
      {
         out = this->evolve_PWR2layer(t);
      }
      else if (inttype=="PWR2_Player")
      {
         deterministic=true;
         out = this->evolve_PWR2_Player(t);
      }
      else if (inttype=="randPWR2_layer")
      {
         deterministic=false;
         out = this->evolve_randPWR2_layer(t);
      }
      else if (inttype=="PWR2_Player_depth")
      {
         deterministic=true;
         out = this->evolve_PWR2_Player_depth(t);
      }
      else if (inttype=="PWR2_Player_backward")
      {
         deterministic=true;
         out = this->evolve_PWR2_Player_backward(t);
      }
      else if (inttype=="PWR2rev")
      {
         out = this->evolve_PWR2rev(t);
      }
      else if (inttype=="PWR2_CZ_noH")
      {
         out = this->evolve_PWR2_CZ_noH(t);
      }
      else if (inttype=="PWR2_CZ_H")
      {
         out = this->evolve_PWR2_CZ_H(t);
      }
      else if (inttype=="Riffle_double_CZ_H")
      {
         out = this->evolve_Riffle_double_CZ_H(t);
      }
      else if (inttype=="Riffle_double_CZ_H_bylayer")
      {
         out = this->evolve_Riffle_double_CZ_H_bylayer(t);
      }
      else if (inttype=="Riffle_double_CZ_H_P_bylayer")
      {
         out = this->evolve_Riffle_double_CZ_H_P_bylayer(t);
      }
      else if (inttype=="Riffle_double_H_CZ_P_bylayer")
      {
         out = this->evolve_Riffle_double_H_CZ_P_bylayer(t);
      }
      else if (inttype=="Riffle_CZ_H_bylayer")
      {
         out = this->evolve_Riffle_CZ_H_bylayer(t);
      }
      else if (inttype=="Riffle_CZ_H_P_bylayer")
      {
         out = this->evolve_Riffle_CZ_H_P_bylayer(t);
      }
      else if (inttype=="Riffle_H_CZ_P_bylayer")
      {
         out = this->evolve_Riffle_H_CZ_P_bylayer(t);
      }
      else if (inttype=="Riffle_reverse_double_CZ_H_bylayer")
      {
         out = this->evolve_Riffle_reverse_double_CZ_H_bylayer(t);
      }
      else if (inttype=="Riffle_reverse_double_CZ_H_P_bylayer")
      {
         out = this->evolve_Riffle_reverse_double_CZ_H_P_bylayer(t);
      }
      else if (inttype=="Riffle_reverse_double_H_CZ_P_bylayer")
      {
         out = this->evolve_Riffle_reverse_double_H_CZ_P_bylayer(t);
      }
      else if (inttype=="Riffle_reverse_CZ_H_bylayer")
      {
         out = this->evolve_Riffle_reverse_CZ_H_bylayer(t);
      }
      else if (inttype=="Riffle_reverse_even_odd_CZ_H_bylayer")
      {
         out = this->evolve_Riffle_reverse_even_odd_CZ_H_bylayer(t); 
      }
      else if (inttype=="Riffle_reverse_even_odd_CZ_H_P_bylayer")
      {
         out = this->evolve_Riffle_reverse_even_odd_CZ_H_P_bylayer(t);  // https://arxiv.org/pdf/2102.13117.pdf
      }
      else if (inttype=="Riffle_reverse_even_odd_CZ_H_P")
      {
         out = this->evolve_Riffle_reverse_even_odd_CZ_H_P(t); // https://arxiv.org/pdf/2102.13117.pdf
      }
      else if (inttype=="Riffle_reverse_even_odd_CZ_H_P_ss_powerlaw_kac")
      {
         out = this->evolve_Riffle_reverse_even_odd_CZ_H_P_ss_powerlaw_kac(t); 
      }
      else if (inttype=="Riffle_reverse_double_even_odd_CZ_H_bylayer")
      {
         out = this->evolve_Riffle_reverse_double_even_odd_CZ_H_bylayer(t);
      }
      else if (inttype=="Riffle_reverse_double_even_odd_CZ_H_P_bylayer")
      {
         out = this->evolve_Riffle_reverse_double_even_odd_CZ_H_P_bylayer(t);
      }
      else if (inttype=="Riffle_reverse_CZ_H_P_bylayer")
      {
         out = this->evolve_Riffle_reverse_CZ_H_P_bylayer(t);
      }
      else if (inttype=="Riffle_reverse_H_CZ_P_bylayer")
      {
         out = this->evolve_Riffle_reverse_H_CZ_P_bylayer(t);
      }
      else if (inttype=="PWR2_double_CZ_H")
      {
         out = this->evolve_PWR2_double_CZ_H(t);
      }
      else if (inttype=="PWR2_ss_powerlaw_CHP")
      {
         out = this->evolve_PWR2_ss_powerlaw_CHP(t);
      }
      else if (inttype=="PWR2_ss_powerlaw")
      {
         out = this->evolve_PWR2_ss_powerlaw(t);
      }
      else if (inttype=="PWR2_ss_powerlaw_kac")
      {
         out = this->evolve_PWR2_ss_powerlaw_kac(t);
      }
      else if (inttype=="PWR2_ss_powerlaw_bylayer")
      {
         out = this->evolve_PWR2_ss_powerlaw_bylayer(t);
      }
      else if (inttype=="PWR2_ss_powerlaw_bylayer_kac")
      {
         out = this->evolve_PWR2_ss_powerlaw_bylayer_kac(t);
      }
      else if (inttype=="PWR2_NN_CZ_H_P")
      {
         out = this->evolve_PWR2_NN_CZ_H_P(t);
      }
      else if (inttype=="PWR2_CZ_H_P")
      {
         out = this->evolve_PWR2_CZ_H_P(t);
      }
      else if (inttype=="PWR2_CZ_H_Pfirst")
      {
         out = this->evolve_PWR2_CZ_H_Pfirst(t);
      }
      else if (inttype=="PWR2_CZ_H_P2")
      {
         out = this->evolve_PWR2_CZ_H_P2(t);
      }
      else if (inttype=="PWR2_CZ_H_Pfirst2")
      {
         out = this->evolve_PWR2_CZ_H_Pfirst2(t);
      }
      else if (inttype=="PWR2_CZ_H2")
      {
         out = this->evolve_PWR2_CZ_H2(t);
      }
      else if (inttype=="PWR2_CZ_H_NN")
      {
         out = this->evolve_PWR2_CZ_H_NN(t);
      }
      else if (inttype=="PWR2_CZ_H_P_NN")
      {
         out = this->evolve_PWR2_CZ_H_P_NN(t);
      }
      else if (inttype=="PWR2_CZ_H_NN_NN")
      {
         out = this->evolve_PWR2_CZ_H_NN_NN(t);
      }
      else if (inttype=="PWR2_CZ_Hlayer")
      {
         out = this->evolve_PWR2_CZ_Hlayer(t);
      }
      else if (inttype=="PWR2_CZ_H_Player")
      {
         out = this->evolve_PWR2_CZ_H_Player(t);
      }
      else if (inttype=="PWR2_CZ_H_Pfirstlayer")
      {
         out = this->evolve_PWR2_CZ_H_Pfirstlayer(t);
      }
      else if (inttype=="PWR2_blockQ")
      {
         out = this->evolve_PWR2_blockQ(t);
      }
      else if (inttype == "NNAA")
      {
         out = this->evolve_NNAA(t);
      }
      else if (inttype == "AA")
      {
         out = this->evolve_AA(t);
      }
      else if (inttype == "sqL")
      {
         out = this->evolve_sqL(t);
      }
      else if (inttype == "NN")
      {
         out = this->evolve_NN(t);
      }
      else if (inttype == "NN_Player")
      {
         deterministic=true;
         out = this->evolve_NN_Player(t);
      }
      else if (inttype == "NN_CZ_Hlayer")
      {
         out = this->evolve_NN_CZ_Hlayer(t);
      }
      else if (inttype == "NN_CZ_H_Player")
      {
         out = this->evolve_NN_CZ_H_Player(t);
      }
      else if (inttype == "NN_CZ_H_Pfirstlayer")
      {
         out = this->evolve_NN_CZ_H_Pfirstlayer(t);
      }
      else if (inttype == "depth")
      {
         out = this->evolve_depth(t);
      }
      else if (inttype == "s_random")
      {
         if (t==1)
         {
            make_all_2_sites_vec();
         }
         out = this-> evolve_s_random(t);
      }
      else if (inttype == "randNN_obc")
      {
         deterministic=false;
         out = this->evolve_randNN_obc(t);
      }
      else if (inttype == "randNN")
      {
         deterministic=false;
         out = this->evolve_randNN(t);
      }
      else if (inttype == "randAlltoAll")
      {
         deterministic=false;
         out = this->evolve_randAlltoAll(t);
      }
      else if (inttype == "randAlltoAll_powerlaw_kac")
      {
         deterministic=false;
         out = this->evolve_randAlltoAll_powerlaw_kac(t);
      }
      else if (inttype == "randAlltoAll_CZ_Hlayer")
      {
         out = this->evolve_randAlltoAll_CZ_Hlayer(t);
      }
      else if (inttype == "randAlltoAll_CZ_H_Player")
      {
         out = this->evolve_randAlltoAll_CZ_H_Player(t);
      }
      else if (inttype == "randAlltoAll_CZ_H_Pfirstlayer")
      {
         out = this->evolve_randAlltoAll_CZ_H_Pfirstlayer(t);
      }
      else if (inttype == "BetheLattice")
      {
         out = this->evolve_BetheLattice(t);
      }
      else if (inttype == "BetheLattice_Player")
      {
         deterministic=true;
         out = this->evolve_BetheLattice_Player(t);
      }
      else if (inttype == "randBetheLattice_Player")
      {
         deterministic=false;
         out = this->evolve_BetheLattice_Player(t);
      }
      else if (inttype=="H_only")
      {
         deterministic=true;
         out = this->evolve_H_only(t);
      }
      else
      {
         std::cout << inttype <<" does not exist" << std::endl;
         throw 3;
      }

      return out;
   }

   void EvolveClifford_evolve::make_sites_vec()
   {
      if (sites.size()==0)
      {
         for(int sites_i=0;sites_i<Length;sites_i++)
         {
            sites.push_back(sites_i);
         }
      }
      else if (sites.size() == Length)
      {
         // do nothing
      }
      else 
      {
         std::cout << "site vec size is wrong: " << sites.size() << "," << Length << std::endl;
         throw 4;
      }
   }

   void EvolveClifford_evolve::make_all_2_sites_vec()
   {
      if (all_2_sites.size()==0)
      {
         for(int sites_i=0;sites_i<Length;sites_i++)
         {
            for(int sites_j=(sites_i+1);sites_j<Length;sites_j++)
            {
               std::vector<int> vec_temp;
               vec_temp.push_back(sites_i);
               vec_temp.push_back(sites_j);
               all_2_sites.push_back(vec_temp);
            }
         }
      }
      else
      {
         std::cout << "site vec size is not 0" << std::endl;
         throw 4;
      }

      // std::cout << std::endl;
      // for(int i=0;i<all_2_sites.size();i++)
      // {
      //    std::cout << all_2_sites[i][0] << "," << all_2_sites[i][1] << std::endl;
      // }
      // std::cout << std::endl;
   }

   void EvolveClifford_evolve::Riffle_sites_vec()
   {
      if ((sites.size()==0) || (Length%2 != 0 ))
      {
         std::cout << "site vec size is 0" << std::endl;
         throw 4;
      }
      else
      {
         std::vector<int> out;
         for(int ind=0;ind<(Length/2);ind++)
         {
            out.push_back(sites[ind]);
            out.push_back(sites[Length/2+ind]);
         }

         if ( out.size() != sites.size() )
         {
            std::cout << "Something went wrong" << std::endl;
            throw 4;
         }
         else
         {
            sites=out;
         }
      }
   }

   void EvolveClifford_evolve::Riffle_sites_vec_reverse()
   {
      if ((sites.size()==0) || (Length%2 != 0 ))
      {
         std::cout << "site vec size is 0" << std::endl;
         throw 4;
      }
      else
      {
         std::vector<int> out;
         for(int ind=0;ind<(Length/2);ind++)
         {
            out.push_back(sites[ind*2]);
         }
         for(int ind=0;ind<(Length/2);ind++)
         {
            out.push_back(sites[ind*2+1]);
         }

         if ( out.size() != sites.size() )
         {
            std::cout << "Something went wrong" << std::endl;
            throw 4;
         }
         else
         {
            sites=out;
         }
      }
   }

   void EvolveClifford_evolve::make_cyclic_vec()
   {
      if (cyclic_site_vec.size()==0)
      {
         for(int ind=0;ind<Length;ind++)
         {
            cyclic_site_vec.push_back(ind);
         }
      }
      else
      {
         std::cout << "Cyclic site vec size is not 0" << std::endl;
         throw 4;
      }
   }

   void EvolveClifford_evolve::rotate_bits()
   {
      if(cyclic_site_vec.size()==0)
      {
         std::cout << "Cyclic site vec size is 0" << std::endl;
         throw 4;
      }
      else
      {
         for(int ind=0;ind<Length;ind++)
         {
            cyclic_site_vec[ind] = rotate_bit(cyclic_site_vec[ind],Length);
         }
      }
   }

   void EvolveClifford_evolve::set_BetheLattice(int coord_num, int depth_in)
   {
      if ((sites.size() > 0 ) ||  (depth_in < 1) )
      {
         throw 2;
      }

      coordination_number=coord_num;
      num_shells=depth_in;

      for(int ind=0;ind<coord_num+1;ind++)
      {
         sites.push_back(ind);
         site_labels.push_back(std::to_string(ind));
      }

      int tot_sites=1;
      int numsites_prev=coordination_number;
      tot_sites+=numsites_prev;

      for(int i_ind=2;i_ind<=(num_shells);i_ind++)
      {
         for(int j_ind=0;j_ind<numsites_prev;j_ind++)
         {
            for(int k_ind=0;k_ind<(coordination_number-1);k_ind++)
            {
               int currsite=tot_sites+j_ind*(coordination_number-1)+k_ind;
               sites.push_back(currsite);
               site_labels.push_back(site_labels[tot_sites-numsites_prev+j_ind]+std::to_string(k_ind));
            }
         }

         numsites_prev=numsites_prev*(coordination_number-1);
         tot_sites+=numsites_prev;
      }


      for(int i_ind=0;i_ind<coordination_number;i_ind++)
      {
         std::vector<int> site1s;
         std::vector<int> site2s;

         site1s.push_back(0);
         site2s.push_back(i_ind+1);

         for(int j_ind=2;j_ind<=(num_shells);j_ind+=2)
         {
            if (j_ind==2)
            {
               tot_sites=1;
               tot_sites+=coordination_number;
            }
            else
            {
               tot_sites+=coordination_number*intpow((coordination_number-1),j_ind-3);
               tot_sites+=coordination_number*intpow((coordination_number-1),j_ind-2);
            }

            for(int k_ind=0;k_ind<coordination_number*intpow((coordination_number-1),j_ind-1);k_ind++)
            {
               int site1=tot_sites+k_ind;
               int branch_num=k_ind%coordination_number;
               int site2=0;

               if ((branch_num+i_ind+1)==coordination_number)
               {
                  site2=tot_sites-coordination_number*intpow((coordination_number-1),j_ind-2) + k_ind/(coordination_number-1);
               }
               else
               {
                  site2=tot_sites+coordination_number*intpow((coordination_number-1),(j_ind-1))+
                     + k_ind*(coordination_number-1) 
                     + (branch_num+i_ind)%coordination_number;
               }

               if(site2<sites.size())
               {
                  site1s.push_back(site1);
                  site2s.push_back(site2);
               }
               //else
               //{
               //   std::cerr << "site2 is greater than sites.size()" << std::endl;
               //   std::cerr << site2 << " is greater than "  << sites.size() << std::endl;
               //}
            }
         }

         site1_vec.push_back(site1s);
         site2_vec.push_back(site2s);
      }
   }

   // I think this will do. But let's think about this tomorrow.

   void EvolveClifford_evolve::evolve(std::string fname_in, 
         std::string inttype_in, long long int numtimesteps_in,long long int maxnumgates_in, 
         double proj_p_in, 
         int boundary_in,bool deterministic_in,bool track_in, int numsamples)
   {
      fname=fname_in;
      inttype=inttype_in;

      deterministic=deterministic_in;

      proj_p=proj_p_in;

      boundary=boundary_in;
      
      numtimesteps=numtimesteps_in;
      maxnumgates=maxnumgates_in;

      track=track_in;
      if(track) track_entropy=track_in;
      track_entropy= track| track_tpi | track_teleport | track_mi_M;

      std::cout << "proj_p=" << proj_p << std::endl;

      if (boundary<0)
      {
         boundary=Length/2;
      }

      if (! is_initialized)
      {
         std::cerr << "initialization: something is wrong" << std::endl;
         throw 3;
      }

      if (depth > 0 )
      {
         std::cout << "depth = " << depth << std::endl;
      }

      numgates=0;
      int out=0;

      curr_state=init_state;

      auto start_time = std::chrono::high_resolution_clock::now();

      if ( evolving_sites.size() != Length ) 
      {
         for( int ind=0;ind<Length;ind++)
         {
            evolving_sites.push_back(ind);
         }
      }

      make_sites_vec();
      bool is_broken=false;

      // std::vector<int> tempvecIABM=calc_IABM(curr_state,Length,{0,int(Length/2),1,int(Length-1)},true);
      // std::cout << tempvecIABM[0] << "," ;
      // std::cout << tempvecIABM[1] << "," ;
      // std::cout << tempvecIABM[2] << "," ;
      // std::cout << tempvecIABM[3] << std::endl;

      if (save_every<0) save_every=1;

      for(long long int time_now=1;time_now<numtimesteps;time_now++)
      {
         int out = this->evolve_one_timestep(time_now);

         for(int site=0;site<Length;site++)
         {
            double curr_val = rand01(generator);
            // std::cout << curr_val << std::endl;
            if (   curr_val < proj_p )
            {
               bool phase = CliffordGates::apply_Pmeasure(curr_state,site,true);
            }
         }

         // print_vector(sites);
         if (time_now%save_every == 0)
         {
            if(track_entropy) 
            {
               out=this->append_entropy();
            }
         }

         if (out==0)
         {
            is_broken=true;
            break;
         }

         if ( time_now%100 == 0)
         {
            if( inttype != "s_random" )
            {
               std::cout << time_now <<" of "<< numtimesteps << " with " 
                  << numgates << " gates " << " applied " << std::endl << std::flush;
               auto current_time = std::chrono::high_resolution_clock::now();
               std::cout <<
                  std::chrono::duration_cast<std::chrono::seconds>(current_time - start_time).count() << " seconds past" << std::endl;
            }
            else if ( (time_now%10000) == 0)
            {
               std::cout << time_now <<" of "<< numtimesteps << " with " << numgates << " gates " << " applied " << std::endl << std::flush;
               auto current_time = std::chrono::high_resolution_clock::now();
               std::cout <<
                  std::chrono::duration_cast<std::chrono::seconds>(current_time - start_time).count() << " seconds past" << std::endl;
            }
         }


         if (saveall)
         {
            std::cout << "now saving " << time_now << std::endl;
            if (time_now%save_every == 0)
            {
               print_state_to_file(curr_state, fname+"_"+std::to_string(time_now) );
            }
         }
      }

      if (! is_broken )
      {

         int out = this->evolve_one_timestep(numtimesteps);

         // print_vector(sites);
         if(track_entropy) 
         {
            out=this->append_entropy();
         }

         if (saveall)
         {
            std::cout << "now saving " << numtimesteps << std::endl;
            print_state_to_file(curr_state, fname+"_"+std::to_string(numtimesteps) );
         }
      }

      if(track_entropy)
      {
         if (track_tpi)
         {
            std::ofstream outfile_l;
            std::ofstream outfile_p;
            std::string fname_l = fname+"_l";
            std::string fname_p = fname+"_p";

            outfile_l.open(fname_l);
            for(int ind=0; ind<tpi_ls.size(); ind++)
            {
               outfile_l << tpi_ls[ind] << std::endl;
            }
            outfile_l.close();

            outfile_p.open(fname_p);
            for(int ind=0; ind<tpi_ps.size(); ind++)
            {
               outfile_p << tpi_ps[ind] << std::endl;
            }
            outfile_p.close();
         }
         if (track_teleport |  track_mi_M )
         {
            print_intlist_to_file(IABMs,fname+"_IAMB");
         }
         if (track)
         {
            std::ofstream outfile;
            outfile.open(fname);
            for(int ind=0; ind<bes.size(); ind++)
            {
               outfile << bes[ind] << std::endl;
            }
            outfile.close();
         }
      }
      else if( purify and save )
      {
         std::ofstream outfile;
         outfile.open(fname);
         outfile << be << std::endl;
         outfile.close();
      }

      state_now=curr_state;
      tpi_l_is_negative=false;
      tpi_p_is_negative=false;
   }

   int EvolveClifford_evolve::evolve_pair(int site1, int site2)
   {
#if 0
      if (measurement_time == "after")
      {
         CliffordGates::projective_measure(curr_state,proj_p,site1,site2);
      }
#endif

      if (deterministic)
      {
         CliffordGates::apply_Hadamard(curr_state,site1);
         CliffordGates::apply_CNOT(curr_state,site1,site2);
         CliffordGates::apply_Hadamard(curr_state,site2);
         numgates+=1;
      }
      else
      {
         long numelm=(Allgates.size());
         std::uniform_int_distribution<long> intdist(0, numelm-1);
         long randnum = intdist(generator);
         if (is_YY && (! is_conj))
         {
            rand_gates_now.push_back(randnum);
         }
         else if (is_YY && is_conj) 
         {
            randnum=rand_gates_now[rand_gates_count];
            rand_gates_count++;
         }

         std::vector<std::string> gates = Allgates[randnum];

         // debug
         //
         CliffordGates::apply_RandomUnitary(curr_state,site1,site2,gates,is_conj);
         numgates+=1;
      }

#if 0
      if (deterministic && apply_phase_gate)
      {
         apply_PhaseGate(curr_state,site1);
         apply_PhaseGate(curr_state,site2);
      }

      if (measurement_time == "before")
      {
         CliffordGates::projective_measure(curr_state,proj_p,site1,site2);
      }
#endif

      return 1;
   }

   int EvolveClifford_evolve::evolve_pair_backward(int site1, int site2)
   {
      if (deterministic)
      {
         CliffordGates::apply_Hadamard(curr_state,site2);
         CliffordGates::apply_CNOT(curr_state,site1,site2);
         CliffordGates::apply_Hadamard(curr_state,site1);
         numgates+=1;
      }
      else
      {
         long numelm=(Allgates.size());
         std::uniform_int_distribution<long> intdist(0, numelm-1);
         long randnum = intdist(generator);
         if (is_YY && (! is_conj))
         {
            rand_gates_now.push_back(randnum);
         }
         else if (is_YY && is_conj) 
         {
            randnum=rand_gates_now[rand_gates_count];
            rand_gates_count++;
         }

         std::vector<std::string> gates = Allgates[randnum];
         CliffordGates::apply_RandomUnitary(curr_state,site1,site2,gates,is_conj);
         numgates+=1;
      }

      return 1;
   }

   int EvolveClifford_evolve::apply_CZ(int site1, int site2)
   {
      CliffordGates::apply_Hadamard(curr_state,site2);
      CliffordGates::apply_CNOT(curr_state,site1,site2);
      CliffordGates::apply_Hadamard(curr_state,site2);
      numgates+=1;

      return 1;
   }

   int EvolveClifford_evolve::entangle_pair(int site1, int site2)
   {
      CliffordGates::apply_Hadamard(curr_state,site1);
      CliffordGates::apply_CNOT(curr_state,site1,site2);
      CliffordGates::apply_Hadamard(curr_state,site2);
      return 1;
   }































   int EvolveClifford_evolve::evolve_PWR2layer(long long int i)
   {
      int totlayer=2*int_log2(Length) - 1;
      int half_tot=int_log2(Length);

      if( (i%totlayer) == half_tot )
      {
         int log2d=(int_log2(Length)-1);

         int d=intpow(2,log2d);

         // std::cout << i << "," << d <<  std::endl;

         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            int finished = this -> evolve_pair(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }
      else
      {
         int log2d=(i - 1)%totlayer;

         int d=intpow(2,log2d);

         int addfactor=0;

         if ( ((i-1)%totlayer) > (half_tot-1) )
         {
            log2d=((i-1)%totlayer - half_tot);
            d=intpow(2,log2d);
            addfactor=d;
         }

         // std::cout << i << "," << d <<  std::endl;

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+addfactor;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_Player(long long int timestep)
   {
      deterministic=true;

      int totlayer=2*int_log2(Length) - 1;
      int half_tot=int_log2(Length);
      int finished=1;

      if( (timestep%totlayer) == half_tot )
      {
         int log2d=(int_log2(Length)-1);

         int d=intpow(2,log2d);
         // std::cout << "d= " << d << std::endl;


         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            // int site2=(site1+d);
            if (site2 < Length)
            {
               finished = this -> evolve_pair(sites[site1],sites[site2]);
            }

            if (finished == 0)
            {
               return finished;
            }
         }

         for(int site=0;site<Length;site++)
         {
            CliffordGates::apply_PhaseGate(curr_state,sites[site]);

            if(is_conj)
            {
               CliffordGates::apply_PhaseGate(curr_state,sites[site]);
               CliffordGates::apply_PhaseGate(curr_state,sites[site]);
            }
         }
      }
      else
      {

         int log2d=(timestep - 1)%totlayer;

         int d=intpow(2,log2d);

         int addfactor=0;

         if ( ((timestep-1)%totlayer) > (half_tot-1) )
         {
            log2d=((timestep-1)%totlayer - half_tot);
            d=intpow(2,log2d);
            addfactor=d;
         }
         // std::cout << "d= " << d << "," << addfactor << std::endl;


         // std::cout << i << "," << d <<  std::endl;

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+addfactor;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               // int site2=(site1+d);

               if (site2 < Length)
               {
                  finished = this -> evolve_pair(sites[site1],sites[site2]);
               }

               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_randPWR2_layer(long long int timestep)
   {
      deterministic=false;
      int totlayer=2*int_log2(Length) - 1;
      int half_tot=int_log2(Length);
      int finished=1;

      if( (timestep%totlayer) == half_tot )
      {
         int log2d=(int_log2(Length)-1);

         int d=intpow(2,log2d);
         // std::cout << "d= " << d << std::endl;


         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            // int site2=(site1+d);
            if (site2 < Length)
            {
               finished = this -> evolve_pair(sites[site1],sites[site2]);
            }

            if (finished == 0)
            {
               return finished;
            }
         }
      }
      else
      {

         int log2d=(timestep - 1)%totlayer;

         int d=intpow(2,log2d);

         int addfactor=0;

         if ( ((timestep-1)%totlayer) > (half_tot-1) )
         {
            log2d=((timestep-1)%totlayer - half_tot);
            d=intpow(2,log2d);
            addfactor=d;
         }
         // std::cout << "d= " << d << "," << addfactor << std::endl;


         // std::cout << i << "," << d <<  std::endl;

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+addfactor;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               // int site2=(site1+d);

               if (site2 < Length)
               {
                  finished = this -> evolve_pair(sites[site1],sites[site2]);
               }

               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_Player_depth(long long int timestep)
   {
      deterministic=true;

      int totlayer=0;
      int half_tot=0;
      int finished=1;

      if ( depth >= int_log2(Length)  )
      {
         this->evolve_PWR2_Player(timestep);
      }
      else
      {
         totlayer=2*depth;
         half_tot=depth;

         int log2d=(timestep - 1)%depth;
         int d=intpow(2,log2d);

         int addfactor=0;

         if ( ((timestep -1)%totlayer) >= half_tot )
         {
            addfactor=d;
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+addfactor;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);

               // std::cout << sites[site1] << "," << sites[site2] << std::endl;
               // int site2=(site1+d);

               if (site2 < Length)
               {
                  finished = this -> evolve_pair(sites[site1],sites[site2]);
               }

               if (finished == 0)
               {
                  return finished;
               }
            }
         }

         if ((timestep-1)%totlayer == (half_tot-1))
         {
            for(int site=0;site<Length;site++)
            {
               CliffordGates::apply_PhaseGate(curr_state,sites[site]);
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_Player_backward(long long int i)
   {
      int totlayer=2*int_log2(Length) - 1;
      int half_tot=int_log2(Length);
      int finished=1;

      if( (i%totlayer) == half_tot )
      {

         for(int site=0;site<Length;site++)
         {
            CliffordGates::apply_PhaseGate(curr_state,sites[site]);

            if(! is_conj)
            {
               CliffordGates::apply_PhaseGate(curr_state,sites[site]);
               CliffordGates::apply_PhaseGate(curr_state,sites[site]);
            }
         }

         int log2d=(int_log2(Length)-1);

         int d=intpow(2,log2d);

         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            // int site2=(site1+d);
            if (site2 < Length)
            {
               finished = this -> evolve_pair_backward(sites[site1],sites[site2]);
            }

            if (finished == 0)
            {
               return finished;
            }
         }
      }
      else
      {
         int log2d=(i - 1)%totlayer;

         int d=intpow(2,log2d);

         int addfactor=0;

         if ( ((i-1)%totlayer) > (half_tot-1) )
         {
            log2d=((i-1)%totlayer - half_tot);
            d=intpow(2,log2d);
            addfactor=d;
         }

         int site0=d;
         while(site0>0)
         {
            site0--;
            for(int site=site0+addfactor;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               // int site2=(site1+d);

               if (site2 < Length)
               {
                  finished = this -> evolve_pair_backward(sites[site1],sites[site2]);
               }

               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_noH(long long int i)
   {
      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      for(int log2d=0;log2d<=(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);
         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H(long long int i)
   {
      if(i==1)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }
      }

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);

         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_double_CZ_H(long long int i)
   {
      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         for(int layernum=0;layernum<2;layernum++)
         {
            for(int site=0;site<Length;site++)
            {
               apply_Hadamard(curr_state,site);
            }

            int d=intpow(2,log2d);
            for(int site0=0;site0<d;site0++)
            {
               for(int site=site0;site<Length;site+=2*d)
               {
                  int site1=site;
                  int site2=(site1+d); // obc
                  if (site2 < Length )
                  {
                     int finished = this -> apply_CZ(site1,site2);
                     if (finished == 0)
                     {
                        return finished;
                     }
                  }
               }
            }

            for(int site0=0;site0<d;site0++)
            {
               for(int site=site0+d;site<Length;site+=2*d)
               {
                  int site1=site;
                  int site2=(site1+d); // obc
                  if (site2 < Length)
                  {
                     int finished = this -> apply_CZ(site1,site2);
                     if (finished == 0)
                     {
                        return finished;
                     }
                  }
               }
            }
         }
      }

      for(int layernum=0;layernum<2;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int log2d=(int_log2(Length)-1);

         int d=intpow(2,log2d);
         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d);

            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }


/* Riffle Operations */
   int EvolveClifford_evolve::evolve_Riffle_double_CZ_H(long long int i)
   {
      for(int log2d=0;log2d<int_log2(Length);log2d++)
      {
         // verbose
         // std::cout << std::endl;
         // std::cout << log2d << std::endl;
         // std::cout << std::endl;
         // for(int ind=0;ind<Length;ind++)
         // {
         //    std::cout << sites[ind] << std::endl;
         // }
         // std::cout << std::endl;

         for(int layernum=0;layernum<2;layernum++)
         {
            for(int site=0;site<Length;site++)
            {
               apply_Hadamard(curr_state,site);
            }

            int d=1;
            for(int site0=0;site0<d;site0++)
            {
               for(int site=site0;site<Length;site+=2*d)
               {
                  int site1=site;
                  int site2=(site1+d); // obc
                  if (site2 < Length )
                  {
                     int finished = this -> apply_CZ(sites[site1],sites[site2]);
                     if (finished == 0)
                     {
                        return finished;
                     }
                  }
               }
            }

            for(int site0=0;site0<d;site0++)
            {
               for(int site=site0+d;site<Length;site+=2*d)
               {
                  int site1=site;
                  int site2=(site1+d); // obc
                  if (site2 < Length)
                  {
                     int finished = this -> apply_CZ(sites[site1],sites[site2]);
                     if (finished == 0)
                     {
                        return finished;
                     }
                  }
               }
            }
         }
      }

      // Riffle_sites_vec();
      return 1;
   }

   int EvolveClifford_evolve::evolve_Riffle_double_CZ_H_bylayer(long long int i)
   {
      // verbose
      // std::cout << std::endl;
      // std::cout << "evolve_Riffle_double_CZ_H_bylayer: timestep " << i << std::endl;
      // std::cout << std::endl;
      // for(int sites_ind=0;sites_ind<Length;sites_ind++)
      // {
      //    std::cout << sites[sites_ind] << std::endl;
      // }
      // std::cout << std::endl;

      for(int layernum=0;layernum<2;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      Riffle_sites_vec();
      return 1;
   }

   int EvolveClifford_evolve::evolve_Riffle_double_CZ_H_P_bylayer(long long int i)
   {
      // verbose
      // std::cout << std::endl;
      // std::cout << "evolve_Riffle_double_CZ_H_bylayer: timestep " << i << std::endl;
      // std::cout << std::endl;
      // for(int sites_ind=0;sites_ind<Length;sites_ind++)
      // {
      //    std::cout << sites[sites_ind] << std::endl;
      // }
      // std::cout << std::endl;

      for(int layernum=0;layernum<2;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
         }

         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      Riffle_sites_vec();
      return 1;
   }

   int EvolveClifford_evolve::evolve_Riffle_double_H_CZ_P_bylayer(long long int i)
   {
      // verbose
      // std::cout << std::endl;
      // std::cout << "evolve_Riffle_double_CZ_H_bylayer: timestep " << i << std::endl;
      // std::cout << std::endl;
      // for(int sites_ind=0;sites_ind<Length;sites_ind++)
      // {
      //    std::cout << sites[sites_ind] << std::endl;
      // }
      // std::cout << std::endl;

      for(int layernum=0;layernum<2;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
         }
      }

      Riffle_sites_vec();
      return 1;
   }

   int EvolveClifford_evolve::evolve_Riffle_CZ_H_bylayer(long long int i)
   {
      // verbose
      // std::cout << std::endl;
      // std::cout << "evolve_Riffle_double_CZ_H_bylayer: timestep " << i << std::endl;
      // std::cout << std::endl;
      // for(int sites_ind=0;sites_ind<Length;sites_ind++)
      // {
      //    std::cout << sites[sites_ind] << std::endl;
      // }
      // std::cout << std::endl;

      for(int layernum=0;layernum<1;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      Riffle_sites_vec();
      return 1;
   }

   int EvolveClifford_evolve::evolve_Riffle_CZ_H_P_bylayer(long long int i)
   {
      // verbose
      // std::cout << std::endl;
      // std::cout << "evolve_Riffle_double_CZ_H_bylayer: timestep " << i << std::endl;
      // std::cout << std::endl;
      // for(int sites_ind=0;sites_ind<Length;sites_ind++)
      // {
      //    std::cout << sites[sites_ind] << std::endl;
      // }
      // std::cout << std::endl;

      for(int layernum=0;layernum<1;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
         }

         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      Riffle_sites_vec();
      return 1;
   }

   int EvolveClifford_evolve::evolve_Riffle_H_CZ_P_bylayer(long long int i)
   {
      // verbose
      // std::cout << std::endl;
      // std::cout << "evolve_Riffle_double_CZ_H_bylayer: timestep " << i << std::endl;
      // std::cout << std::endl;
      // for(int sites_ind=0;sites_ind<Length;sites_ind++)
      // {
      //    std::cout << sites[sites_ind] << std::endl;
      // }
      // std::cout << std::endl;

      for(int layernum=0;layernum<1;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
         }
      }

      Riffle_sites_vec();
      return 1;
   }


   int EvolveClifford_evolve::evolve_Riffle_reverse_double_CZ_H_bylayer(long long int i)
   {
      // verbose
      // std::cout << std::endl;
      // std::cout << "evolve_Riffle_double_CZ_H_bylayer: timestep " << i << std::endl;
      // std::cout << std::endl;
      // for(int sites_ind=0;sites_ind<Length;sites_ind++)
      // {
      //    std::cout << sites[sites_ind] << std::endl;
      // }
      // std::cout << std::endl;

      for(int layernum=0;layernum<2;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      Riffle_sites_vec_reverse();
      return 1;
   }

   int EvolveClifford_evolve::evolve_Riffle_reverse_double_CZ_H_P_bylayer(long long int i)
   {
      // verbose
      // std::cout << std::endl;
      // std::cout << "evolve_Riffle_double_CZ_H_bylayer: timestep " << i << std::endl;
      // std::cout << std::endl;
      // for(int sites_ind=0;sites_ind<Length;sites_ind++)
      // {
      //    std::cout << sites[sites_ind] << std::endl;
      // }
      // std::cout << std::endl;

      for(int layernum=0;layernum<2;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
         }

         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      Riffle_sites_vec_reverse();
      return 1;
   }

   int EvolveClifford_evolve::evolve_Riffle_reverse_double_H_CZ_P_bylayer(long long int i)
   {
      // verbose
      // std::cout << std::endl;
      // std::cout << "evolve_Riffle_double_CZ_H_bylayer: timestep " << i << std::endl;
      // std::cout << std::endl;
      // for(int sites_ind=0;sites_ind<Length;sites_ind++)
      // {
      //    std::cout << sites[sites_ind] << std::endl;
      // }
      // std::cout << std::endl;

      for(int layernum=0;layernum<2;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
         }
      }

      Riffle_sites_vec_reverse();
      return 1;
   }

   int EvolveClifford_evolve::evolve_Riffle_reverse_CZ_H_bylayer(long long int i)
   {
      // verbose
      // std::cout << std::endl;
      // std::cout << "evolve_Riffle_double_CZ_H_bylayer: timestep " << i << std::endl;
      // std::cout << std::endl;
      // for(int sites_ind=0;sites_ind<Length;sites_ind++)
      // {
      //    std::cout << sites[sites_ind] << std::endl;
      // }
      // std::cout << std::endl;

      for(int layernum=0;layernum<1;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      Riffle_sites_vec_reverse();
      return 1;
   }

   int EvolveClifford_evolve::evolve_Riffle_reverse_even_odd_CZ_H_bylayer(long long int i)
   {
      for(int layernum=0;layernum<1;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         int add_dfac=int(int(i-1)/int_log2(Length))%2;

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*add_dfac;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      Riffle_sites_vec_reverse();
      return 1;
   }

   int EvolveClifford_evolve::evolve_Riffle_reverse_even_odd_CZ_H_P_bylayer(long long int i)
   {
      for(int layernum=0;layernum<1;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
         }

         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         int add_dfac=int(int(i-1)/int_log2(Length))%2;

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*add_dfac;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      Riffle_sites_vec_reverse();
      return 1;
   }

   int EvolveClifford_evolve::evolve_Riffle_reverse_even_odd_CZ_H_P(long long int t)
   {
      for(int subt=0;subt<int_log2(Length);subt++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
         }

         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         int add_dfac=(t-1)%2;

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*add_dfac;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         Riffle_sites_vec_reverse();
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_Riffle_reverse_even_odd_CZ_H_P_ss_powerlaw_kac(long long int t)
   {
      double tot_weight=0;
      tot_weight+=(pow(double(Length/2.),double(ss_exp)));
      for(int temp_i=0; temp_i<(int_log2(Length)-1);temp_i++)  // 0,1,...,\log_2 N-2
      {
         double d=pow(double(2.),double(temp_i));
         tot_weight += (2*pow(d,ss_exp));
      }
      double normfac=tot_weight/2.;

      for(int subt=0;subt<int_log2(Length);subt++)
      {
         if (((t-1)%2)==1) // we skip 1/2 ranged for odd steps
         {
            if (subt == (int_log2(Length)-1)) 
            {
               Riffle_sites_vec_reverse();
               continue;
            }
         }

         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
         }

         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         int add_dfac=(t-1)%2;
         double prob = pow(pow(2.,double(subt)),double(ss_exp))/normfac;

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*add_dfac;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if ((subt != 0) && (site1 == (Length/2-1))) // avoid 
               {
                  continue;
               }

               if (site2 < Length )
               {
                  // std::cout << site1 << "," << site2 << std::endl;
                  if ( rand01(generator) < prob ) 
                  {
                     int finished = this -> apply_CZ(sites[site1],sites[site2]);
                     if (finished == 0)
                     {
                        return finished;
                     }
                  }
               }
            }
         }

         Riffle_sites_vec_reverse();
      }

      return 1;
   }


   int EvolveClifford_evolve::evolve_Riffle_reverse_double_even_odd_CZ_H_bylayer(long long int i)
   {
      for(int layernum=0;layernum<2;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         int add_dfac=int(int(i-1)/int_log2(Length))%2;

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*add_dfac;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      Riffle_sites_vec_reverse();
      return 1;
   }

   int EvolveClifford_evolve::evolve_Riffle_reverse_double_even_odd_CZ_H_P_bylayer(long long int i)
   {
      for(int layernum=0;layernum<2;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
         }

         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         int add_dfac=int(int(i-1)/int_log2(Length))%2;

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*add_dfac;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      Riffle_sites_vec_reverse();
      return 1;
   }


   int EvolveClifford_evolve::evolve_Riffle_reverse_CZ_H_P_bylayer(long long int i)
   {
      // verbose
      // std::cout << std::endl;
      // std::cout << "evolve_Riffle_double_CZ_H_bylayer: timestep " << i << std::endl;
      // std::cout << std::endl;
      // for(int sites_ind=0;sites_ind<Length;sites_ind++)
      // {
      //    std::cout << sites[sites_ind] << std::endl;
      // }
      // std::cout << std::endl;

      for(int layernum=0;layernum<1;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
         }

         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      Riffle_sites_vec_reverse();
      return 1;
   }

   int EvolveClifford_evolve::evolve_Riffle_reverse_H_CZ_P_bylayer(long long int i)
   {
      // verbose
      // std::cout << std::endl;
      // std::cout << "evolve_Riffle_double_CZ_H_bylayer: timestep " << i << std::endl;
      // std::cout << std::endl;
      // for(int sites_ind=0;sites_ind<Length;sites_ind++)
      // {
      //    std::cout << sites[sites_ind] << std::endl;
      // }
      // std::cout << std::endl;

      for(int layernum=0;layernum<1;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int d=1;
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(sites[site1],sites[site2]);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
         }
      }

      Riffle_sites_vec_reverse();
      return 1;
   }


   int EvolveClifford_evolve::evolve_PWR2_CZ_H_P(long long int i)
   {
      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {

         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
            apply_Hadamard(curr_state,site);
         }

         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

      }


      for(int site=0;site<Length;site++)
      {
         apply_PhaseGate(curr_state,site);
         apply_Hadamard(curr_state,site);
      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);

         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_NN_CZ_H_P(long long int i)
   {

      for(int site=0;site<Length;site++)
      {
         apply_PhaseGate(curr_state,site);
         apply_Hadamard(curr_state,site);
      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {

         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
            apply_Hadamard(curr_state,site);
         }

         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

      }


      for(int site=0;site<Length;site++)
      {
         apply_PhaseGate(curr_state,site);
         apply_Hadamard(curr_state,site);
      }

      log2d=(int_log2(Length)-1);

      d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);

         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_Pfirst(long long int i)
   {

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         for(int site=0;site<Length;site++)
         {
            if (i==1)
            {
               apply_PhaseGate(curr_state,site);
            }
            apply_Hadamard(curr_state,site);
         }

         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);

         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H2(long long int i)
   {
      this -> EvolveClifford_evolve::evolve_PWR2_CZ_H2(1);
      this -> EvolveClifford_evolve::evolve_PWR2_CZ_H2(2);
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_P2(long long int i)
   {
      this -> EvolveClifford_evolve::evolve_PWR2_CZ_H_P(1);
      this -> EvolveClifford_evolve::evolve_PWR2_CZ_H_P(2);
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_Pfirst2(long long int i)
   {
      this -> EvolveClifford_evolve::evolve_PWR2_CZ_H_Pfirst(1);
      this -> EvolveClifford_evolve::evolve_PWR2_CZ_H_Pfirst(2);
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_NN(long long int i)
   {
      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);

      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);

         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      // d=1
      d=1;
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_P_NN(long long int i)
   {
      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {

         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
            apply_Hadamard(curr_state,site);
         }

         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

      }


      for(int site=0;site<Length;site++)
      {
         apply_PhaseGate(curr_state,site);
         apply_Hadamard(curr_state,site);
      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);

         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_PhaseGate(curr_state,site);
         apply_Hadamard(curr_state,site);
      }


      d=1;
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_NN_NN(long long int i)
   {
      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }
         
      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);

         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      // d=1
      d=1;
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      d=1;
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_Hlayer(long long int i)
   {
      if (i==1)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }
      }

      int max_layers=int_log2(Length);

      int log2Lnow=(i-1)%max_layers;

      int d=intpow(2,log2Lnow);
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_Player(long long int i)
   {
      for(int site=0;site<Length;site++)
      {
         apply_PhaseGate(curr_state,site);
         apply_Hadamard(curr_state,site);
      }

      int max_layers=int_log2(Length);

      int log2Lnow=(i-1)%max_layers;

      int d=intpow(2,log2Lnow);
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_Pfirstlayer(long long int i)
   {
      for(int site=0;site<Length;site++)
      {
         if (i==1)
         {
            apply_PhaseGate(curr_state,site);
         }
         apply_Hadamard(curr_state,site);
      }

      int max_layers=int_log2(Length);

      int log2Lnow=(i-1)%max_layers;

      int d=intpow(2,log2Lnow);
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }



   int EvolveClifford_evolve::evolve_PWR2_ss_powerlaw_CHP(long long int i)
   {
      double normfac=1.;
      if(ss_exp>0)
      {
         normfac=pow(double(Length/2.), double(ss_exp));
      }

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp))/normfac; // d^s 

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               if ( rand01(generator) < prob ) 
               {
                  int finished = 1;
                  if(rand01(generator)<1/4.)
                  {
                     CliffordGates::apply_CNOT(curr_state,site1,site2);
                  }
                  else if(rand01(generator)<2/6.)
                  {
                     CliffordGates::apply_CNOT(curr_state,site2,site1);
                  }
                  else if(rand01(generator)<3/6.)
                  {
                     CliffordGates::apply_PhaseGate(curr_state,site1);
                  }
                  else if(rand01(generator)<4/6.)
                  {
                     CliffordGates::apply_Hadamard(curr_state,site1);
                  }
                  else if(rand01(generator)<5/6.)
                  {
                     CliffordGates::apply_PhaseGate(curr_state,site2);
                  }
                  else if(rand01(generator)<6/6.)
                  {
                     CliffordGates::apply_Hadamard(curr_state,site2);
                  }

                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      if(i%2 == 0)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp)); // d^s 

         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            if ( rand01(generator) < prob ) 
            {
               int finished = 1;
               if(rand01(generator)<1/4.)
               {
                  CliffordGates::apply_CNOT(curr_state,site1,site2);
               }
               else if(rand01(generator)<2/6.)
               {
                  CliffordGates::apply_CNOT(curr_state,site2,site1);
               }
               else if(rand01(generator)<3/6.)
               {
                  CliffordGates::apply_PhaseGate(curr_state,site1);
               }
               else if(rand01(generator)<4/6.)
               {
                  CliffordGates::apply_Hadamard(curr_state,site1);
               }
               else if(rand01(generator)<5/6.)
               {
                  CliffordGates::apply_PhaseGate(curr_state,site2);
               }
               else if(rand01(generator)<6/6.)
               {
                  CliffordGates::apply_Hadamard(curr_state,site2);
               }

               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }
      return 1;
   }







   int EvolveClifford_evolve::evolve_PWR2_ss_powerlaw(long long int i)
   {
      double normfac=1.;
      if(ss_exp>0)
      {
         normfac=pow(double(Length/2.), double(ss_exp));
      }

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp))/normfac; // d^s 

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               if ( rand01(generator) < prob ) 
               {
                  int finished = this -> evolve_pair(site1,site2); // applies two-site gates
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      if(i%2 == 0)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp)); // d^s 

         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            if ( rand01(generator) < prob ) 
            {
               int finished = this -> evolve_pair(site1,site2); // applies two-site gates
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_ss_powerlaw_kac(long long int i)
   {
      double tot_weight=0;
      tot_weight+=pow(double(Length/2.),double(ss_exp));
      for(int temp_i=0; temp_i<(int_log2(Length)-1);temp_i++)  // 0,1,...,\log_2 N-2
      {
         double d=pow(double(2.),double(temp_i));
         tot_weight += 2*pow(d,ss_exp);
      }
      double normfac=tot_weight/2.;

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp))/normfac; // d^s 

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               if ( rand01(generator) < prob ) 
               {
                  int finished = this -> evolve_pair(site1,site2); // applies two-site gates
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      if(i%2 == 0)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp))/normfac; // d^s 

         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            if ( rand01(generator) < prob ) 
            {
               int finished = this -> evolve_pair(site1,site2); // applies two-site gates
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }
      return 1;
   }


   int EvolveClifford_evolve::evolve_PWR2_ss_powerlaw_bylayer(long long int i)
   {
      double normfac=1.;
      if(ss_exp>0)
      {
         normfac=pow(double(Length/2.), double(ss_exp));
      }


      int totlayer=2*int_log2(Length) - 1;
      int half_tot=int_log2(Length);

      if( (i%totlayer) == 0)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp))/normfac; 

         // std::cout << i << " : " << d << "," << 0 << std::endl;
         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            if ( rand01(generator) < prob ) 
            {
               int finished = this -> evolve_pair(site1,site2); // applies two-site gates
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }
      else
      {
         int log2d=i%totlayer-1;
         int d=intpow(2,log2d);
         int addfactor=d;

         if ( (i%totlayer) >=half_tot)
         {
            log2d=i%totlayer - half_tot;
            d=intpow(2,log2d);
            addfactor=0;
         }

         double prob = pow(double(d),double(ss_exp))/normfac; // d^s 
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+addfactor;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               if ( rand01(generator) < prob ) 
               {
                  int finished = this -> evolve_pair(site1,site2); // applies two-site gates
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_ss_powerlaw_bylayer_kac(long long int i)
   {
      double tot_weight=0;
      tot_weight+=pow(double(Length/2.),double(ss_exp));
      for(int temp_i=0; temp_i<(int_log2(Length)-1);temp_i++)  // 0,1,...,\log_2 N-2
      {
         double d=pow(double(2.),double(temp_i));
         tot_weight += 2*pow(d,ss_exp);
      }
      double normfac=tot_weight/2.;

      int totlayer=2*int_log2(Length) - 1;
      int half_tot=int_log2(Length);
      if( (i%totlayer) == 0)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp))/normfac; 

         // std::cout << i << " : " << d << "," << 0 << std::endl;
         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            if ( rand01(generator) < prob ) 
            {
               int finished = this -> evolve_pair(site1,site2); // applies two-site gates
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }
      else
      {
         int log2d=i%totlayer-1;
         int d=intpow(2,log2d);
         int addfactor=d;

         if ( (i%totlayer) >=half_tot)
         {
            log2d=i%totlayer - half_tot;
            d=intpow(2,log2d);
            addfactor=0;
         }

         // std::cout << i << " : " << d << "," << addfactor << std::endl;
         double prob = pow(double(d),double(ss_exp))/normfac; // d^s 
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+addfactor;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               if ( rand01(generator) < prob ) 
               {
                  int finished = this -> evolve_pair(site1,site2); // applies two-site gates
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }
      return 1;
   }


   int EvolveClifford_evolve::evolve_PWR2(long long int i)
   {
      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      if(i%2 == 0)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            int finished = this -> evolve_pair(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_obc_bylayer(long long int i)
   {
      int log2d=(i-1)%int_log2(Length);
      int d=intpow(2,log2d);
      int add_dfac=int(int(i-1)/int_log2(Length))%2;

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d*add_dfac;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d);
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_obc_double_bylayer(long long int i)
   {
      int log2d=(i-1)%int_log2(Length);
      int d=intpow(2,log2d);
      int add_dfac=int(int(i-1)/int_log2(Length))%2;

      for(int numlayer=0;numlayer<2;numlayer++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*add_dfac;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d);
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_2(long long int i)
   {
      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*((i+1)%2);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      if(i%2 == 1)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            int finished = this -> evolve_pair(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_blockQ(long long int i)
   {
      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d);
               if (site2 < Length)
               {
                  int finished = this -> entangle_pair(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d);
               if (site2 < Length)
               {
                  int finished = this -> entangle_pair(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      int log2d=(int_log2(Length)-1);
      int d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d)%(Length);
         int finished = this -> entangle_pair(site1,site2);
         if (finished == 0)
         {
            return finished;
         }
      }
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2rev(long long int i)
   {

      if(i%2 == 1)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            int finished = this -> evolve_pair(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      for(int log2d=(int_log2(Length)-2);log2d>=0;log2d--)
      {
         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_depth(long long int i)
   {
      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         if (log2d < depth)
         {
            int d=intpow(2,log2d);
            for(int site0=0;site0<d;site0++)
            {
               for(int site=site0+d*(i%2);site<Length;site+=2*d)
               {
                  int site1=site;
                  int site2=(site1+d)%(Length);
                  int finished = this -> evolve_pair(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      if(i%2 == 0)
      {
         int log2d=(int_log2(Length)-1);
         if (log2d < depth) 
         {
            int d=intpow(2,log2d);
            for(int site=0;site<Length/2;site++)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
         else if (deterministic && apply_phase)
         {
            for(int site=0;site<Length;site++)
            {
               apply_PhaseGate(curr_state,site);
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_NN(long long int i)
   {
      int d=1;
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d*(i%2);site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            int finished;
            if (i%2==1)
            {
               finished = this -> evolve_pair(site1,site2);
            }
            else 
            {
               finished = this -> evolve_pair(site1,site2);
            }

            if (finished == 0)
            {
               return finished;
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_randNN_obc(long long int i)
   {
      int d=1;
      deterministic=false;

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d*((i-1)%2);site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d);
            if (site2 < Length)
            {
               int finished;
               if (i%2==1)
               {
                  finished = this -> evolve_pair(site1,site2);
               }
               else 
               {
                  finished = this -> evolve_pair(site1,site2);
               }

               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_NN_Player(long long int i)
   {
      int d=1;
      deterministic=true;

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d*((i-1)%2);site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d)%Length;
            if (site2 < Length)
            {
               int finished;
               if (i%2==1)
               {
                  finished = this -> evolve_pair(sites[site1],sites[site2]);
               }
               else 
               {
                  finished = this -> evolve_pair(sites[site1],sites[site2]);
               }

               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }


      if ( (i-1)%2 == 0 )
      {
         for(int site=0;site<Length;site++)
         {
            CliffordGates::apply_PhaseGate(curr_state,sites[site]);
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_randNN(long long int i)
   {
      int d=1;

      deterministic=false;

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d*((i-1)%2);site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d)%Length;
            if (site2 < Length)
            {
               int finished;
               if (i%2==1)
               {
                  finished = this -> evolve_pair(sites[site1],sites[site2]);
               }
               else 
               {
                  finished = this -> evolve_pair(sites[site1],sites[site2]);
               }

               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_NN_CZ_Hlayer(long long int i)
   {
      if (i==1)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }
      }

      int d=1;
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }

         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);

               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_NN_CZ_H_Player(long long int i)
   {
      for(int site=0;site<Length;site++)
      {
         apply_PhaseGate(curr_state,site);
         apply_Hadamard(curr_state,site);
      }

      int d=1;
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }

         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);

               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_NN_CZ_H_Pfirstlayer(long long int i)
   {
      for(int site=0;site<Length;site++)
      {
         if (i==1)
         {
            apply_PhaseGate(curr_state,site);
         }

         apply_Hadamard(curr_state,site);
      }

      int d=1;
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }

         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);

               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }


   int EvolveClifford_evolve::evolve_NNAA(long long int i)
   {
      for(int d=1;d<(Length/2);d++)
      {
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<(d*(Length/d));site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      if(i%2 == 0)
      {
         for(int site0=0;site0<1;site0++)
         {
            for(int site=site0;site<Length; site+=2*1)
            {
               int site1=site;
               int site2=(site1+1)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }

         for(int d=1; d<Length/2;d++)
         {
            for(int site=d*(Length/d);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }

         for(int site0=0;site0<1;site0++)
         {
            for(int site=site0+1;site<Length; site+=2*1)
            {
               int site1=site;
               int site2=(site1+1)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }

         for( int site=0; site<Length/2;site++)
         {
            int site1=site;
            int site2=(site+Length/2)%(Length);
            int finished = this -> evolve_pair(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_AA(long long int i)
   {
      for(int d=1;d<(Length/2);d++)
      {
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<(d*((Length)/d));site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      if(i%2 == 0)
      {
         for(int d=1; d<Length/2;d++)
         {
            for(int site=d*((Length)/d);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }

         for( int site=0; site<Length/2;site++)
         {
            int site1=site;
            int site2=(site+Length/2)%(Length);
            int finished = this -> evolve_pair(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }
      return 1;
   }

   int EvolveClifford_evolve::evolve_sqL(long long int i)
   {
      for(int d=1;d<std::sqrt(Length);d++)
      {
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<(d*((Length)/d));site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      if(i%2 == 0)
      {
         for(int d=1; d<std::sqrt(Length);d++)
         {
            for(int site=d*((Length)/d);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }

         if (deterministic && apply_phase)
         {
            for(int site=0;site<Length;site++)
            {
               apply_PhaseGate(curr_state,site);
            }
         }
      }
      return 1;
   }

   int EvolveClifford_evolve::evolve_randAlltoAll(long long int i)
   {

      if ( (i==1) && (deterministic) )
      {
         std::cerr << "evolve_randAlltoAll: warning, Qgates will be applied" << std::endl;
      }
      std::vector<int> curr_sites=evolving_sites;
      std::shuffle ( curr_sites.begin(), curr_sites.end(), generator);
      // std::cout << "site vector contains:";

      // for (std::vector<int>::iterator it=curr_sites.begin(); it!=curr_sites.end(); ++it) std::cout << ' ' << *it;

      if (Length%2 != 0 )
      {
         std::cout << "Length " << Length << "must be even integer " << std::endl;
         throw 1;
      }

      for(int i=0;i<Length;i+=2)
      {
         int site1 = curr_sites[i];
         int site2 = curr_sites[i+1];
         int finished = this -> evolve_pair(site1,site2);
         if (finished == 0)
         {
            return finished;
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_randAlltoAll_powerlaw_kac(long long int t_local)
   {
      int maxtime=Length-1;
      for(int temp_i=0; temp_i<maxtime;temp_i++)
      {
         double tot_weight=0;
         tot_weight+=pow(double(Length/2.),double(ss_exp));
         for(int d=1; d<(Length/2);d++)  // d=1,2,...,L/2-1
         {
            tot_weight += 2*pow(double(d),double(ss_exp));
         }
         double normfac=tot_weight;

         if ( (t_local==1) && (deterministic) )
         {
            std::cerr << "evolve_randAlltoAll: warning, Qgates will be applied" << std::endl;
         }
         std::vector<int> curr_sites=evolving_sites;
         std::shuffle ( curr_sites.begin(), curr_sites.end(),generator);

         if (Length%2 != 0 )
         {
            std::cout << "Length " << Length << "must be even integer " << std::endl;
            throw 1;
         }

         for(int ind=0;ind<Length;ind+=2)
         {
            int site1 = curr_sites[ind];
            int site2 = curr_sites[ind+1];
            double d = std::min(std::abs(site2-site1),Length-std::abs(site2-site1));
            double prob = pow(double(d),double(ss_exp))/normfac;
            if ( rand01(generator) < prob ) 
            {
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }



   int EvolveClifford_evolve::evolve_s_random(long long int i)
   {
      std::uniform_int_distribution<int> intdistm(0,2);
      std::uniform_int_distribution<int> intdistLength(0,Length-1);
      std::uniform_int_distribution<int> intdistcomb(0,(Length*(Length-1)/2)-1);
      int m = intdistm(generator);

      int site1;
      int site2;

      if (m==0)
      {
         site1 = intdistLength(generator);
         apply_Hadamard(curr_state,site1);
      }
      else if (m==1)
      {
         site1 = intdistLength(generator);
         apply_PhaseGate(curr_state,site1);
      }
      else if (m==2)
      {
         int comb_num = intdistcomb(generator);
         site1 = all_2_sites[comb_num][0];
         site2 = all_2_sites[comb_num][1];
         this -> apply_CZ(site1,site2);
      }
      else
      {
         std::cout << "evolve_s_random something is wrong" << std::endl;
         throw 5;
      }
      

      return 1;
   }

   int EvolveClifford_evolve::evolve_randAlltoAll_CZ_Hlayer(long long int i)
   {
      if (i==1)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }
      }

      std::vector<int> curr_sites=evolving_sites;
      std::shuffle ( curr_sites.begin(), curr_sites.end(), generator);
      // std::cout << "site vector contains:";

      // for (std::vector<int>::iterator it=curr_sites.begin(); it!=curr_sites.end(); ++it) std::cout << ' ' << *it;

      if (Length%2 != 0 )
      {
         std::cout << "Length " << Length 
            << "must be even integer " << std::endl;
         throw 1;
      }

      for(int i=0;i<Length;i+=2)
      {
         int site1 = curr_sites[i];
         int site2 = curr_sites[i+1];
         int finished = this -> apply_CZ(site1,site2);

         if (finished == 0)
         {
            return finished;
         }
      }

      for(int i=1;i<Length;i+=2)
      {
         int site1 = curr_sites[i];
         if ( (i+1) < Length ) // obc
         {
            int site2 = curr_sites[i+1];
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_randAlltoAll_CZ_H_Player(long long int i)
   {
      for(int site=0;site<Length;site++)
      {
         apply_PhaseGate(curr_state,site);
         apply_Hadamard(curr_state,site);
      }

      std::vector<int> curr_sites=evolving_sites;
      std::shuffle ( curr_sites.begin(), curr_sites.end(), generator);
      // std::cout << "site vector contains:";

      // for (std::vector<int>::iterator it=curr_sites.begin(); it!=curr_sites.end(); ++it) std::cout << ' ' << *it;

      if (Length%2 != 0 )
      {
         std::cout << "Length " << Length 
            << "must be even integer " << std::endl;
         throw 1;
      }

      for(int i=0;i<Length;i+=2)
      {
         int site1 = curr_sites[i];
         int site2 = curr_sites[i+1];
         int finished = this -> apply_CZ(site1,site2);

         if (finished == 0)
         {
            return finished;
         }
      }

      for(int i=1;i<Length;i+=2)
      {
         int site1 = curr_sites[i];
         if ( (i+1) < Length ) // obc
         {
            int site2 = curr_sites[i+1];
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_randAlltoAll_CZ_H_Pfirstlayer(long long int i)
   {
      for(int site=0;site<Length;site++)
      {
         if (i==1)
         {
            apply_PhaseGate(curr_state,site);
         }
         apply_Hadamard(curr_state,site);
      }

      std::vector<int> curr_sites=evolving_sites;
      std::shuffle ( curr_sites.begin(), curr_sites.end(),generator);
      // std::cout << "site vector contains:";

      // for (std::vector<int>::iterator it=curr_sites.begin(); it!=curr_sites.end(); ++it) std::cout << ' ' << *it;

      if (Length%2 != 0 )
      {
         std::cout << "Length " << Length 
            << "must be even integer " << std::endl;
         throw 1;
      }

      for(int i=0;i<Length;i+=2)
      {
         int site1 = curr_sites[i];
         int site2 = curr_sites[i+1];
         int finished = this -> apply_CZ(site1,site2);

         if (finished == 0)
         {
            return finished;
         }
      }

      for(int i=1;i<Length;i+=2)
      {
         int site1 = curr_sites[i];
         if ( (i+1) < Length ) // obc
         {
            int site2 = curr_sites[i+1];
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_BetheLattice(long long int i)
   {
      for(int sites_i=0;sites_i<site1_vec.size();sites_i++)
      {
         for(int sites_j=0;sites_j<site1_vec[sites_i].size();sites_j++)
         {
            int site1 = site1_vec[sites_i][sites_j];
            int site2 = site2_vec[sites_i][sites_j];
            int finished = this -> evolve_pair(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      if (deterministic && apply_phase)
      {
         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_BetheLattice_Player(long long int i)
   {
      int sites_i=(i-1)%site1_vec.size();
      // std::cout << "now evolving: " << sites_i << " , " << site1_vec.size() << std::endl;

      for(int sites_j=0;sites_j<site1_vec[sites_i].size();sites_j++)
      {
         int site1 = site1_vec[sites_i][sites_j];
         int site2 = site2_vec[sites_i][sites_j];
         // std::cout << site1 << " , " << site2 << std::endl; 
         int finished = this -> evolve_pair(site1,site2);
         if (finished == 0)
         {
            return finished;
         }
      }

      if (sites_i==(site1_vec.size()-1))
      {
         // std::cout << "applying phase gate" << std::endl;
         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_H_only(long long int i)
   {
      std::cout << "applying at time "<< i << std::endl;
      for(int site=0;site<Length;site++)
      {
         std::cout << sites[site] << std::endl;
         apply_Hadamard(curr_state,sites[site]);
      }

      return 1;
   }

   int EvolveClifford_evolve::append_entropy()
   {
      int out=0;
      if(track)
      {
         be = CliffordGates::get_entropy_comp(curr_state,Length_Orig,boundary);
         // std::cout << Length_Orig << "," << boundary << "," << be << std::endl;
         bes.push_back(be);
         if ((be < 1) && purify)
         {
            return out=0;
         }
      }

      if (track_tpi)
      {
         int tpi_l = CliffordGates::get_tpi_l(curr_state,Length_Orig,subsyssize);
         int tpi_p = CliffordGates::get_tpi_p(curr_state,Length_Orig,subsyssize,2);
         // int tpi_p = CliffordGates::get_tpi_p(curr_state,Length_Orig);
         // bes.push_back(max(tpi_l,tpi_p));
         tpi_ls.push_back(tpi_l);
         tpi_ps.push_back(tpi_p);

         if (tpi_l < 0) {tpi_l_is_negative=true;}
         if (tpi_p < 0) {tpi_p_is_negative=true;}

         if (stop_negative_tpi && (tpi_l_is_negative && tpi_p_is_negative))
         {
            return out=0;
         }
      }

      if (track_teleport)
      {
         std::vector<int> maxgdisl={0,1,1,3,7,13,29,53,117,213,469,853};
         std::vector<int> maxgdisp={0,1,1,3,7,11,23,43,87,171,343,683};
         IABMs.push_back(calc_IABM(curr_state,Length,{0,int(Length/2),1,int(Length-1),
                  maxgdisl[CliffordGates::int_log2(Length)],
                  maxgdisp[CliffordGates::int_log2(Length)]
                  }));
      }

      if (track_mi_M)
      {
         IABMs.push_back(calc_IABM(curr_state,Length,mi_M_sites));
      }
      return out=1;
   }
}
