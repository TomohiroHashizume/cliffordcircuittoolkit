#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>
#include <random>
#include <stdio.h>
#include <string>
#include "monnamap.h"
#include "EvolveClifford.h"

namespace CliffordGates {
   class EvolveClifford_evolve : public EvolveClifford
   {
      public :
         EvolveClifford_evolve(){} ;
         EvolveClifford_evolve(int Length_in,std::string init_in, int seed_in=-1, bool verbose_in=0)
         {
            init=init_in;
            Length=Length_in;
            Length_Orig=Length_in;
            seed=seed_in;
            verbose=verbose_in;
            this->initialize();
         }

         void evolve(std::string fname_in, std::string inttype_in, long long int numtimesteps_in, long long int maxnumgates_in, 
               double proj_p_in, 
               int boundary_in,bool deterministic_in,bool track_in, int numsamples=1000);
         void toggle_apply_phase(bool apply_phase_in){ apply_phase=apply_phase_in; }
         void toggle_purify(bool purify_in){purify=purify_in; }
         void toggle_save(bool save_in){save=save_in; }
         void toggle_saveall(bool saveall_in){saveall=saveall_in; }
         void set_save_every(int save_every_in){save_every=save_every_in; }

         void set_depth(int depth_in) { depth=depth_in; }
         void set_BetheLattice(int coord_num, int depth_in);
         void set_ss_exp(double ss_exp_in){ ss_exp=ss_exp_in; }
         
         void set_track_teleport(bool track_teleport_in){track_teleport=track_teleport_in;}
         void set_track_tpi(bool track_tpi_in){track_tpi=track_tpi_in;}
         void set_subsyssize(bool subsyssize_in){subsyssize=subsyssize_in;}
         void set_track_mi_M(bool track_mi_M_in);
         void set_track_mi_M_monna(bool track_mi_M_in);

         void set_stop_negative_tpi(bool stop_negative_tpi_in ){stop_negative_tpi=stop_negative_tpi_in;track_tpi=true;}

      protected :
         int INF=(unsigned)!((int)0);

         int evolve_one_timestep(long long int t);
         int evolve_pair(int site1,int site2);
         int evolve_pair_backward(int site1,int site2);
         int entangle_pair(int site1,int site2);
         int apply_CZ(int site1,int site2);

         int evolve_PWR2_ss_powerlaw_CHP(long long int i);
         int evolve_PWR2_ss_powerlaw(long long int i);
         int evolve_PWR2_ss_powerlaw_kac(long long int i);
         int evolve_PWR2_ss_powerlaw_bylayer(long long int i);
         int evolve_PWR2_ss_powerlaw_bylayer_kac(long long int i);

         int evolve_PWR2(long long int i);
         int evolve_PWR2_obc_bylayer(long long int i);
         int evolve_PWR2_obc_double_bylayer(long long int i);

         int evolve_PWR2_2(long long int timestep); // half length at the end of i=1

         int evolve_PWR2layer(long long int timestep);
         int evolve_PWR2_Player(long long int timestep);
         int evolve_randPWR2_layer(long long int timestep);
         int evolve_PWR2_Player_depth(long long int timestep);
         int evolve_PWR2_Player_backward(long long int timestep);

         int evolve_PWR2rev(long long int timestep);

         int evolve_PWR2_CZ_noH(long long int i); // open boundary condition
         int evolve_PWR2_CZ_H(long long int i); // open boundary condition
         int evolve_PWR2_double_CZ_H(long long int i); // open boundary condition

         int evolve_Riffle_double_CZ_H(long long int i); // open boundary condition

         int evolve_Riffle_double_CZ_H_bylayer(long long int i); // open boundary condition
         int evolve_Riffle_double_CZ_H_P_bylayer(long long int i); // open boundary condition
         int evolve_Riffle_double_H_CZ_P_bylayer(long long int i);

         int evolve_Riffle_reverse_double_CZ_H_bylayer(long long int i); // open boundary condition
         int evolve_Riffle_reverse_double_CZ_H_P_bylayer(long long int i); // open boundary condition
         int evolve_Riffle_reverse_double_H_CZ_P_bylayer(long long int i);

         int evolve_Riffle_CZ_H_bylayer(long long int i); // open boundary condition
         int evolve_Riffle_CZ_H_P_bylayer(long long int i); // open boundary condition
         int evolve_Riffle_H_CZ_P_bylayer(long long int i);

         int evolve_Riffle_reverse_CZ_H_bylayer(long long int i); // open boundary condition
         int evolve_Riffle_reverse_even_odd_CZ_H_bylayer(long long int i); // open boundary condition
         int evolve_Riffle_reverse_even_odd_CZ_H_P_bylayer(long long int i); // open boundary condition
         int evolve_Riffle_reverse_even_odd_CZ_H_P_ss_powerlaw_kac(long long int i); // open boundary condition

         int evolve_Riffle_reverse_even_odd_CZ_H_P(long long int i); // open boundary condition
         int evolve_Riffle_reverse_double_even_odd_CZ_H_bylayer(long long int i); // open boundary condition
         int evolve_Riffle_reverse_double_even_odd_CZ_H_P_bylayer(long long int i); // open boundary condition
         int evolve_Riffle_reverse_CZ_H_P_bylayer(long long int i); // open boundary condition
         int evolve_Riffle_reverse_H_CZ_P_bylayer(long long int i);
         
         int evolve_PWR2_CZ_H_P(long long int i); // open boundary condition
         int evolve_PWR2_NN_CZ_H_P(long long int i); // open boundary condition
         int evolve_PWR2_CZ_H_P_NN(long long int i); // open boundary condition
         int evolve_PWR2_CZ_H_Pfirst(long long int i); // open boundary condition
         int evolve_PWR2_CZ_H_P2(long long int i); // open boundary condition
         int evolve_PWR2_CZ_H_Pfirst2(long long int i); // open boundary condition
         int evolve_PWR2_CZ_H2(long long int i); // open boundary condition
         int evolve_PWR2_CZ_H_NN(long long int i); // open boundary condition
         int evolve_PWR2_CZ_H_NN_NN(long long int i); // open boundary condition
         int evolve_PWR2_CZ_Hlayer(long long int i); // open boundary condition
         int evolve_PWR2_CZ_H_Player(long long int i); // open boundary condition
         int evolve_PWR2_CZ_H_Pfirstlayer(long long int i); // open boundary condition

         int evolve_PWR2_blockQ(long long int i); // open boundary condition

         int evolve_NNAA(long long int i);

         int evolve_randNN_obc(long long int i);
         int evolve_randNN(long long int i);
         int evolve_NN(long long int i);
         int evolve_NN_Player(long long int i);
         int evolve_NN_CZ_Hlayer(long long int i);
         int evolve_NN_CZ_H_Player(long long int i);
         int evolve_NN_CZ_H_Pfirstlayer(long long int i);

         int evolve_AA(long long int i);
         int evolve_sqL(long long int i);
         int evolve_depth(long long int i);

         int evolve_s_random(long long int i);
         int evolve_randAlltoAll(long long int i);
         int evolve_randAlltoAll_powerlaw_kac(long long int i);

         int evolve_randAlltoAll_CZ_Hlayer(long long int i);
         int evolve_randAlltoAll_CZ_H_Player(long long int i);
         int evolve_randAlltoAll_CZ_H_Pfirstlayer(long long int i);

         int evolve_BetheLattice(long long int i);
         int evolve_BetheLattice_Player(long long int i);

         int evolve_H_only(long long int i); // open boundary condition

         void make_sites_vec();
         void make_all_2_sites_vec();
         void Riffle_sites_vec();
         void Riffle_sites_vec_reverse();
         void make_cyclic_vec();
         void rotate_bits();

         std::vector<std::vector<bool>> curr_state;

         std::string fname;
         std::string inttype;

         bool deterministic=false;
         bool apply_phase=false;

         bool track_entropy=false;
         int  append_entropy();

         bool track=false;
         bool track_tpi=false;

         bool tpi_l_is_negative=false;
         bool tpi_p_is_negative=false;
         int  subsyssize=-1;

         bool track_teleport=false;
         bool stop_negative_tpi=false;

         bool track_mi_M=false;
         std::vector<int> mi_M_sites;

         bool purify=false;
         bool save=true;
         int save_every=-1;
         bool saveall=false;

         bool is_YY=false;
         bool is_conj=false;

         std::vector<int> bes;
         std::vector<int> tpi_ls;
         std::vector<int> tpi_ps;

         std::vector<std::vector<int>> IABMs;

         double proj_p=0;
         double flip_p=0;

         std::string measurement_time="before";
         int boundary;

         long long int numtimesteps;
         long long int maxnumgates;

         int outcount=0;

         long long int numgates=0;
         int be=INF;

         std::ofstream outfile;

         // depth params: 
         int depth = -1;

         // Bethe Lattice params: 
         int coordination_number; 
         int num_shells;

         // powerlaw params
         double ss_exp=0.;

         std::vector<int> rand_gates_now;
         int rand_gates_count=0;

         std::vector<std::string> site_labels;
         std::vector<int> evolving_sites;
         std::vector<int> sites;
         std::vector<std::vector<int>> all_2_sites;

         std::vector<std::vector<int>> site1_vec;
         std::vector<std::vector<int>> site2_vec;

         std::vector<int> cyclic_site_vec;
   }
   ;
}
