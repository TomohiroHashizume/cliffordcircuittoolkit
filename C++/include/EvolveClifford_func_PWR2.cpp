#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <string>
#include "EvolveClifford_evolve.h"

namespace CliffordGates {
   int EvolveClifford_evolve::evolve_PWR2layer(long long int i)
   {
      int totlayer=2*int_log2(Length) - 1;
      int half_tot=int_log2(Length);

      if( (i%totlayer) == half_tot )
      {
         int log2d=(int_log2(Length)-1);

         int d=intpow(2,log2d);

         // std::cout << i << "," << d <<  std::endl;

         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            int finished = this -> evolve_pair(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }
      else
      {
         int log2d=(i - 1)%totlayer;

         int d=intpow(2,log2d);

         int addfactor=0;

         if ( ((i-1)%totlayer) > (half_tot-1) )
         {
            log2d=((i-1)%totlayer - half_tot);
            d=intpow(2,log2d);
            addfactor=d;
         }

         // std::cout << i << "," << d <<  std::endl;

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+addfactor;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_Player(long long int timestep)
   {
      deterministic=true;

      int totlayer=2*int_log2(Length) - 1;
      int half_tot=int_log2(Length);
      int finished=1;

      if( (timestep%totlayer) == half_tot )
      {
         int log2d=(int_log2(Length)-1);

         int d=intpow(2,log2d);
         // std::cout << "d= " << d << std::endl;


         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            // int site2=(site1+d);
            if (site2 < Length)
            {
               finished = this -> evolve_pair(sites[site1],sites[site2]);
            }

            if (finished == 0)
            {
               return finished;
            }
         }

         for(int site=0;site<Length;site++)
         {
            CliffordGates::apply_PhaseGate(curr_state,sites[site]);

            if(is_conj)
            {
               CliffordGates::apply_PhaseGate(curr_state,sites[site]);
               CliffordGates::apply_PhaseGate(curr_state,sites[site]);
            }
         }
      }
      else
      {

         int log2d=(timestep - 1)%totlayer;

         int d=intpow(2,log2d);

         int addfactor=0;

         if ( ((timestep-1)%totlayer) > (half_tot-1) )
         {
            log2d=((timestep-1)%totlayer - half_tot);
            d=intpow(2,log2d);
            addfactor=d;
         }
         // std::cout << "d= " << d << "," << addfactor << std::endl;


         // std::cout << i << "," << d <<  std::endl;

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+addfactor;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               // int site2=(site1+d);

               if (site2 < Length)
               {
                  finished = this -> evolve_pair(sites[site1],sites[site2]);
               }

               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_randPWR2_layer(long long int timestep)
   {
      deterministic=false;
      int totlayer=2*int_log2(Length) - 1;
      int half_tot=int_log2(Length);
      int finished=1;

      if( (timestep%totlayer) == half_tot )
      {
         int log2d=(int_log2(Length)-1);

         int d=intpow(2,log2d);
         // std::cout << "d= " << d << std::endl;


         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            // int site2=(site1+d);
            if (site2 < Length)
            {
               finished = this -> evolve_pair(sites[site1],sites[site2]);
            }

            if (finished == 0)
            {
               return finished;
            }
         }
      }
      else
      {

         int log2d=(timestep - 1)%totlayer;

         int d=intpow(2,log2d);

         int addfactor=0;

         if ( ((timestep-1)%totlayer) > (half_tot-1) )
         {
            log2d=((timestep-1)%totlayer - half_tot);
            d=intpow(2,log2d);
            addfactor=d;
         }
         // std::cout << "d= " << d << "," << addfactor << std::endl;


         // std::cout << i << "," << d <<  std::endl;

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+addfactor;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               // int site2=(site1+d);

               if (site2 < Length)
               {
                  finished = this -> evolve_pair(sites[site1],sites[site2]);
               }

               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_Player_depth(long long int timestep)
   {
      deterministic=true;

      int totlayer=0;
      int half_tot=0;
      int finished=1;

      if ( depth >= int_log2(Length)  )
      {
         this->evolve_PWR2_Player(timestep);
      }
      else
      {
         totlayer=2*depth;
         half_tot=depth;

         int log2d=(timestep - 1)%depth;
         int d=intpow(2,log2d);

         int addfactor=0;

         if ( ((timestep -1)%totlayer) >= half_tot )
         {
            addfactor=d;
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+addfactor;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);

               // std::cout << sites[site1] << "," << sites[site2] << std::endl;
               // int site2=(site1+d);

               if (site2 < Length)
               {
                  finished = this -> evolve_pair(sites[site1],sites[site2]);
               }

               if (finished == 0)
               {
                  return finished;
               }
            }
         }

         if ((timestep-1)%totlayer == (half_tot-1))
         {
            for(int site=0;site<Length;site++)
            {
               CliffordGates::apply_PhaseGate(curr_state,sites[site]);
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_Player_backward(long long int i)
   {
      int totlayer=2*int_log2(Length) - 1;
      int half_tot=int_log2(Length);
      int finished=1;

      if( (i%totlayer) == half_tot )
      {

         for(int site=0;site<Length;site++)
         {
            CliffordGates::apply_PhaseGate(curr_state,sites[site]);

            if(! is_conj)
            {
               CliffordGates::apply_PhaseGate(curr_state,sites[site]);
               CliffordGates::apply_PhaseGate(curr_state,sites[site]);
            }
         }

         int log2d=(int_log2(Length)-1);

         int d=intpow(2,log2d);

         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            // int site2=(site1+d);
            if (site2 < Length)
            {
               finished = this -> evolve_pair_backward(sites[site1],sites[site2]);
            }

            if (finished == 0)
            {
               return finished;
            }
         }
      }
      else
      {
         int log2d=(i - 1)%totlayer;

         int d=intpow(2,log2d);

         int addfactor=0;

         if ( ((i-1)%totlayer) > (half_tot-1) )
         {
            log2d=((i-1)%totlayer - half_tot);
            d=intpow(2,log2d);
            addfactor=d;
         }

         int site0=d;
         while(site0>0)
         {
            site0--;
            for(int site=site0+addfactor;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               // int site2=(site1+d);

               if (site2 < Length)
               {
                  finished = this -> evolve_pair_backward(sites[site1],sites[site2]);
               }

               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_noH(long long int i)
   {
      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      for(int log2d=0;log2d<=(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);
         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H(long long int i)
   {
      if(i==1)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }
      }

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);

         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_double_CZ_H(long long int i)
   {
      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         for(int layernum=0;layernum<2;layernum++)
         {
            for(int site=0;site<Length;site++)
            {
               apply_Hadamard(curr_state,site);
            }

            int d=intpow(2,log2d);
            for(int site0=0;site0<d;site0++)
            {
               for(int site=site0;site<Length;site+=2*d)
               {
                  int site1=site;
                  int site2=(site1+d); // obc
                  if (site2 < Length )
                  {
                     int finished = this -> apply_CZ(site1,site2);
                     if (finished == 0)
                     {
                        return finished;
                     }
                  }
               }
            }

            for(int site0=0;site0<d;site0++)
            {
               for(int site=site0+d;site<Length;site+=2*d)
               {
                  int site1=site;
                  int site2=(site1+d); // obc
                  if (site2 < Length)
                  {
                     int finished = this -> apply_CZ(site1,site2);
                     if (finished == 0)
                     {
                        return finished;
                     }
                  }
               }
            }
         }
      }

      for(int layernum=0;layernum<2;layernum++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         int log2d=(int_log2(Length)-1);

         int d=intpow(2,log2d);
         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d);

            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }


   int EvolveClifford_evolve::evolve_PWR2_CZ_H_P(long long int i)
   {
      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {

         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
            apply_Hadamard(curr_state,site);
         }

         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

      }


      for(int site=0;site<Length;site++)
      {
         apply_PhaseGate(curr_state,site);
         apply_Hadamard(curr_state,site);
      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);

         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_NN_CZ_H_P(long long int i)
   {

      for(int site=0;site<Length;site++)
      {
         apply_PhaseGate(curr_state,site);
         apply_Hadamard(curr_state,site);
      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {

         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
            apply_Hadamard(curr_state,site);
         }

         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

      }


      for(int site=0;site<Length;site++)
      {
         apply_PhaseGate(curr_state,site);
         apply_Hadamard(curr_state,site);
      }

      log2d=(int_log2(Length)-1);

      d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);

         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_Pfirst(long long int i)
   {

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         for(int site=0;site<Length;site++)
         {
            if (i==1)
            {
               apply_PhaseGate(curr_state,site);
            }
            apply_Hadamard(curr_state,site);
         }

         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);

         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H2(long long int i)
   {
      this -> EvolveClifford_evolve::evolve_PWR2_CZ_H2(1);
      this -> EvolveClifford_evolve::evolve_PWR2_CZ_H2(2);
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_P2(long long int i)
   {
      this -> EvolveClifford_evolve::evolve_PWR2_CZ_H_P(1);
      this -> EvolveClifford_evolve::evolve_PWR2_CZ_H_P(2);
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_Pfirst2(long long int i)
   {
      this -> EvolveClifford_evolve::evolve_PWR2_CZ_H_Pfirst(1);
      this -> EvolveClifford_evolve::evolve_PWR2_CZ_H_Pfirst(2);
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_NN(long long int i)
   {
      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);

      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);

         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      // d=1
      d=1;
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_P_NN(long long int i)
   {
      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {

         for(int site=0;site<Length;site++)
         {
            apply_PhaseGate(curr_state,site);
            apply_Hadamard(curr_state,site);
         }

         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

      }


      for(int site=0;site<Length;site++)
      {
         apply_PhaseGate(curr_state,site);
         apply_Hadamard(curr_state,site);
      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);

         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_PhaseGate(curr_state,site);
         apply_Hadamard(curr_state,site);
      }


      d=1;
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_NN_NN(long long int i)
   {
      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length )
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d); // obc
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }
         
      }

      int log2d=(int_log2(Length)-1);

      int d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d);

         if (site2 < Length)
         {
            int finished = this -> apply_CZ(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      // d=1
      d=1;
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      d=1;
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_Hlayer(long long int i)
   {
      if (i==1)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }
      }

      int max_layers=int_log2(Length);

      int log2Lnow=(i-1)%max_layers;

      int d=intpow(2,log2Lnow);
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_Player(long long int i)
   {
      for(int site=0;site<Length;site++)
      {
         apply_PhaseGate(curr_state,site);
         apply_Hadamard(curr_state,site);
      }

      int max_layers=int_log2(Length);

      int log2Lnow=(i-1)%max_layers;

      int d=intpow(2,log2Lnow);
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_CZ_H_Pfirstlayer(long long int i)
   {
      for(int site=0;site<Length;site++)
      {
         if (i==1)
         {
            apply_PhaseGate(curr_state,site);
         }
         apply_Hadamard(curr_state,site);
      }

      int max_layers=int_log2(Length);

      int log2Lnow=(i-1)%max_layers;

      int d=intpow(2,log2Lnow);
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length )
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d); // obc
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }


   int EvolveClifford_evolve::evolve_PWR2_ss_powerlaw_CHP(long long int i)
   {
      double normfac=1.;
      if(ss_exp>0)
      {
         normfac=pow(double(Length/2.), double(ss_exp));
      }

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp))/normfac; // d^s 

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               if ( (std::rand()/(RAND_MAX + 1.)) < prob ) 
               {
                  int finished = 1;
                  if((std::rand()/(RAND_MAX + 1.))<1/4.)
                  {
                     CliffordGates::apply_CNOT(curr_state,site1,site2);
                  }
                  else if((std::rand()/(RAND_MAX + 1.))<2/6.)
                  {
                     CliffordGates::apply_CNOT(curr_state,site2,site1);
                  }
                  else if((std::rand()/(RAND_MAX + 1.))<3/6.)
                  {
                     CliffordGates::apply_PhaseGate(curr_state,site1);
                  }
                  else if((std::rand()/(RAND_MAX + 1.))<4/6.)
                  {
                     CliffordGates::apply_Hadamard(curr_state,site1);
                  }
                  else if((std::rand()/(RAND_MAX + 1.))<5/6.)
                  {
                     CliffordGates::apply_PhaseGate(curr_state,site2);
                  }
                  else if((std::rand()/(RAND_MAX + 1.))<6/6.)
                  {
                     CliffordGates::apply_Hadamard(curr_state,site2);
                  }

                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      if(i%2 == 0)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp)); // d^s 

         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            if ( (std::rand()/(RAND_MAX + 1.)) < prob ) 
            {
               int finished = 1;
               if((std::rand()/(RAND_MAX + 1.))<1/4.)
               {
                  CliffordGates::apply_CNOT(curr_state,site1,site2);
               }
               else if((std::rand()/(RAND_MAX + 1.))<2/6.)
               {
                  CliffordGates::apply_CNOT(curr_state,site2,site1);
               }
               else if((std::rand()/(RAND_MAX + 1.))<3/6.)
               {
                  CliffordGates::apply_PhaseGate(curr_state,site1);
               }
               else if((std::rand()/(RAND_MAX + 1.))<4/6.)
               {
                  CliffordGates::apply_Hadamard(curr_state,site1);
               }
               else if((std::rand()/(RAND_MAX + 1.))<5/6.)
               {
                  CliffordGates::apply_PhaseGate(curr_state,site2);
               }
               else if((std::rand()/(RAND_MAX + 1.))<6/6.)
               {
                  CliffordGates::apply_Hadamard(curr_state,site2);
               }

               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }
      return 1;
   }


   int EvolveClifford_evolve::evolve_PWR2_ss_powerlaw(long long int i)
   {
      double normfac=1.;
      if(ss_exp>0)
      {
         normfac=pow(double(Length/2.), double(ss_exp));
      }

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp))/normfac; // d^s 

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               if ( (std::rand()/(RAND_MAX + 1.)) < prob ) 
               {
                  int finished = this -> evolve_pair(site1,site2); // applies two-site gates
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      if(i%2 == 0)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp)); // d^s 

         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            if ( (std::rand()/(RAND_MAX + 1.)) < prob ) 
            {
               int finished = this -> evolve_pair(site1,site2); // applies two-site gates
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_ss_powerlaw_kac(long long int i)
   {
      double tot_weight=0;
      tot_weight+=pow(double(Length/2.),double(ss_exp));
      for(int temp_i=0; temp_i<(int_log2(Length)-1);temp_i++)  // 0,1,...,\log_2 N-2
      {
         double d=pow(double(2.),double(temp_i));
         tot_weight += 2*pow(d,ss_exp);
      }
      double normfac=tot_weight/2.;

      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp))/normfac; // d^s 

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               if ( (std::rand()/(RAND_MAX + 1.)) < prob ) 
               {
                  int finished = this -> evolve_pair(site1,site2); // applies two-site gates
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      if(i%2 == 0)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp))/normfac; // d^s 

         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            if ( (std::rand()/(RAND_MAX + 1.)) < prob ) 
            {
               int finished = this -> evolve_pair(site1,site2); // applies two-site gates
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }
      return 1;
   }


   int EvolveClifford_evolve::evolve_PWR2_ss_powerlaw_bylayer(long long int i)
   {
      double normfac=1.;
      if(ss_exp>0)
      {
         normfac=pow(double(Length/2.), double(ss_exp));
      }


      int totlayer=2*int_log2(Length) - 1;
      int half_tot=int_log2(Length);

      if( (i%totlayer) == 0)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp))/normfac; 

         // std::cout << i << " : " << d << "," << 0 << std::endl;
         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            if ( (std::rand()/(RAND_MAX + 1.)) < prob ) 
            {
               int finished = this -> evolve_pair(site1,site2); // applies two-site gates
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }
      else
      {
         int log2d=i%totlayer-1;
         int d=intpow(2,log2d);
         int addfactor=0;

         if ( (i%totlayer) >=half_tot)
         {
            log2d=i%totlayer - half_tot;
            d=intpow(2,log2d);
            addfactor=d;
         }

         double prob = pow(double(d),double(ss_exp))/normfac; // d^s 
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+addfactor;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               if ( (std::rand()/(RAND_MAX + 1.)) < prob ) 
               {
                  int finished = this -> evolve_pair(site1,site2); // applies two-site gates
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_ss_powerlaw_bylayer_kac(long long int i)
   {
      double tot_weight=0;
      tot_weight+=pow(double(Length/2.),double(ss_exp));
      for(int temp_i=0; temp_i<(int_log2(Length)-1);temp_i++)  // 0,1,...,\log_2 N-2
      {
         double d=pow(double(2.),double(temp_i));
         tot_weight += 2*pow(d,ss_exp);
      }

      double normfac=tot_weight/2.;

      int totlayer=2*int_log2(Length) - 1;
      int half_tot=int_log2(Length);
      if( (i%totlayer) == 0)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         double prob = pow(double(d),double(ss_exp))/normfac; 

         // std::cout << i << " : " << d << "," << 0 << std::endl;
         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            if ( (std::rand()/(RAND_MAX + 1.)) < prob ) 
            {
               int finished = this -> evolve_pair(site1,site2); // applies two-site gates
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }
      else
      {
         int log2d=i%totlayer-1;
         int d=intpow(2,log2d);
         int addfactor=0;

         if ( (i%totlayer) >=half_tot)
         {
            log2d=i%totlayer - half_tot;
            d=intpow(2,log2d);
            addfactor=d;
         }

         // std::cout << i << " : " << d << "," << addfactor << std::endl;
         double prob = pow(double(d),double(ss_exp))/normfac; // d^s 
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+addfactor;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               if ( (std::rand()/(RAND_MAX + 1.)) < prob ) 
               {
                  int finished = this -> evolve_pair(site1,site2); // applies two-site gates
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }
      return 1;
   }


   int EvolveClifford_evolve::evolve_PWR2(long long int i)
   {
      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      if(i%2 == 0)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            int finished = this -> evolve_pair(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_obc_bylayer(long long int i)
   {
      int log2d=(i-1)%int_log2(Length);
      int d=intpow(2,log2d);
      int add_dfac=int(int(i-1)/int_log2(Length))%2;

      for(int site=0;site<Length;site++)
      {
         apply_Hadamard(curr_state,site);
      }

      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d*add_dfac;site<Length;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d);
            if (site2 < Length)
            {
               int finished = this -> apply_CZ(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_obc_double_bylayer(long long int i)
   {
      int log2d=(i-1)%int_log2(Length);
      int d=intpow(2,log2d);
      int add_dfac=int(int(i-1)/int_log2(Length))%2;

      for(int numlayer=0;numlayer<2;numlayer++)
      {
         for(int site=0;site<Length;site++)
         {
            apply_Hadamard(curr_state,site);
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*add_dfac;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d);
               if (site2 < Length)
               {
                  int finished = this -> apply_CZ(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_2(long long int i)
   {
      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*((i+1)%2);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      if(i%2 == 1)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            int finished = this -> evolve_pair(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2_blockQ(long long int i)
   {
      for(int log2d=0;log2d<(int_log2(Length)-1);log2d++)
      {
         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d);
               if (site2 < Length)
               {
                  int finished = this -> entangle_pair(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }

         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d;site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d);
               if (site2 < Length)
               {
                  int finished = this -> entangle_pair(site1,site2);
                  if (finished == 0)
                  {
                     return finished;
                  }
               }
            }
         }
      }

      int log2d=(int_log2(Length)-1);
      int d=intpow(2,log2d);
      for(int site=0;site<Length/2;site++)
      {
         int site1=site;
         int site2=(site1+d)%(Length);
         int finished = this -> entangle_pair(site1,site2);
         if (finished == 0)
         {
            return finished;
         }
      }
      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2rev(long long int i)
   {

      if(i%2 == 1)
      {
         int log2d=(int_log2(Length)-1);
         int d=intpow(2,log2d);
         for(int site=0;site<Length/2;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length);
            int finished = this -> evolve_pair(site1,site2);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      for(int log2d=(int_log2(Length)-2);log2d>=0;log2d--)
      {
         int d=intpow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<Length;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      return 1;
   }

}
