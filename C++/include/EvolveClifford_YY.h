#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <string>

namespace CliffordGates {
   class EvolveClifford_YY : public EvolveClifford_evolve
   {
      public :
         EvolveClifford_YY(){};
         EvolveClifford_YY(int Length_in, int seed_in=0, int verbose_in=0)
         {
            init="Z";
            Length=Length_in;
            seed=seed_in;
            verbose=verbose_in;
            this->initialize();
         }

         void evolve_yy(std::string inttype_in, long int numtimesteps_in, long int maxnumgates_in, 
               double proj_p_in, int boundary_in, bool deterministic_in, bool track_in);
         void make_EPRpairs_yy(int numalice_in);
         void decode(int numsamples);
         void output_stab_phases(std::string fname){ print_stab_phases_to_file(stab_phases,fname); }
         void toggle_half(bool half_in){half=half_in;}

      private :

         bool half=false;
         void make_measurements();

         void add_to_sites_vec(int offset);
         void reverse_parity();
         void reverse_parity_to_original();

         int numalice=1;
         std::vector<std::vector<bool>> stab_phases;
   };

   void EvolveClifford_YY::evolve_yy( std::string inttype_in, long int numtimesteps_in,long int maxnumgates_in, 
         double proj_p_in, int boundary_in,bool deterministic_in,bool track_in)
   {
      inttype=inttype_in;
      deterministic=deterministic_in;
      proj_p=proj_p_in;
      boundary=boundary_in;
      numtimesteps=numtimesteps_in;
      maxnumgates=maxnumgates_in;
      track=track_in;
      is_YY=true;

      if (! is_initialized)
      {
         std::cerr << "initialization: something is wrong" << std::endl;
         throw 3;
      }

      std::cout << "proj_p=" << proj_p << std::endl;


      if (depth > 0)
      {
         std::cout << "depth = " << depth << std::endl;
      }

      if (boundary<=0)
      {
         boundary=Length/2;
      }

      numgates=0;
      int out=0;

      curr_state=init_state;

      auto start_time = std::chrono::high_resolution_clock::now();

      int Length_Orig=Length;

      Length = Length/2 - numalice;

      this->make_sites_vec();
      this->add_to_sites_vec(numalice);

      for(long int i=1;i<numtimesteps;i++)
      {
         rand_gates_count=0;
         rand_gates_now.clear();

         is_conj=false;
         out = this->evolve_one_timestep(i);
         this->reverse_parity();

         is_conj=true;
         out = this->evolve_one_timestep(i);
         this -> make_measurements();

         this->reverse_parity_to_original();

         if ( inttype.substr(0,14) == "Riffle_reverse" )
         {
            Riffle_sites_vec_reverse();
         }
         else if ( inttype.substr(0,6) == "Riffle" )
         {
            Riffle_sites_vec();
         }

         if ( i%100 == 0)
         {
            std::cout << i <<" of "<< numtimesteps << " with " << numgates << " gates " << " applied " << std::endl << std::flush;
            auto current_time = std::chrono::high_resolution_clock::now();
            std::cout <<
               std::chrono::duration_cast<std::chrono::seconds>(current_time - start_time).count() << " seconds past" << std::endl;
         }
      }

      long int i = numtimesteps;
      if(i > 0)
      {
         rand_gates_count=0;
         rand_gates_now.clear();

         is_conj=false;
         out = this->evolve_one_timestep(i);
         this->reverse_parity();

         is_conj=true;
         out = this->evolve_one_timestep(i);
         this -> make_measurements();

         this->reverse_parity_to_original();

         if ( inttype.substr(0,14) == "Riffle_reverse" )
         {
            Riffle_sites_vec_reverse();
         }
         else if ( inttype.substr(0,6) == "Riffle" )
         {
            Riffle_sites_vec();
         }
         is_conj=false;
      }

      Length = (Length+numalice)*2;
      state_now=curr_state;
      print_state(state_now);
   }

   // may be I should call it encode.
   void EvolveClifford_YY::make_EPRpairs_yy(int numalice_in=1)
   {
      numalice=numalice_in;
      for(int site0=0;site0<Length;site0++)
      {
         apply_Hadamard(init_state,site0);
         apply_PhaseGate(init_state,site0);
         apply_PhaseGate(init_state,site0);
         apply_Hadamard(init_state,site0);
      }

      for(int site0=2*numalice; site0<(Length/2);site0++)
      {
         CliffordGates::apply_Hadamard(init_state,site0);
         CliffordGates::apply_CNOT(init_state,site0,Length-1-2*numalice-site0);
         apply_Hadamard( init_state,Length-1-2*numalice-site0);
         apply_PhaseGate(init_state,Length-1-2*numalice-site0);
         apply_PhaseGate(init_state,Length-1-2*numalice-site0);
         apply_Hadamard( init_state,Length-1-2*numalice-site0);
      }

      for(int site0=0;site0<numalice;site0++)
      {
         CliffordGates::apply_Hadamard(init_state,site0);
         CliffordGates::apply_CNOT(init_state,site0,site0+numalice);
         apply_Hadamard( init_state,site0+numalice);
         apply_PhaseGate(init_state,site0+numalice);
         apply_PhaseGate(init_state,site0+numalice);
         apply_Hadamard( init_state,site0+numalice);

         CliffordGates::apply_Hadamard(init_state,Length-1-site0);
         CliffordGates::apply_CNOT(init_state,Length-1-site0,Length-1-site0-numalice);
         apply_Hadamard( init_state,Length-1-site0-numalice);
         apply_PhaseGate(init_state,Length-1-site0-numalice);
         apply_PhaseGate(init_state,Length-1-site0-numalice);
         apply_Hadamard( init_state,Length-1-site0-numalice);
      }

      state_now=init_state;
   }

   void EvolveClifford_YY::decode(int numsamples)
   {
      std::vector<std::vector<bool>> stab_phases_temp;

      for(int site0=2*numalice; site0<(Length/2);site0++)
      {
         apply_Hadamard( state_now,Length-1-2*numalice-site0);
         apply_PhaseGate(state_now,Length-1-2*numalice-site0);
         apply_PhaseGate(state_now,Length-1-2*numalice-site0);
         apply_Hadamard( state_now,Length-1-2*numalice-site0);
         CliffordGates::apply_CNOT(state_now,site0,Length-1-2*numalice-site0);
         CliffordGates::apply_Hadamard(state_now,site0);
      }

      for(int sample=0; sample<numsamples;sample++)
      {
         std::vector<bool> temp;
         std::vector<std::vector<bool>> measured_state = state_now;

         for(int site0=0;site0<Length;site0++)
         {
            temp.push_back(CliffordGates::apply_Pmeasure(measured_state,site0,true));
         }
         
         stab_phases_temp.push_back(temp);
      }
      stab_phases=stab_phases_temp;
   }

   void EvolveClifford_YY::reverse_parity()
   {
      if (sites.size() != Length )
      {
         std::cout << "sites.size " << sites.size() << " != " << Length  << " !" << std::endl;
         throw 3;
      }

      for(int ind=0;ind<sites.size() ;ind++)
      {
         sites[ind] = sites[ind] + Length;
      }

      std::vector<int> temp;
      
      int ind=sites.size();
      while(ind>0)
      {
         ind--;
         temp.push_back(sites[ind]);
      }
      sites=temp;
#if 0
#endif
   }

   void EvolveClifford_YY::reverse_parity_to_original()
   {
      if (sites.size() != Length )
      {
         std::cout << "sites.size " << sites.size() << " != " << Length  << " !" << std::endl;
         throw 3;
      }

      std::vector<int> temp;
      
      int ind=sites.size();
      while(ind>0)
      {
         ind--;
         temp.push_back(sites[ind]);
      }
      sites=temp;
#if 0
#endif

      for(int ind=0;ind<sites.size();ind++)
      {
         sites[ind] = sites[ind] - Length;
      }
   }

   void EvolveClifford_YY::add_to_sites_vec(int offset)
   {
      for(int ind=0;ind<sites.size();ind++)
      {
         sites[ind] = sites[ind] + offset;
      }
   }

   void EvolveClifford_YY::make_measurements()
   {
      int maxlen=2*Length;
      if (half)
      {
         maxlen=Length;
      }

      for(int site=0;site<maxlen;site++)
      {
         if ( (std::rand()/(RAND_MAX + 1.))  < proj_p )
         {
            CliffordGates::apply_Pmeasure(curr_state,numalice+site,true);
         }
      }
   }
}
