#include <algorithm>
#include <chrono>
#include <cmath>
#include <fstream>
#include <math.h>
#include <stdio.h>
#include <random>
#include <string>
#include <sstream>
#include <cstdlib>
#include <vector>
#include "EvolveClifford_Util.h"

extern "C" void dgesvd_( char* jobu, char* jobvt, int* m, int* n, double* a,
      int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt,
      double* work, int* lwork, int* info );

namespace CliffordGates {
   class EvolveClifford
   {
      public :
         EvolveClifford(){} ;
         EvolveClifford(int Length_in,std::string init_in,int seed_in=1,int verbose_in=0)
            : init(init_in),Length(Length_in), Length_Orig(Length_in), seed(seed_in), verbose(verbose_in)
         {
            Length=Length_in;
            Length_Orig=Length_in;
            this->initialize();
         }

         void export_state(std::string fname){ print_state_to_file(state_now,fname); }
         std::vector<std::vector<bool>> get_state(){ return state_now; }
         std::string get_state_string(){ return print_state_to_string(state_now); }

         void thermalize();
         void make_EPRpairs(int num_pairs_in=0);
         void make_EPRpairs_uudd(int num_pairs_in=0);

         void initialize_state(std::string init_state_str );
         void toggle_is_yy(bool is_yy_in) { is_yy=is_yy_in; }

      protected :
         void initialize();
         void initialize_state();

         bool check_maximally_entangled();

         std::vector<std::string> Allone;
         std::vector<std::string> Alltwo;
         std::vector<std::vector<std::string>> Allgates;

         int verbose;

         int seed;
         std::mt19937 generator; 
         std::uniform_real_distribution<double> rand01{0.,1.};

         int Length;
         int Length_Orig;
         std::string init;

         bool is_initialized=false;
         bool is_yy=false;

         int num_pairs=0;

         std::vector<std::vector<bool>> init_state;
         std::vector<std::vector<bool>> state_now;
   }
   ;
}
