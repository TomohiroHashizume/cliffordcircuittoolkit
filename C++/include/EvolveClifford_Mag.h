#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <string>

namespace CliffordGates {
   void print_mags_to_file(std::vector<std::vector<int>> &mags_out, std::string fname="mag.out")
   {
      std::ofstream outfile;
      outfile.open(fname);
      for(int i=0; i<mags_out.size(); i++)
      {
         for(int j=0; j<(mags_out[i].size()-1); j++)
         {
            outfile << mags_out[i][j] << "," ;
         }
         outfile << mags_out[i][mags_out[i].size()-1] << std::endl;
      }
      outfile.close();
   }

   class EvolveClifford_Mag : public EvolveClifford_evolve
   {
      public :
         EvolveClifford_Mag(){}
         EvolveClifford_Mag(int Length_in,std::string init_in,int seed_in=0,int verbose_in=0)
         {
            init=init_in;
            Length=Length_in;
            seed=seed_in;
            verbose=verbose_in;
            this->initialize();
         }

         void evolve_mag(std::string inttype_in, long int numtimesteps_in, long int maxnumgates_in, 
               double proj_p_in, double flip_p_in,
               int boundary_in, bool deterministic_in,bool track_in);
         void output_mag_measures(std::string fname){ print_mags_to_file(mags,fname); }

      private :
         void make_measurements();
         std::vector<std::vector<int>> mags;
   };

   void EvolveClifford_Mag::evolve_mag( std::string inttype_in, long int numtimesteps_in,long int maxnumgates_in, 
         double proj_p_in, double flip_p_in, int boundary_in, bool deterministic_in, bool track_in)
   {
      inttype=inttype_in;
      deterministic=deterministic_in;
      proj_p=proj_p_in;
      flip_p=flip_p_in;
      boundary=boundary_in;
      numtimesteps=numtimesteps_in;
      maxnumgates=maxnumgates_in;
      track=track_in;

      std::cout << "proj_p=" << proj_p << std::endl;
      std::cout << "flip_p=" << flip_p << std::endl;

      if (boundary<=0)
      {
         boundary=Length/2;
      }

      if (! is_initialized)
      {
         std::cerr << "initialization: something is wrong" << std::endl;
         throw 3;
      }

      if (depth > 0 )
      {
         std::cout << "depth = " << depth << std::endl;
      }

      numgates=0;
      int out=0;

      curr_state=init_state;

      auto start_time = std::chrono::high_resolution_clock::now();

      int Length_Orig=Length;

      for(long int i=1;i<numtimesteps;i++)
      {
         out = this->evolve_one_timestep(i);
         this -> make_measurements();

         if ( i%100 == 0)
         {
            std::cout << i <<" of "<< numtimesteps << " with " << numgates << " gates " << " applied " << std::endl << std::flush;
            auto current_time = std::chrono::high_resolution_clock::now();
            std::cout <<
               std::chrono::duration_cast<std::chrono::seconds>(current_time - start_time).count() << " seconds past" << std::endl;

            std::vector<int> mags_now;
            std::vector<std::vector<bool>> state_measured=curr_state;
            for(int site=0; site<Length; site++)
            {
               mags_now.push_back(int(CliffordGates::apply_Pmeasure(state_measured,site,true)));
            }
            mags.push_back(mags_now);
         }

      }

      out = this->evolve_one_timestep(numtimesteps);

      std::vector<int> mags_now;
      std::vector<std::vector<bool>> state_measured=curr_state;
      for(int site=0; site<Length; site++)
      {
         mags_now.push_back(int(CliffordGates::apply_Pmeasure(state_measured,site,true)));
      }

      mags.push_back(mags_now);
      state_now=curr_state;
   }

   void EvolveClifford_Mag::make_measurements()
   {
      for(int site=0;site<Length;site++)
      {
         if ( rand01(generator)  < proj_p )
         {
            bool phase = CliffordGates::apply_Pmeasure(curr_state,site,true);
            if (phase)
            {
               if ( rand01(generator) < flip_p )
               {
                  CliffordGates::flip(curr_state,site);
               }
            }
         }
      }
   }
}
