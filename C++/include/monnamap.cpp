#include <chrono>
#include <cmath>
#include <fstream>
#include <math.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include <cstdlib>
#include <vector>
#include <monnamap.h>

namespace MonnaMap {
   std::vector<int> b_nary(int i,int b,int maxpow)
   {
      std::vector<int> out;
      int k=int(pow(int(b),int(maxpow)));
      while(k > 0)
      {
         out.push_back(int(i/k));
         i = int(i%k);
         k = int(k/b);
      }
      return out;
   }

   int inverse_b_nary(std::vector<int> vec,int b,int maxpow)
   {
      int out=0;
      for(int i=0;i<maxpow+1;i++)
      {
         out += vec[i]*int(pow(b,i));
      }
      return out ;
   }

   int intlog(int num,int b)
   {
      int count=0;
      int k=b;
      while(k <= num)
      {
         k *= b;
         count++;
      }
      return count ;
   }

   std::vector<int> monnamap(int L,int b)
   {
      int maxpow=MonnaMap::intlog(L-1,b);

      std::vector<int>b_naryvec;
      std::vector<std::vector<int>>b_naryvecs;
      for(int i=0;i<L;i++)
      {
         b_naryvec=b_nary(i,b,maxpow);
         b_naryvecs.push_back(b_naryvec);
      }

      std::vector<int> out;
      for(int i=0;i<b_naryvecs.size();i++)
      {
         out.push_back(inverse_b_nary(b_naryvecs[i],b,maxpow));
      }
      return out;
   }
}
