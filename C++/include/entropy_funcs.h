#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <filesystem>
#include <fmt/core.h>
#include <fstream>
#include <iostream>
#include <limits.h>
#include <math.h>
#include <sys/stat.h>
#include <../../CliffordSimulation_Toolkit/C/include/EvolveClifford.h>

unsigned long long int nchoosek(int n, int k)
{
   int upper=1;
   int lower=1;

   // log(n!) ~ n log n - n 
   // log( nchoosek (n,k) )  = log(n!) - log(k!) - log((n-k)!) 
   //
   // n log n - n - k log k + k - (n-k)*log(n-k) +  n + k
   //
   // nlog n - klogk - (n-k)log(n-k) + 2k 
   //
   //

   if (n==0)
   {
      return 1;
   }

   if (k==0)
   {
      return 1;
   }

   if (k==1)
   {
      return n;
   }
   else if ( (n < 30) || (k < 3 ) )
   {
      for(int i=0;i<k;i++)
      {
         upper*=(n-i);
      }

      for(int i=0;i<k;i++)
      {
         lower*=(1+i);
      }

      return upper/lower;
   }
   else
   {
      double log10choosek=(double(n)*std::log( double(n) ) 
            - double(k)*std::log(double(k))  - double(n-k)*std::log(double(n-k)) 
            + 2.*double(k))/std::log(10.);

      if (log10choosek > 20)
      {
         return ULLONG_MAX;
      }
      else 
      {
         for(int i=0;i<k;i++)
         {
            upper*=(n-i);
         }

         for(int i=0;i<k;i++)
         {
            lower*=(1+i);
         }

         return upper/lower;
      }
   }
}

std::string numpy_array2string(std::vector<int> subsystem)
{
   std::string str_out="[";
   for(int i=0;i<subsystem.size()-1; i++)
   {
      if ( subsystem[subsystem.size()-1] >= 100 )
      {
         str_out += fmt::v7::format("{0:3d}",subsystem[i]) + " ";
      }
      else if (subsystem[subsystem.size()-1] >= 10)
      {
         str_out += fmt::v7::format("{0:2d}",subsystem[i]) + " ";
      }
      else
      {
         str_out += fmt::v7::format("{0:1d}",subsystem[i]) + " ";
      }
   }

   str_out += std::to_string(subsystem[subsystem.size()-1]) + "]";
   return str_out;
}

int callback(void *NotUsed, int argc, char **argv, char **azColName){
   return 0;
}

int make_table(std:: string dbname)
{
   sqlite3 *db;
   int rc; 
   char *zErrMsg = 0;

   std::string sql;
   rc = sqlite3_open_v2(dbname.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX , nullptr);

   /* Create SQL statement */

   sql=  "CREATE TABLE IF NOT EXISTS ENTROPY(" \
   "L                 INTEGER       NOT NULL," \
   "PROJ_PTAG         INTEGER       NOT NULL," \
   "INTTYPE           INTEGER       NOT NULL," \
   "MEASUREMENT_TIME  TEXT          NOT NULL," \
   "INIT_STATE        TEXT          NOT NULL," \
   "DURATION          INTEGER       NOT NULL," \
   "TRAJ              INTEGER       NOT NULL," \
   "LSUB              INTEGER       NOT NULL," \
   "NUMCONF           INTEGER       NOT NULL," \
   "MIN               INTEGER       NOT NULL," \
   "MAX               INTEGER       NOT NULL," \
   "MEAN              REAL          NOT NULL," \
   "VAR               REAL          NOT NULL," \
   "STD               REAL          NOT NULL);";

   /* Execute SQL statement */
   rc = sqlite3_exec(db, sql.c_str(),callback,0,&zErrMsg);
   sqlite3_close_v2(db);
   return 0;
}

int make_table_temp(std:: string dbname)
{
   sqlite3 *db;
   int rc; 
   char *zErrMsg = 0;

   std::string sql;
   rc = sqlite3_open_v2(dbname.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX , nullptr);

   /* Create SQL statement */

   sql="CREATE TABLE IF NOT EXISTS ENTROPY("   \
   "L                 INTEGER       NOT NULL," \
   "PROJ_PTAG         INTEGER       NOT NULL," \
   "INTTYPE           TEXT          NOT NULL," \
   "MEASUREMENT_TIME  TEXT          NOT NULL," \
   "INIT_STATE        TEXT          NOT NULL," \
   "LSUB              INTEGER       NOT NULL," \
   "SUBSYSTEM         TEXT          NOT NULL," \
   "ENTROPY           INTEGER       NOT NULL);";

   /* Execute SQL statement */
   rc = sqlite3_exec(db, sql.c_str(),callback,0,&zErrMsg);
   sqlite3_close_v2(db);
   return 0;
}

bool check_entropy_exist_temp(
      std::string dbname,
      int L, int proj_ptag, std::string inttype, 
      std::string measurement_time, std::string init_state, 
      int lsub, std::string subsystemstr)
{
   sqlite3 *db;
   int rc; 
   char *zErrMsg = 0;

   std::string sql;
   sqlite3_stmt* pStmt=NULL;

   rc = sqlite3_open_v2(dbname.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX , nullptr);
   int iResult = SQLITE_ERROR;

   /* Create SQL statement */
   sql = "SELECT ENTROPY FROM ENTROPY WHERE "  \
   "L=? AND "  \
   "PROJ_PTAG=?        AND "  \
   "INTTYPE=?          AND "  \
   "MEASUREMENT_TIME=? AND "  \
   "INIT_STATE=?       AND "  \
   "LSUB=?             AND "  \
   "SUBSYSTEM=?; ";

   iResult = sqlite3_prepare_v2(db,sql.c_str(), -1, &pStmt, nullptr);

   sqlite3_bind_int64(pStmt,1,L);
   sqlite3_bind_int64(pStmt,2,proj_ptag);
   sqlite3_bind_text(pStmt,3,inttype.c_str(),inttype.size(),SQLITE_STATIC);
   sqlite3_bind_text(pStmt,4,measurement_time.c_str(),measurement_time.size(),SQLITE_STATIC);
   sqlite3_bind_text(pStmt,5,init_state.c_str(),init_state.size(),SQLITE_STATIC);
   sqlite3_bind_int64(pStmt,6,lsub);
   sqlite3_bind_text(pStmt,7,subsystemstr.c_str(),subsystemstr.size(),SQLITE_STATIC);

   bool out=false;
   if (sqlite3_step(pStmt) == SQLITE_ROW )
   {
      // std::string state_str= (const char *) sqlite3_column_blob(pStmt, 0);
      // std::cout << state_str << std::endl;
      out=true;
   }
   else 
   {
      out=false;
   }

   sqlite3_finalize(pStmt);
   sqlite3_close_v2(db);
   return out;
}

void insert_entropy_to_table_temp(std::string dbname,
      int L, int proj_ptag, std::string inttype, std::string measurement_time,std::string init_state,int lsub,
      std::string subsystemstr, int entropy)
{
   sqlite3 *db;
   int rc; 
   char *zErrMsg = 0;

   std::string sql;
   sqlite3_stmt* pStmt=NULL;

   rc = sqlite3_open_v2(dbname.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX , nullptr);
   int iResult = SQLITE_ERROR;

   /* Create SQL statement */
   sql = "INSERT INTO ENTROPY(L,PROJ_PTAG,INTTYPE,MEASUREMENT_TIME,INIT_STATE,LSUB,SUBSYSTEM,ENTROPY) "\
          "VALUES(?,?,?,?,?,?,?,?);";

   iResult = sqlite3_prepare_v2(db,sql.c_str(), -1, &pStmt, nullptr);

   sqlite3_bind_int64(pStmt,1,L);
   sqlite3_bind_int64(pStmt,2,proj_ptag);
   sqlite3_bind_text(pStmt,3,inttype.c_str(),inttype.size(),SQLITE_STATIC);
   sqlite3_bind_text(pStmt,4,measurement_time.c_str(),measurement_time.size(),SQLITE_STATIC);
   sqlite3_bind_text(pStmt,5,init_state.c_str(),init_state.size(),SQLITE_STATIC);
   sqlite3_bind_int64(pStmt,6,lsub);
   sqlite3_bind_text(pStmt,7,subsystemstr.c_str(),subsystemstr.size(),SQLITE_STATIC);
   sqlite3_bind_int64(pStmt,8,entropy);

   while(sqlite3_step(pStmt) == SQLITE_BUSY){}

   sqlite3_finalize(pStmt);
   sqlite3_close_v2(db);
}

std::string get_state_from_table(
      std::string dbname, 
      int L, int proj_ptag, std::string inttype, 
      std::string measurement_time, std::string init_state,int traj)
{
   std::string state="";
   sqlite3 *db;
   int rc; 
   char *zErrMsg = 0;

   std::string sql;
   sqlite3_stmt* pStmt=NULL;

   rc = sqlite3_open_v2(dbname.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX , nullptr);
   int iResult = SQLITE_ERROR;

   /* Create SQL statement */
   sql = "SELECT STATE FROM STATES WHERE "  \
   "L=? AND "  \
   "PROJ_PTAG=?        AND "  \
   "INTTYPE=?          AND "  \
   "MEASUREMENT_TIME=? AND "  \
   "INIT_STATE=?       AND "  \
   "TRAJ=?; ";

   iResult = sqlite3_prepare_v2(db,sql.c_str(), -1, &pStmt, nullptr);

   sqlite3_bind_int64(pStmt,1,L);
   sqlite3_bind_int64(pStmt,2,proj_ptag);
   sqlite3_bind_text(pStmt,3,inttype.c_str(),inttype.size(),SQLITE_STATIC);
   sqlite3_bind_text(pStmt,4,measurement_time.c_str(),measurement_time.size(),SQLITE_STATIC);
   sqlite3_bind_text(pStmt,5,init_state.c_str(),init_state.size(),SQLITE_STATIC);
   sqlite3_bind_int64(pStmt,6,traj);

   bool out=false;
   if (sqlite3_step(pStmt) == SQLITE_ROW )
   {
      state = (const char *) sqlite3_column_blob(pStmt, 0);
   }

   sqlite3_finalize(pStmt);
   sqlite3_close_v2(db);

   return state;
}

void get_code_params(std::string fdir, std::string dbname_state,std::string dbname_temp,int L,
      int proj_ptag,std::string inttype,int lsub,int duration,int traj,
      std::string measurement_time, std::string init_state)
{
   auto start_time = std::chrono::high_resolution_clock::now();

   std::string state=get_state_from_table(dbname_state,L,proj_ptag,inttype,measurement_time,init_state,traj);
   if (state.size() > 0)
   {
      std::vector<std::vector<int>> state_temp=CliffordGates::from_string_to_state(state);
      std::vector<std::vector<int>> dat = CliffordGates::get_stabilizer(state_temp);

      int numtrajmax=20000;
      std::cout << "L=" << L << ", Lsub=" << lsub << std::endl;

      if (nchoosek(L,lsub) < numtrajmax)
      {
         std::cout << "with " << nchoosek(L,lsub) << " configurations" << std::endl;
         std::vector<bool> v(L);
         std::fill(v.end() - lsub, v.end(), true);

         std::vector<std::vector<int>> all_config;
         do
         {
            std::vector<int> curr_config;
            for(int i =0; i<L; ++i)
            {
               if(v[i])
               {
                  curr_config.push_back(i+1);
               }
            }
            all_config.push_back(curr_config);
         }
         while (std::next_permutation(v.begin(), v.end()));

         for (int configtraj=0;configtraj<nchoosek(L,lsub) ;configtraj++)
         {
            if (( configtraj % 100 ) == 0 )
            {

               std::cout << "traj=" << configtraj << std::endl << std::flush;
               auto current_time = std::chrono::high_resolution_clock::now();
               std::cout << std::chrono::duration_cast<std::chrono::seconds>(current_time - start_time).count() 
                  << " seconds elapsed" << std::endl;
            }


            std::vector<int> subsystem=all_config[configtraj];
            std::sort(subsystem.begin(),subsystem.end());

            std::string subsystemstr = numpy_array2string(subsystem);
         
            bool entropy_exist=check_entropy_exist_temp(dbname_temp,L,proj_ptag,inttype,measurement_time,init_state,lsub,subsystemstr);

            if (! entropy_exist)
            {
               std::vector<std::vector<int>> A = CliffordGates::get_subsystem_stabilizer(dat,subsystem);
               int entropy=CliffordGates::get_rank(A)-lsub;

               if (entropy < 0 ) 
               {
                  std::cerr << " Entropy is negative! " << std::endl;
                  throw 0;
               }
         
               insert_entropy_to_table_temp(dbname_temp,L,proj_ptag,inttype,measurement_time,init_state,
                     lsub,subsystemstr,entropy);
            }
         }
      }
      else
      {
      //    np.random.seed(0)
      //    numtraj=numtrajmax
      //    print('Lsub=',lsub,',',numtraj,'trajectories',flush=True)

      //    randsubiter=map(np.random.choice,[L]*numtraj,[lsub]*numtraj,[False]*numtraj)
      //    infos=np.array(list(map(get_m,[fdir]*numtraj,[dbname_state]*numtraj,[dbname_temp]*numtraj,
      //       [L]*numtraj,[proj_ptag]*numtraj,[inttype]*numtraj,
      //       [measurement_time]*numtraj,[init_state]*numtraj,
      //       randsubiter,[traj]*numtraj,[dat]*numtraj,range(numtraj))),dtype=float)
         int a =0;
      }
   }
}

// def get_all_ent(dbname_temp,dbname_out,L,proj_ptag,inttype,Lsub,duration,traj,mt,init_state) :
//    try :
//       infos=np.array(get_entropies_from_table(dbname_temp,L,proj_ptag,inttype,mt,init_state,Lsub)).reshape(-1)
//       print(dbname_temp)
//    except :
//       infos=[]
// 
//    if len(infos) > 2 :
//       print(dbname_temp)
//       info_mean=np.mean(infos)
//       info_var=np.var(infos)
//       info_std=np.std(infos)/np.sqrt(len(infos))
// 
//       page_lim=2.**(2.*Lsub-L)
// 
//       print("{},{},{},{},{},{},{},{:.15f},{},{}".format(duration,init_state,L,Lsub,
//          len(infos),min(infos),max(infos),(Lsub-info_mean)/page_lim,
//          info_std,len(infos[infos<Lsub])/float(len(infos))),flush=True)
// 
//       insert_entropy_to_table(dbname_out,L,proj_ptag,inttype,
//             mt,init_state,duration,traj,Lsub,len(infos),min(infos),max(infos),info_mean,info_var,info_std)
// 
//       return infos 
// 
// def get_avg_entropy_from_table(dbname,L,proj_ptag,inttype,
//       Lsub,traj,duration,mt="before",init_state="Z") :
//    con=sqlite3.connect(dbname)
//    cur=con.cursor()
//    sqcom="SELECT DURATION,MEAN,STD FROM ENTROPY WHERE " + \
//    """L={} and PROJ_PTAG={} and inttype='{}' """.format(L,proj_ptag,inttype) +  \
//    """and LSUB={} and TRAJ={} and DURATIOn={} and MEASUREMENT_TIME='{}' and INIT_STATE='{}'""".format(Lsub,traj,duration,mt,init_state)
// 
//    cur.execute(sqcom)
//    con.commit()
//    data=cur.fetchone()
//    con.close()
//    return data

double get_avg_entropy(std::string dbdir, int L,int proj_ptag,std::string inttype,int Lsub,int duration,int traj,
      std::string measurement_time,std::string init_state)
{
   std::string dbdir_state=fmt::v7::format("./data/duration/{}/{}/{}/{}/",duration,inttype,L,proj_ptag);
   std::string dbname_state=dbdir_state+"states.db";

   std::string dbname_out=dbdir+"postanalysis_duration.db";
   std::string dbname_temp=dbdir_state+fmt::v7::format("postanalysis_{}_{}.db",traj,Lsub);


   bool have_state=false;

   if (std::filesystem::exists(dbdir_state))
   {
      std::string state;
      try
      {
         state=get_state_from_table(dbname_state,L,proj_ptag,inttype,measurement_time,init_state,traj);
         if (state.size() > 0)
         {
            have_state=true;
         }
      }
      catch (...)
      {
         have_state=false;
      }
   }

   if (! have_state)
   {
      std::cout << "now evolving a state for " << duration << std::endl << std::flush;

      std::string make_state_command=fmt::v7::format("bash calc_EE_traj.sh {} {} {} {} {} {}",inttype,L,proj_ptag,measurement_time,
            duration,traj);

      std::system(make_state_command.c_str());

      std::string move_db_command=fmt::v7::format("bash move_to_db_sub_traj.sh {} {} {} {} {} {}",inttype,L,proj_ptag,measurement_time,
            duration,traj);

      std::system(move_db_command.c_str());

   }

   make_table_temp(dbname_temp);
   make_table(dbname_out);

   get_code_params(dbdir,dbname_state,dbname_temp,L,proj_ptag,inttype,Lsub,
         duration,traj,measurement_time=measurement_time,init_state=init_state);

   // get_all_ent(dbname_temp,dbname_out,L,proj_ptag,inttype,Lsub,duration,traj,measurement_time,init_state)
   // data=get_avg_entropy_from_table(dbname_out,L,proj_ptag,inttype,Lsub,traj,duration,mt=measurement_time,init_state=init_state)
   // return data[1]
   return 0.01;
}
