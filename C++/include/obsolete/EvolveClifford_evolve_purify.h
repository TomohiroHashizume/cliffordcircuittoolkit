#include <chrono>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <string>
#include <cstdlib>
#include <cmath>

namespace CliffordGates {
   int int_log2(int num)
   {
      int count=0;
      while(num > 1)
      {
         num /= 2;
         count++;
      }
      return count;
   }

   int projective_measure(std::vector<std::vector<bool>> &state,double proj_p,int site1,int site2,bool Xmeasure=false)
   {
      int count = 0;
      if ( ( std::rand()/(RAND_MAX + 1.))  < proj_p )
      {
         if (! Xmeasure)
         {
            CliffordGates::apply_Pmeasure(state,site1);
            count++;
         }
         else
         {
            CliffordGates::apply_PmeasureX(state,site1);
            count++;
         }
      }

      if ( ( std::rand()/(RAND_MAX + 1.))  < proj_p )
      {
         if ( ! Xmeasure) 
         {
            CliffordGates::apply_Pmeasure(state,site2);
            count++;
         }
         else
         {
            CliffordGates::apply_PmeasureX(state,site2);
            count++;
         }
      }
      return count;
   }

   class EvolveClifford_evolve : public EvolveClifford
   {
      public :
         using EvolveClifford::EvolveClifford;
         void evolve(std::string fname_in, std::string inttype_in, int numtimesteps_in,int maxnumgates_in, 
               double proj_p_in, std::string measurement_time_in,
               int boundary_in,bool deterministic_in,bool track_in, int numsamples);
         void toggle_apply_phase(bool apply_phase_in){ apply_phase=apply_phase_in; }
         void toggle_Xmeasure(bool Xmeasure_in ){ measure_X=Xmeasure_in; }
         void toggle_save(bool save_in){ save=save_in; }

      private :
         int INF=(unsigned)!((int)0);

         int evolve_pair(int site1,int site2,bool apply_phase_gate);
         int evolve_PWR2(int i);
         int evolve_NNAA(int i);
         int evolve_NN(int i);
         int evolve_AA(int i);

         std::vector<std::vector<bool>> curr_state;

         std::string fname;
         std::string inttype;
         std::vector<int> out_t;

         bool deterministic=true;
         bool thermalize=false;
         bool track=true;
         bool apply_phase=false;
         bool measure_X=false;
         bool save=false;

         double proj_p;

         std::string measurement_time;
         int boundary;

         int numtimesteps;
         int maxnumgates;

         int curr_t;
         int outcount=0;

         int numgates=0;
         int nummeasures=0;
         int be=INF;

         std::vector<int> bes;
         std::ofstream outfile;
   }
   ;

   void EvolveClifford_evolve::evolve(std::string fname_in, std::string inttype_in, int numtimesteps_in,int maxnumgates_in, 
         double proj_p_in, std::string measurement_time_in,
         int boundary_in,bool deterministic_in,bool track_in, int numsamples=1000)
   {
      fname=fname_in;
      inttype=inttype_in;

      deterministic=deterministic_in;
      track=track_in;
      if ( ! save )
      {
         track=false;
      }

      thermalize=false;

      proj_p=proj_p_in;

      measurement_time=measurement_time_in;
      boundary=boundary_in;
      
      numtimesteps=numtimesteps_in;
      maxnumgates=maxnumgates_in;

      std::cout << "proj_p=" << proj_p << std::endl;

      if (maxnumgates < 1)
      {
         maxnumgates=INF;
         out_t.push_back(INF);
      }
      else 
      {
         if (track)
         {
            out_t = CliffordGates::intlogspace(maxnumgates,numsamples);
         }
         else
         {
            out_t.push_back(INF);
         }
      }

      if (boundary<0)
      {
         boundary=Length/2;
      }

      if (! is_initialized)
      {
         std::cerr << "initialization: something is wrong" << std::endl;
         throw 3;
      }

      if (! thermalize)
      {
         std::cout << "making EPR pairs" << std::endl;
         this->make_EPRpairs();
      }

      if (! this->check_maximally_entangled())
      {
         std::cerr << "EvolveClifford_evolve: something is wrong" << std::endl;
         throw 2;
      }

      numgates=0;
      nummeasures=0;
      int out=0;

      curr_state=init_state;
      curr_t=out_t[outcount];

      auto start_time = std::chrono::high_resolution_clock::now();
      for(int i=0;i<numtimesteps;i++)
      {
         if (inttype=="PWR2")
         {
            out = this->evolve_PWR2(i);
         }
         else if (inttype == "NNAA")
         {
            out = this->evolve_NNAA(i);
         }
         else if (inttype == "AA")
         {
            out = this->evolve_AA(i);
         }
         else if (inttype == "NN")
         {
            out = this->evolve_NN(i);
         }
         else
         {
            std::cout << inttype <<" does not exist" << std::endl;
            throw 3;
         }

         if (out==0)
         {
            break;
         }

         if ( i%100 == 0)
         {
            std::cout << i <<" of "<< numtimesteps << " with " << numgates << " gates " << " applied " << std::endl << std::flush;
            std::cout << " and " << nummeasures << " measurements " << std::endl << std::flush;
            std::cout << " ideally " << numgates*(2.0*proj_p) << std::endl << std::flush;
            auto current_time = std::chrono::high_resolution_clock::now();
            std::cout <<
               std::chrono::duration_cast<std::chrono::seconds>(current_time - start_time).count() << " seconds past" << std::endl;
            be = CliffordGates::get_entropy_comp(curr_state,Length,boundary);
            if (be < 1)
            {
               break;
            }

         }
      }

      if (! track)
      {
         be = CliffordGates::get_entropy_comp(curr_state,Length,boundary);
         bes.push_back(be);
      }

      if (save)
      {
         outfile.open(fname_in);
         for(int i=0;i<bes.size();i++)
         {
            int be=bes[i];
            curr_t=out_t[i];
            outfile << be << "," << curr_t << std::endl;
         }
         outfile.close();
      }
      state_now=curr_state;
   }

   int EvolveClifford_evolve::evolve_pair(int site1, int site2,bool apply_phase_gate=false)
   {
      if (measurement_time == "after")
      {
         nummeasures+=CliffordGates::projective_measure(curr_state,proj_p,site1,site2,measure_X);
      }

      if (deterministic)
      {
         CliffordGates::apply_Hadamard(curr_state,site1);
         CliffordGates::apply_CNOT(curr_state,site1,site2);
         CliffordGates::apply_Hadamard(curr_state,site2);
         numgates+=1;
      }
      else
      {
         int numelm=(Allgates.size());
         int randnum = std::rand()%numelm;
         std::vector<std::string> gates = Allgates[randnum];
         CliffordGates::apply_RandomUnitary(curr_state,site1,site2,gates);
         numgates+=1;
      }

      if (numgates > maxnumgates)
      {
         return 0;
      }

      if (measurement_time == "before")
      {
         nummeasures += CliffordGates::projective_measure(curr_state,proj_p,site1,site2,measure_X);
      }

      if (track)
      {
         if (numgates == curr_t)
         {
            be = CliffordGates::get_entropy(curr_state,Length,boundary);
            if (bes.size() > 2) 
            {
               if (be > bes[bes.size() - 1])
               {
                  std::cout << "be = " << be << " be_prev = " << bes[bes.size() - 1] << std::endl;
                  be = CliffordGates::get_entropy_comp(curr_state,Length,boundary,true);
                  print_state_to_file(curr_state);
                  std::cout << be << std::endl;
                  be = CliffordGates::get_entropy(curr_state,Length,boundary,true);
                  std::cout << be << std::endl;
                  throw 2;
               }
            }

            bes.push_back(be);
            outcount++;
            if (outcount < out_t.size())
            {
               curr_t=out_t[outcount];
            }
            else
            {
               return 0;
            }
         }
      }

      if (be==0)
      {
         std::cout << numgates << ", done" << std::endl;
         return 0;
      }

      if (deterministic && apply_phase_gate)
      {
         apply_PhaseGate(curr_state,site1);
         apply_PhaseGate(curr_state,site2);
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_PWR2(int i)
   {
      for(int log2d=0;log2d<(int_log2(Length)-2);log2d++)
      {
         int d=std::pow(2,log2d);
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<Length/2;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length/2);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      if(i%2 == 0)
      {
         int log2d=(int_log2(Length)-2);
         int d=std::pow(2,log2d);
         for(int site=0;site<Length/4;site++)
         {
            int site1=site;
            int site2=(site1+d)%(Length/2);
            int finished = this -> evolve_pair(site1,site2,apply_phase);
            if (finished == 0)
            {
               return finished;
            }
         }
      }
      return 1;
   }

   int EvolveClifford_evolve::evolve_NN(int i)
   {
      int d=1;
      for(int site0=0;site0<d;site0++)
      {
         for(int site=site0+d*(i%2);site<Length/2;site+=2*d)
         {
            int site1=site;
            int site2=(site1+d)%(Length/2);
            int finished;
            if (i%2==1)
            {
               finished = this -> evolve_pair(site1,site2);
            }
            else 
            {
               finished = this -> evolve_pair(site1,site2,apply_phase);
            }

            if (finished == 0)
            {
               return finished;
            }
         }
      }

      return 1;
   }


   int EvolveClifford_evolve::evolve_NNAA(int i)
   {
      for(int d=1;d<(Length/4);d++)
      {
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<(d*((Length/2)/d));site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length/2);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      if(i%2 == 0)
      {
         for(int site0=0;site0<1;site0++)
         {
            for(int site=site0;site<Length/2; site+=2*1)
            {
               int site1=site;
               int site2=(site1+1)%(Length/2);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }

         for(int d=1; d<Length/4;d++)
         {
            for(int site=d*((Length/2)/d);site<Length/2;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length/2);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }

         for(int site0=0;site0<1;site0++)
         {
            for(int site=site0+1;site<Length/2; site+=2*1)
            {
               int site1=site;
               int site2=(site1+1)%(Length/2);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }

         for( int site=0; site<Length/4;site++)
         {
            int site1=site;
            int site2=(site+Length/4)%(Length/2);
            int finished = this -> evolve_pair(site1,site2,apply_phase);
            if (finished == 0)
            {
               return finished;
            }
         }
      }

      return 1;
   }

   int EvolveClifford_evolve::evolve_AA(int i)
   {
      for(int d=1;d<(Length/4);d++)
      {
         for(int site0=0;site0<d;site0++)
         {
            for(int site=site0+d*(i%2);site<(d*((Length/2)/d));site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length/2);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }
      }

      if(i%2 == 0)
      {
         for(int d=1; d<Length/4;d++)
         {
            for(int site=d*((Length/2)/d);site<Length/2;site+=2*d)
            {
               int site1=site;
               int site2=(site1+d)%(Length/2);
               int finished = this -> evolve_pair(site1,site2);
               if (finished == 0)
               {
                  return finished;
               }
            }
         }

         for( int site=0; site<Length/4;site++)
         {
            int site1=site;
            int site2=(site+Length/4)%(Length/2);
            int finished = this -> evolve_pair(site1,site2,apply_phase);
            if (finished == 0)
            {
               return finished;
            }
         }
      }
      return 1;
   }
}
