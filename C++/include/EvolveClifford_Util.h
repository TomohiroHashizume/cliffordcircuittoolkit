#include <chrono>
#include <cmath>
#include <fstream>
#include <math.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include <cstdlib>
#include <vector>
#include "monnamap.h"

extern "C" void dgesvd_( char* jobu, char* jobvt, int* m, int* n, double* a,
      int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt,
      double* work, int* lwork, int* info );

namespace CliffordGates {
   void print_state_to_file(std::vector<std::vector<bool>> &state, std::string fname="state.out");

   void print_intlist_to_file(std::vector<std::vector<int>> &intlist, std::string fname="intlist.out");

   std::string print_state_to_string(std::vector<std::vector<bool>> &state);

   void print_state(std::vector<std::vector<bool>> &state);

   void print_state(std::vector<std::vector<int>> &state);

   void print_vector(std::vector<int> &vec);

   void print_stab_phases_to_file(std::vector<std::vector<bool>> &stab_phases_out, std::string fname="stab_phases.out");

   std::vector<std::vector<bool>> get_stabilizer(std::vector<std::vector<bool>> state);

   std::vector<std::vector<bool>> get_subsystem_stabilizer(const std::vector<std::vector<bool>> state, 
         int Length, std::vector<int> subsystem);

   std::vector<std::vector<int>> from_string_to_state(std::string str_state);

   int g(bool x1,bool z1,bool x2, bool z2);

   void rowsum(std::vector<std::vector<bool>> &state, int p,int q, bool measure);

   void apply_Hadamard(std::vector<std::vector<bool>> &state, int site);

   void apply_PhaseGate(std::vector<std::vector<bool>> &state, int site);;

   void apply_CNOT(std::vector<std::vector<bool>> &state, int site1, int site2);

   bool apply_Pmeasure(std::vector<std::vector<bool>> &state, int site,bool measure=false);

   void flip(std::vector<std::vector<bool>> &state, int site);

   std::vector<std::string> get_gates(std::string elm);

   int apply_PmeasureX(std::vector<std::vector<bool>> &state, int site,bool measure);

   void get_all_elms(std::vector<std::string> &One_Site, std::vector<std::string> &Two_Site,
         std::vector<std::vector<std::string>> &All_Gates);

   void Apply_ACE(std::string gate,std::vector<std::vector<bool>> &state,int site,bool is_conj);

   void Apply_BD(std::string gate,std::vector<std::vector<bool>> &state,int site1,int site2,bool is_conj);

   void apply_RandomUnitary(std::vector<std::vector<bool>> &state, int site1, int site2, 
         std::vector<std::string> gates,bool is_conj=false);

   void swap(std::vector<std::vector<int>> &mat, int row1, int row2, int col);

   void swap(std::vector<std::vector<bool>> &mat, int row1, int row2, int col);

   int get_rank(std::vector<std::vector<int>> mat,bool verbose=false);

   int get_rank(std::vector<std::vector<bool>> mat,bool verbose=false);

   int get_entropy(std::vector<std::vector<bool>> state,const int Length,const int boundary,bool verbose=false);

   int get_entropy_comp(std::vector<std::vector<bool>> state,const int Length, const int boundary,bool verbose=false);

   int get_tpi_l(const std::vector<std::vector<bool>> state,const int Length, int subsyssize, bool verbose=false);

   int get_tpi_p(const std::vector<std::vector<bool>> state,const int Length, int subsyssize, int base, bool verbose=false);

   std::vector<int> intlogspace(int end,int numsample);

   int intpow(int x, int p);
   int int_log2(int num);

   std::vector<int> binarize(int num,int maxnum);
   int read_binary_flipped(std::vector<int> in);
   int read_binary(std::vector<int> in);

   std::vector<int> rotate_vec(std::vector<int> in);
   int rotate_bit(int num,int maxnum);

   void projective_measure(std::vector<std::vector<bool>> &state,double proj_p,int site1,int site2);

   int get_BetheLatticeLength(int coord_num, int depth_in);

   std::vector<int> calc_IABM(std::vector<std::vector<bool>> state, int Length, std::vector<int> sites,bool verbose=false);
}
