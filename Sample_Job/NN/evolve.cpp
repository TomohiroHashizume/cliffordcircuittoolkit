#include <chrono>
#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>

#include "EvolveClifford.h"
#include "EvolveClifford_evolve.h"

int main(int argc,char *argv[])
{
   if (argc != 7)
   {
      throw 3;
   }

   std::string fname=argv[1]; //output file name
   std::string inttype=argv[2]; //name of interaction 
   int L = std::atoi(argv[3]);
   int proj_ptag=std::atoi(argv[4]); 
   std::string init=argv[5]; //initial state Z
   int seed=std::atoi(argv[6]); //don't use traj

   long long int duration=10000*L;

   double proj_p=proj_ptag/10000.;

   int verbose=1; // you cna turn it off by int verbose=0

   long long int numtimesteps=duration;

   std::cout << "Number of timesteps is " << numtimesteps << std::endl;

   long long int maxnumgates=1e12;
   bool deterministic=true;
   bool track=true;
   bool save=false;
   int boundary=L;

   auto start_time = std::chrono::high_resolution_clock::now();

   std::cout << "Now computing.. " << std::endl;

   int int_log2L=CliffordGates::int_log2(L);
   if (inttype == "BetheLattice_Player")
   {
      L=CliffordGates::get_BetheLatticeLength(3,int_log2L);
   }

   CliffordGates::EvolveClifford_evolve evolve(L+1,init,seed,verbose);
   evolve.make_EPRpairs_uudd(1);
   evolve.thermalize();

   if (inttype == "BetheLattice_Player")
   {
      evolve.set_BetheLattice(3,int_log2L);
   }

   evolve.toggle_purify(true);
   evolve.toggle_save(false);

   evolve.evolve(fname,inttype,numtimesteps,maxnumgates,proj_p,boundary,deterministic,track);

   auto current_time = std::chrono::high_resolution_clock::now();
   std::cout << "Program terminated after " << 
      std::chrono::duration_cast<std::chrono::seconds>(current_time - start_time).count() << " seconds" << std::endl;
}
