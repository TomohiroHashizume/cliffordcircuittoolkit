#!/bin/bash

if [ ! -d "./IO/" ]; then 
   mkdir "./IO/"
fi

##################################
inttypes=("randNN")

LogLs=(6 7 8) #L=2**LogL

pbegs=(100 100 100)
pends=(2000 2000 2000)

pincs=(100 100 100)

init_state="Z"

for (( LogLind=0; LogLind < ${#LogLs[@]}; LogLind++))
do
   for (( inttypeind=0; inttypeind < ${#inttypes[@]}; inttypeind++))
   do
      LogL=${LogLs[${LogLind}]}

      pbeg=${pbegs[${LogLind}]}
      pend=${pends[${LogLind}]}
      pinc=${pincs[${LogLind}]}

      for (( proj_ptag=${pbeg} ; proj_ptag<=${pend} ; proj_ptag+=${pinc} ))
      do
         inttype=${inttypes[${inttypeind}]}
         echo ${proj_ptag}
         echo ${inttype},${init_state}

         echo "LogL = ${LogL}"

         numjobs=$( ps uaxw |grep "calc_EE.sh" |grep -c -v grep )

         echo "proj_ptag=${proj_ptag}"
         echo "submitted simulate.sh ${inttype} ${LogL} ${proj_ptag} ${init_state} ${testmode}"
         bash simulate.sh ${inttype} ${LogL} ${proj_ptag} ${init_state} ${testmode}

      done

      if [ ${testmode} = true ]; then
         exit 0
      fi

   done
done
