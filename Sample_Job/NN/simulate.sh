#!/bin/bash
#SBATCH -n 1
#SBATCH --mem=1000M
#SBATCH --partition="qoqms_intel"
#SBATCH --time=5-00:00:00
#SBATCH --job-name=SQ_tau
#SBATCH --get-user-env
#SBATCH --array=1-5%2

#SBATCH --output=./IO/%x_%A_%a.out
#SBATCH --error=./IO/%x_%A_%a.err

##SBATCH --array=1-10%10

source ~/.bashrc
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1

inttype=${1}
LogL=${2}
proj_ptag=${3}
init_state=${4}

testmode=${5}

L=$(( 2 ** ${LogL} )) 

savedir="./data/${inttype}/${init_state}/${L}/${proj_ptag}/"
dbname="${savedir}states.db"

echo ${savedir}

if [ ! -d ${savedir} ]; then
   mkdir -p ${savedir}
fi

if [ $( echo ${inttype} |  grep "Bethe" | wc -l ) -gt 0 ]; then
   Lkari=$( python -c "import sys; import numpy as np; sys.path.append('../../../pythonmodules/'); from clifford_modules import *; \
      print(get_BetheLatticeLength(3,int(${LogL})))" )
   echo ${L}
   echo 
fi

trajstart=$(( ${upto} * ( ${SLURM_ARRAY_TASK_ID} - 1 ) ))
upto=10
trajend=$(( ${trajstart} + ${upto} ))

for (( traj=${trajstart}; traj<${trajend};traj++))
do
   fname="${savedir}${traj}.out" # 0.out
   seed=${traj}
   if [ ! -f ${fname}  ]; then
      echo ./evolve ${fname} ${inttype} ${L} ${proj_ptag} ${init_state} ${seed} 
      ./evolve ${fname} ${inttype} ${L} ${proj_ptag} ${init_state} ${seed} 
   fi
done

echo "done"
