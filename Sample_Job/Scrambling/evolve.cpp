#include <chrono>
#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>

#include "EvolveClifford.h"
#include "EvolveClifford_evolve.h"

int main(int argc,char *argv[])
{

   if (argc != 11)
   {
      throw 3;
   }

   std::string dbname=argv[1];
   std::string fname=argv[2];
   std::string inttype=argv[3];
   int L = std::atoi(argv[4]);
   int proj_ptag=std::atoi(argv[5]);
   std::string measurement_time=argv[6];
   bool apply_phase=std::atoi(argv[7]);
   std::string init=argv[8];

   int traj=std::atoi(argv[9]);
   int duration=std::atoi(argv[10]);

   double proj_p=proj_ptag/10000.;

   int seed=1;
   int verbose=1;


   int numtimesteps=0;
   if (inttype=="randNN")
   {
      numtimesteps=CliffordGates::int_log2(L)*4*duration;
   }
   else if (inttype=="randNN_obc")
   {
      numtimesteps=CliffordGates::int_log2(L)*4*duration;
   }
   else if (inttype=="randAlltoAll")
   {
      numtimesteps=CliffordGates::int_log2(L)*4*duration;
   }
   else
   {
      std::cerr << inttype <<" does not exist in the duration list" <<std::endl;
      throw 0;
   }

   std::cout << "Number of timesteps is " << numtimesteps << std::endl;

   long int maxnumgates=1e10;
   int boundary=L;
   bool deterministic=false;
   bool track=false;
   bool save=false;

   auto start_time = std::chrono::high_resolution_clock::now();

   std::cout << "Now computing.. " << std::endl;
   CliffordGates::EvolveClifford_evolve evolve(L,init,traj,verbose);
   if (apply_phase)
   {
      std::cout << "apply_phase = " << apply_phase << std::endl;
   }
   evolve.toggle_apply_phase(apply_phase);
   evolve.evolve(fname,inttype,numtimesteps,maxnumgates,proj_p,boundary,deterministic,track);
   evolve.export_state(fname);

   auto current_time = std::chrono::high_resolution_clock::now();
   std::cout << "Program terminated after " << 
      std::chrono::duration_cast<std::chrono::seconds>(current_time - start_time).count() << " seconds" << std::endl;
}
