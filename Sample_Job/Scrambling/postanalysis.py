import numpy as np
import scipy.optimize
import os
import sys
import scipy.special as sspecial
import itertools
sys.path.append('../../../pythonmodules/')
sys.path.append('../../../pythonmodules/cython/')

from clifford_modules import * 
import rank as rk

dpi=300
def get_m(statedir,L,proj_ptag,inttype,init_state,traj,subsystem,configtraj,state) :
   if( configtraj%1000 == 0 ) :
      print("traj ",configtraj,flush=True)

   subsystem=np.sort(np.array(subsystem,dtype=int))
   subsystemstr=np.array2string(np.array(subsystem,dtype=int))

   if len(subsystem) > (L/2) :
      subsystem=np.array(list(set(np.arange(0,L,dtype=int))-set(subsystem)),dtype=int)

   # Write function here to load the output state from . 
   state=state[L:,:]
   subsystem=np.array(subsystem,dtype=int)
   A=np.array(np.hstack((state[:,subsystem],state[:,L+subsystem])),dtype=np.int)
   entropy=rk.get_rank_C(A,state.shape[0],state.shape[1])-len(subsystem)
   if entropy < 0 :
      print(A)
      print(subsystem)
      sys.exit(0)
   return entropy
   
def get_code_params(statedir,outdir,L,proj_ptag,inttype,Lsub,traj,init_state="Z") :

   state=get_state(dstatedir,L,proj_ptag,inttype,init_state,traj)
   numtrajmax=30000

   if sspecial.comb(L,Lsub) < numtrajmax :
      numtraj=int(sspecial.comb(L,Lsub))
      print('Lsub=',Lsub,',',numtraj,'trajectories',flush=True)

      infos=np.array(list(map(get_m,[statedir]*numtraj,
         [L]*numtraj,[proj_ptag]*numtraj,[inttype]*numtraj,
         [init_state]*numtraj,[traj]*numtraj,
         itertools.combinations(range(L),Lsub),range(int(sspecial.comb(L,Lsub))),[state]*numtraj)),dtype=float)

   else :
      seed=1234 #some number
      np.random.seed(seed)
      numtraj=numtrajmax
      print('Lsub=',Lsub,',',numtraj,'trajectories',flush=True)
      randsubiter=map(np.random.choice,[L]*numtraj,[Lsub]*numtraj,[False]*numtraj) 
      infos=np.array(list(map(get_m,[statedir]*numtraj,[outdir]*numtraj,
         [L]*numtraj,[proj_ptag]*numtraj,[inttype]*numtraj,
         [init_state]*numtraj,[traj]*numtraj,
         randsubiter,range(numtrajmax),[state]*numtraj)),dtype=float)

statedir=sys.argv[1]
outdir=sys.argv[2]
inttype=sys.argv[3]
L=int(sys.argv[4])
proj_ptag=int(sys.argv[5])
measurement_time=sys.argv[6]
apply_phase=int(sys.argv[7])
init_state=sys.argv[8]
traj=int(sys.argv[9])
Lsub=int(sys.argv[10])
duration=int(sys.argv[11])

get_code_params(statedir,outdir,L,proj_ptag,inttype,Lsub,traj,init_state=init_state)
