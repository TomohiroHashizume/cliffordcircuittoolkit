#!/bin/bash
#SBATCH -n 1
#SBATCH --cpus-per-task=1
#SBATCH --partition=standard
#SBATCH --account=daley-mbdiqs

#SBATCH --time=01:00:00
#SBATCH --mem=1000M

#SBATCH --job-name=cp_large

#SBATCH --get-user-env
#SBATCH --array=1-10

#SBATCH --output=./IO/%x_%A_%a.out
#SBATCH --error=./IO/%x_%A_%a.err

source ~/.bashrc
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1

inttype=${1}
L=${2}
proj_ptag=${3}
measurement_time=${4}
apply_phase=${5}
init_state=${6}

duration=${7}
testmode=${8}

savedir="./data/${inttype}/${init_state}/${apply_phase}/${L}/${proj_ptag}/${duration}/"
dbname="${savedir}states.db"

if [ ! -d ${savedir} ]; then
   mkdir -p ${savedir}
fi

if [ ${testmode} = local ]; then
   for (( traj=0; traj<1;traj++))
   do
      fname="${savedir}${traj}.out"
      if [ ! -f ${fname}  ]; then
         echo ./evolve ${dbname} ${fname} ${inttype} ${L} ${proj_ptag} ${measurement_time} ${apply_phase} ${init_state} ${traj} ${duration}
         ./evolve ${dbname} ${fname} ${inttype} ${L} ${proj_ptag} ${measurement_time} ${apply_phase} ${init_state} ${traj} ${duration}
      fi
   done

elif [ ${testmode} = seq ]; then
   for (( traj=0; traj<1;traj++))
   do
      echo ${traj}
      fname="${savedir}${traj}.out"
      if [ ! -f ${fname}  ]; then
         echo ./evolve ${dbname} ${fname} ${inttype} ${L} ${proj_ptag} ${measurement_time} ${apply_phase} ${init_state} ${traj} ${duration}
         ./evolve ${dbname} ${fname} ${inttype} ${L} ${proj_ptag} ${measurement_time} ${apply_phase} ${init_state} ${traj} ${duration}
      fi
   done

elif [ ${testmode} = true ]; then
   trajmax=1
   if [ $( echo ${inttype} | grep rand | wc -l ) -gt 0 ]; then
      trajmax=10
   fi

   trajstart=$(( ${upto} * ( ${SLURM_ARRAY_TASK_ID} - 1 ) ))
   upto=10
   trajend=$(( ${trajstart} + ${upto} ))

   for (( traj=${trajstart}; traj<${trajend};traj++))
   do
      fname="${savedir}${traj}.out"
      if [ -f ${fname} ]; then
         rm ${fname}
      fi

      if [ ! -f ${fname}  ]; then
         echo ./evolve ${dbname} ${fname} ${inttype} ${L} ${proj_ptag} ${measurement_time} ${apply_phase} ${init_state} ${traj} ${duration}
         ./evolve ${dbname} ${fname} ${inttype} ${L} ${proj_ptag} ${measurement_time} ${apply_phase} ${init_state} ${traj} ${duration}
      fi
   done
fi
echo "done"
