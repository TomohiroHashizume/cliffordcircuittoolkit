#!/bin/bash
if [ -z "${1}" ];then 
   exit 0
else
   testmode=${1}
fi

#if [ ${testmode} = true ]; then
#   make clean
#   make all
#fi
maxcorelimit=10

Ls=(8 16 32 64 128 256 512) #64 128 256 512)

#Ls=(128 256 512) #64 128 256 512)
Ls=(8 16 32 64 128 256 512) 
inttypes=("Riffle_double_CZ_H_bylayer" "Riffle_double_CZ_H_P_bylayer" "randNN_obc" "randAlltoAll")
inttypes=("Riffle_reverse_double_CZ_H_bylayer" "Riffle_reverse_double_CZ_H_P_bylayer" "Riffle_reverse_double_H_CZ_P_bylayer")
inttypes=("Riffle_CZ_H_bylayer" "Riffle_CZ_H_P_bylayer" "Riffle_H_CZ_P_bylayer" "Riffle_reverse_CZ_H_bylayer" "Riffle_reverse_CZ_H_P_bylayer" "Riffle_reverse_H_CZ_P_bylayer")
durations=(1 2 10)

Ls=(8 16 32 64 128 256 512 1024) 
inttypes=("Riffle_reverse_CZ_H_bylayer" "Riffle_reverse_double_CZ_H_P_bylayer" "Riffle_reverse_double_CZ_H_P_bylayer")
durations=(1 1 2)

Ls=(8 16 32 64 128 256 512) 
inttypes=("Riffle_reverse_even_odd_CZ_H_bylayer" "PWR2_obc_bylayer")
durations=(2 2)

Ls=(8 16 32 64 128 256 512) 
inttypes=("Riffle_reverse_even_odd_CZ_H_bylayer" "PWR2_obc_bylayer")
durations=(1 1)

Ls=(8 16 32 64 128 256 512) 
inttypes=("Riffle_reverse_double_even_odd_CZ_H_bylayer" "Riffle_reverse_double_even_odd_CZ_H_bylayer")
durations=(1 2)

Ls=(8 16 32 64 128 256 512) 
inttypes=("PWR2_obc_double_bylayer" "PWR2_obc_double_bylayer")
durations=(1 2)

inttypes=("Riffle_reverse_double_even_odd_CZ_H_P_bylayer" "Riffle_reverse_double_even_odd_CZ_H_P_bylayer")
durations=(1 2)

inttypes=("Riffle_reverse_double_even_odd_CZ_H_P_bylayer")
durations=(3)

inttypes=("Riffle_reverse_even_odd_CZ_H_P_bylayer" "Riffle_reverse_even_odd_CZ_H_P_bylayer" "Riffle_reverse_even_odd_CZ_H_P_bylayer")
durations=(1 2 3)

pbeg=0
pend=10
pinc=100

apply_phases=(0)
init_states=("X" "Y" "Z")

measurement_time="before"

for (( Lind=0; Lind < ${#Ls[@]}; Lind++))
do
   for (( inttypeind=0; inttypeind < ${#inttypes[@]}; inttypeind++))
   do
      for (( apind=0; apind < ${#apply_phases[@]}; apind++))
      do
         L=${Ls[${Lind}]}

         for (( isind=0; isind < ${#init_states[@]}; isind++))
         do
            inttype=${inttypes[${inttypeind}]}
            apply_phase=${apply_phases[${apind}]}
            init_state=${init_states[${isind}]}
            duration=${durations[${inttypeind}]}

            is_random=$( echo ${inttype} | grep "rand" | wc -l )
            if [ ${is_random} -gt 0 ]; then
               if [ ${init_state} != "Z" ]; then
                     continue 
               fi
            fi

            echo ${inttype},${apply_phase},${init_state},${duration}

            echo "L = ${L}"

            for ((proj_ptag=${pbeg}; proj_ptag <= ${pend}; proj_ptag += ${pinc} ))
            do
               numjobs=$( ps uaxw |grep "calc_EE.sh" |grep -c -v grep )
               weight_p=${weight_ps[${wpind}]}

               echo "proj_ptag=${proj_ptag}"
               echo "submitted calc_EE.sh ${inttype} ${L} ${proj_ptag} ${measurement_time} ${apply_phase} ${init_state} \
                  ${duration} ${testmode}"

               if [ ${testmode} = true ]; then
                  bash calc_EE.sh ${inttype} ${L} ${proj_ptag} ${measurement_time} ${apply_phase} ${init_state} \
                     ${duration} ${testmode}
               elif [ ${testmode} = seq ]; then
                  bash calc_EE.sh ${inttype} ${L} ${proj_ptag} ${measurement_time} ${apply_phase} ${init_state} \
                     ${duration} ${testmode}

               elif [ ${testmode} = local ] ; then
                  while [ ${numjobs} -gt ${maxcorelimit} ]
                  do
                     numjobs=$( ps uaxw |grep "calc_EE.sh" |grep -c -v grep )
                     sleep 1
                  done

                  nohup bash calc_EE.sh ${inttype} ${L} ${proj_ptag} ${measurement_time} ${apply_phase} ${init_state} \
                     ${duration} ${testmode} \
                     &> ./IO/${inttype}_${L}_${proj_ptag}_${measurement_time}.out &
                  sleep 1 
               else
                  sbatch calc_EE.sh ${inttype} ${L} ${proj_ptag} ${measurement_time} ${apply_phase} ${init_state} \
                     ${duration} ${testmode}
               fi
            done
         done
         if [ ${testmode} = true ]; then
            exit 0
         fi
      done
   done
done
