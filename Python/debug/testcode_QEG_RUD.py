import numpy as np
import os
from EvolveClifford_QEG_RUD import EvolveClifford_QEG_RUD
import matplotlib.pyplot as plt
os.environ['MKL_NUM_THREADS'] = '1'

# This is a debug script for recreating result from the paper:
# Nahum, A., Ruhman, J., Vijay, S., & Haah, J. (2017). Quantum Entanglement Growth under Random Unitary Dynamics. 
# In Physical Review X (Vol. 7, Issue 3). American Physical Society (APS). https://doi.org/10.1103/physrevx.7.031016

L=16
maxtime=600
maxtraj=100
force=True
ess=[]
proj_p=0.15
for i in range(maxtraj) :
   print("trajectory",i)
   Cliffordtype=EvolveCliffordQEG_RUD(L,init='X',seed=i)
   Cliffordtype.evolve(maxtime,proj_p,boundary=L//2)

   """
   bes=Cliffordtype.get_be()
   plt.plot(bes)
   plt.show()
   """

   ess.append(Cliffordtype.get_be())
ess=np.array(ess)
print(ess.shape)

if ess.shape[1] > 200 :
   print(np.mean(ess[:,50]))
   print(np.mean(ess[:,100]))
   print(np.mean(ess[:,200]))
   print(np.mean(ess[:,ess.shape[1]//2]))
   print(np.mean(ess[:,-1]))
elif ess.size < 100 :
   print(ess)

plt.plot(np.mean(ess,axis=0))
plt.show()
