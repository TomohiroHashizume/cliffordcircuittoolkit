import sys
sys.path.append('../')
import numpy as np
from concurrent import futures
from EvolveClifford  import *

# This is a debug script for recreating result from Fig. 15 from the paper:
# Nahum, A., Ruhman, J., Vijay, S., & Haah, J. (2017). Quantum Entanglement Growth under Random Unitary Dynamics. 
# In Physical Review X (Vol. 7, Issue 3). American Physical Society (APS). https://doi.org/10.1103/physrevx.7.031016

class EvolveClifford_QEG_RUD(EvolveClifford) :
   def __init__(self,L,init='Z',seed=0):
      super().__init__(L,init,seed)

   def evolve(self,numtimesteps,proj_p,times=np.array([]),boundary=0) :
      self.boundary=boundary
      if self.boundary == 0:
         self.buondary=self.Length//2

      self.numtimesteps=numtimesteps

      if times.size == 0 :
         print('all times are tracked')
         times=np.arange(1,numtimesteps)

      times=np.sort(times)
      timesind=0
      time=times[timesind]
      self.times=[]
      self.bes=[]

      if (not self.is_initialized) :
         print("Initialize the random unitaries!")

      else :
         states=[]
         curr_state = self.init_state.copy()

         for i in range(1,self.numtimesteps) :

#            for site in range(self.Length) :
#               U=np.random.randint(0,high=3)
#               if U==0 :
#                  apply_Hadamard(curr_state,site)
#               elif U==1 :
#                  apply_PhaseGate(curr_state,site)

            for site in range(0+i%2,self.Length-1,2) :
               site1=site
               site2=site1+1

               if np.random.uniform() < proj_p :
                  apply_Pmeasure(curr_state,site1)
               if np.random.uniform() < proj_p :
                  apply_Pmeasure(curr_state,site2)

               apply_RandomUnitary(curr_state,site1,site2,self.Allone,self.Alltwo)

            if i==(2*time) :
               bestate=np.array(curr_state.copy(),dtype=int)
               bestate=bestate[self.Length:-1,:-1]
               boundary=self.boundary #(self.Length//2)
               bestate=np.c_[bestate[:,:boundary],bestate[:,self.Length:self.Length+boundary]]
               be=np.linalg.matrix_rank(bestate)
               be-=boundary
               self.bes.append(be)

               if len(times) < 1000 :
                  states.append(curr_state.copy())

               self.times.append(time)
               timesind += 1
               if timesind < len(times) :
                  time=times[timesind]
               else  :
                  break

            if i%100 == 0:
               print(i,flush=True)

         self.states=np.array(states)
