import numpy as np
import Cython
import rank as rk
import sys
import timeit
sys.path.append('../')

from clifford_modules import *

def rand_mat_rank_C() :
   mat=np.array(np.round(np.random.uniform(0,1,(L,L))),dtype=np.int32)
   return rk.get_rank_C(mat,mat.shape[0],mat.shape[1])

def rand_mat_rank() :
   mat=np.array(np.round(np.random.uniform(0,1,(L,L))),dtype=np.int32)
   return get_rank(mat)

L=256*4

L0=128
L1=64

L0=128
L1=L
#print(timeit.timeit(rand_mat_rank_C, number=3))
#print(timeit.timeit(rand_mat_rank, number=3))

for i in range(100) :
   mat=np.array(np.round(np.random.uniform(0,1,(L0,L1))),dtype=np.int32)
   print(get_rank(mat))
   mat=np.array(np.round(np.random.uniform(0,1,(L0,L1))),dtype=np.int32)
   print(rk.get_rank_C(mat,mat.shape[0],mat.shape[1]))
   mat=np.array(np.round(np.random.uniform(0,1,(L0,L1))),dtype=np.int32)
   print(rk.get_rank_C(mat,mat.shape[0],mat.shape[1])==get_rank(mat))
