from distutils.core import setup, Extension
from Cython.Build import cythonize
from numpy import get_include # cimport numpy を使うため

ext = Extension("rank", sources=["rank.pyx"], include_dirs=['.', get_include()])
setup(name="rank", ext_modules=cythonize([ext]))

#ext = Extension("cmc_tools", sources=["mc_tools.pyx"], include_dirs=['.', get_include()])
#setup(name="cmc_tools", ext_modules=cythonize([ext]))
