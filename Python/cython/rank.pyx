cimport numpy as np

def get_rank_C(np.ndarray[int, ndim=2] mat,int nrows,int ncols) :
   cdef int rank
   cdef int i,j,k
   cdef int r

   rank=0
   for k in range(ncols) :
      i=-1;
      r=rank
      for r in range(rank,nrows) :
         if( not mat[r,k] ) :
            continue;
         if(i==-1) :
            i=r
            rank+=1
         else :
            for j in range(ncols) :
               mat[r,j] = (mat[r,j] != mat[i,j]);

      if( i>= rank) :
         mat[[i,rank-1],:]=mat[[rank-1,i],:]
   return rank;
