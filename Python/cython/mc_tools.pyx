import numpy as np
import scipy.optimize
from scipy.signal import find_peaks
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
import sqlite3
import pandas as pd
import os
from scipy.special import comb 
from scipy.special import binom

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "sans-serif",
    "font.sans-serif": ["Helvetica"]})
# for Palatino and other serif fonts use:
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "font.serif": ["Palatino"],
})

fontsize=12
fontsizeleg=8
figsize=(4,3.5)
dpi=300
outdir='../data/postanalysis/'

cinds=np.linspace(0.1,0.8,3)
colordict={
   "NN" : plt.cm.hsv(cinds[0]),
   "randAlltoAll" : plt.cm.hsv(cinds[1]),
   "PWR2" :  plt.cm.hsv(cinds[2]),
}

def log_coeff(N,n) :
   if n==0 :
      return 0
   if n==N :
      return 0
   coeff=np.log2(np.arange(N-n+1,N+1)).sum()-np.log2(np.arange(1,n+1)).sum()
   #print(coeff)
   return coeff

def make_logbncoeff(N) :
   return np.array(list(map(log_coeff,[N]*N,np.arange(1,N+1))))

def get_BDavg(pp,Qn,NN) :
   BNnp=np.zeros(len(Qn))
   nmax=int(pp*NN)
   BNnp[nmax-1]=1
   for n in range(nmax-1,0,-1) :
      BNnp[n-1]=BNnp[n]*((n+1)/(NN-n))*((1-pp)/pp)

   for n in range(nmax+1,len(Qn)+1) :
      BNnp[n-1]=BNnp[n-1-1]*((NN-n+1)/n)*(pp/(1.-pp))

   return np.sum(BNnp*Qn)/np.sum(BNnp)

def get_BDerr(pp,Qn,NN) :
   BNnp=np.zeros(len(Qn))
   nmax=int(pp*NN)
   BNnp[nmax-1]=1
   for n in range(nmax-1,0,-1) :
      BNnp[n-1]=BNnp[n]*((n+1)/(NN-n))*((1-pp)/pp)

   for n in range(nmax+1,len(Qn)+1) :
      BNnp[n-1]=BNnp[n-1-1]*((NN-n+1)/n)*(pp/(1.-pp))

   return np.sqrt(np.sum(BNnp*BNnp*Qn*Qn)/(np.sum(BNnp)*np.sum(BNnp)))

