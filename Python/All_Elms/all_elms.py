import numpy as np
import sys
sys.path.append("../")
from EvolveClifford import * 

# The implementation of generating all the elements of two-qubit Clifford Group
# up to their phase. 
# This algorithm is based on the following article. Please cite:
# Selinger, P. (2015). Generators and relations for n-qubit Clifford operators. In S. Abramsky (Ed.), Logical Methods in Computer Science (Vol. 11, Issue 2). 
# Centre pour la Communication Scientifique Directe (CCSD). https://doi.org/10.2168/lmcs-11(2:10)2015

def Apply_ACE(gate,state,site):
   for s in gate :
      if s == 'H' :
         apply_Hadamard(state,site)
      if s == 'P' :
         apply_PhaseGate(state,site)

def Apply_BD(gate,state,site1,site2) :
   for i in range(len(gate)) :
      if gate[i] == 'H' :
         if gate[i+1] == '0' :
            apply_Hadamard(state,H,site1)
         elif gate[i+1] == '1' :
            apply_Hadamard(state,H,site2)
      elif gate[i] == 'P' :
         if gate[i+1] == '0' :
            apply_PhaseGate(state,H,site1)
         elif gate[i+1] == '1' :
            apply_PhaseGate(state,H,site2)
      elif gate[i] == 'C' :
            apply_CNOT(state,H,site1,site2)

def apply_RandomUnitary(state,site1,site2,Allone,Alltwo) :

   randnum=np.random.randint(low=0,high=numelm)
   if randnum < len(Alltwo) :
      elm=Alltwo[randnum]
   else :
      elm=Allone[randnum-len(Alltwo)]

   gates=elm.split('+')
   if len(gates) == 8 :
      Apply_ACE(gates[0],state,site2)
      Apply_B(gates[1],state,site1,site2)
      Apply_ACE(gates[2],state,site1)
      Apply_D(gates[3],state,site1,site2)
      Apply_ACE(gates[4],state,site2)
      Apply_ACE(gates[5],state,site1)
      Apply_ACE(gates[6],state,site1)
      Apply_ACE(gates[7],state,state,site2)
   elif len(gates) == 7 :
      Apply_ACE(gates[0],state,site1)
      Apply_ACE(gates[1],state,site1)
      Apply_BD(gates[2],site1,site2)
      Apply_ACE(gates[3],state,site2)
      Apply_ACE(gates[4],state,site1)
      Apply_ACE(gates[5],state,site1)
      Apply_ACE(gates[6],state,site2)

def get_all_elms() :
   A=['I','H','HPH']

   B=['H1CH0H1CH0H1C','C0H1C','H0P0CH0H1C','H0CH0H1C']

   C=['I','HPPH']

   D=['CH0H1CH0H1CH1','H0CH0H1CH1','H0H1P1CH0H1CH1','H0H1CH0H1CH1']

   E=['I','P','PP','PPP']


   Alltwo=[]
   for Aelm in A :
      for Belm in B :
         for Celm in C :
            for Delm in D:
               for Eelm in E:
                  for Aelm1 in A :
                        for Celm1 in C :
                           for Eelm1 in E:
                              Alltwo.append(Aelm+"+"+Belm+"+"+Celm+"+"+Delm+"+"+Eelm+"+"+Aelm1+"+"+Celm1+"+"+Eelm1)

   Allone=[]
   for Aelm in A :
         for Celm in C :
            for Delm in D:
               for Eelm in E:
                  for Aelm1 in A :
                        for Celm1 in C :
                           for Eelm1 in E:
                              Allone.append(Aelm+"+"+Celm+"+"+Delm+"+"+Eelm+"+"+Aelm1+"+"+Celm1+"+"+Eelm1)

   return Alltwo,Allone

Alltwo,Allone=get_all_elms()
print(len(Alltwo)+len(Allone))
print(len(Alltwo))
#apply_RandomUnitary(Allone,Alltwo)
