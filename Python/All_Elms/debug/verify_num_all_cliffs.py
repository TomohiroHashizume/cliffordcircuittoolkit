# This is to check the number of all elements with the theoretical number.

A=3
B=4
C=2
D=4
E=4
print(A*B*C*D*E*A*C*E)
print(2*(4**2-1)*(4**2)*2*(4**1-1)*(4**1))

A=3
B=8+4+4+4
C=2

D=8+4+4+4
E=4
#print(((3*4*2*4*4)*(3*2*4))*8 + 3*2*4*4*3*2*4*8) #ok! #c.f. https://arxiv.org/pdf/1310.6813.pdf (first equation) upto factor of 8 I think...
