import numpy as np
import os,sys

# This is a bruteforce version of All_Elms.py. Please use All_Elms for the sensible inplementation. 

# The implementation of generating all the elements of two-qubit Clifford Group
# up to their phase. 
# This algorithm is based on the following article. Please cite it.
# Selinger, P. (2015). Generators and relations for n-qubit Clifford operators. In S. Abramsky (Ed.), Logical Methods in Computer Science (Vol. 11, Issue 2). 
# Centre pour la Communication Scientifique Directe (CCSD). https://doi.org/10.2168/lmcs-11(2:10)2015

def Apply_ACE(gate,state,site):
   for s in gate :
      if s == 'H' :
         state=apply_Hadamard(state,site)
      if s == 'P' :
         state=apply_PhaseGate(state,site)
   return state

def Apply_BD(gate,state,site1,site2) :
   for i in range(len(gate)) :
      if gate[i] == 'H' :
         if gate[i+1] == '0' :
            state=apply_Hadamard(state,site1)
         elif gate[i+1] == '1' :
            state=apply_Hadamard(state,site2)
      elif gate[i] == 'P' :
         if gate[i+1] == '0' :
            state=apply_PhaseGate(state,site1)
         elif gate[i+1] == '1' :
            state=apply_PhaseGate(state,site2)
      elif gate[i] == 'C' :
            state=apply_CNOT(state,site2,site1)
   return state

def apply_RandomUnitary(state,site1,site2,elm) :
   gates=elm.split('+')
   if len(gates) == 8 :
      state=Apply_ACE(gates[0],state,site2)
      state=Apply_BD(gates[1],state,site1,site2)
      state=Apply_ACE(gates[2],state,site1)

      state=Apply_BD(gates[3],state,site1,site2)
      state=Apply_ACE(gates[4],state,site2)

      state=Apply_ACE(gates[5],state,site1)
      state=Apply_ACE(gates[6],state,site1)

      state=Apply_ACE(gates[7],state,site2)

   elif len(gates) == 7 :
      state=Apply_ACE(gates[0],state,site1)
      state=Apply_ACE(gates[1],state,site1)

      state=Apply_BD(gates[2],state,site1,site2)
      state=Apply_ACE(gates[3],state,site2)

      state=Apply_ACE(gates[4],state,site1)
      state=Apply_ACE(gates[5],state,site1)

      state=Apply_ACE(gates[6],state,site2)
   return state

def apply_RandomUnitary_1(state,site,elm) :
   gates=elm.split('+')
   for gate in gates :
      for char in gate :
         if char=="H" :
            state=apply_Hadamard_1(state,site)
         elif char=="P" :
            state=apply_PhaseGate_1(state,site)
   return state

def get_all_elms_2() :
   A=['I','H','HPH']

   B=['H1CH0H1CH0H1C','C0H1C','H0P0CH0H1C','H0CH0H1C']

   C=['I','HPPH']

   D=['CH0H1CH0H1CH1','H0CH0H1CH1','H0H1P1CH0H1CH1','H0H1CH0H1CH1']

   E=['I','P','PP','PPP']


   Alltwo=[]
   for Aelm in A :
      for Belm in B :
         for Celm in C :
            for Delm in D:
               for Eelm in E:
                  for Aelm1 in A :
                        for Celm1 in C :
                           for Eelm1 in E:
                              Alltwo.append(Aelm+"+"+Belm+"+"+Celm+"+"+Delm+"+"+Eelm+"+"+Aelm1+"+"+Celm1+"+"+Eelm1)

   Allone=[]
   for Aelm in A :
         for Celm in C :
            for Delm in D:
               for Eelm in E:
                  for Aelm1 in A :
                        for Celm1 in C :
                           for Eelm1 in E:
                              Allone.append(Aelm+"+"+Celm+"+"+Delm+"+"+Eelm+"+"+Aelm1+"+"+Celm1+"+"+Eelm1)

   return Allone,Alltwo

def get_all_elms_1() :
   A=['I','H','HPH']
   C=['I','HPPH']
   E=['I','P','PP','PPP']

   All=[]
   for Aelm in A :
         for Celm in C :
               for Eelm in E:
                     All.append(Aelm+"+"+Celm+"+"+Eelm)
   return All

def apply_CNOT(mat,site1,site2) :
   I= \
   np.array([[1,0,0, 0],
   [0,1,0, 0],
   [0,0,1, 0],
   [0,0,0,1]])
   CZ= \
   np.array([[1,0,0, 0],
   [0,1,0, 0],
   [0,0,1, 0],
   [0,0,0,-1]])

   mat=apply_Hadamard(mat,site2)
   mat=apply_Hadamard(mat,site2)
   mat=CZ@mat
   return mat

def apply_Hadamard(mat,site) :
   I=np.array(
         [[1,0],
          [0,1]]
         )
   H=(1./np.sqrt(2))*np.array(
         [[1,1],
          [1,-1]])

   if site==0 :
      mat=np.kron(H,I)@mat
   else :
      mat=np.kron(I,H)@mat
   return mat

def apply_PhaseGate(mat,site) :
   I=np.array(
         [[1,0],
          [0,1]])
   P=np.array(
         [[1, 0],
          [0,1j]])
   if site==0 :
      mat=np.kron(P,I)@mat
   else :
      mat=np.kron(I,P)@mat
   return mat

def apply_Hadamard_1(mat,site) :
   H=(1./np.sqrt(2))*np.array(
         [[1,1],
          [1,-1]])

   mat=H@mat
   return mat

def apply_PhaseGate_1(mat,site) :
   P=np.array(
         [[1, 0],
          [0,1j]])
   mat=P@mat
   return mat


all_one_2,all_two_2=get_all_elms_2()
all_one=get_all_elms_1()
print(len(all_one_2)+len(all_two_2))
print(len(all_one))

I=np.array([[1,0],[0,1]])
C1mats_temp=[]
for gate in all_one :
   mat=I.copy()
   mat=apply_RandomUnitary_1(mat,0,gate)
   C1mats_temp.append(mat.copy())

C1mats=[]
for mat1 in C1mats_temp :
   for mat2 in C1mats_temp :
      C1mats.append(np.kron(mat1,mat2).copy())

I=np.kron(I,I)
C2mats=[]
for ind in range(len(all_one_2)) :
   gate=all_one_2[ind]
   mat=I.copy()
   mat=apply_RandomUnitary(mat,0,1,gate)
   C2mats.append(mat.copy())

for ind in range(len(all_two_2)) :
   gate=all_two_2[ind]
   mat=I.copy()
   mat=apply_RandomUnitary(mat,0,1,gate)
   C2mats.append(mat.copy())


count=0
dupgate=[]
for mat in C1mats :
   for ind in range(len(all_one_2)) :
      mat2=C2mats[ind]

      absdiff=np.sum(np.abs(mat-mat2))
      if absdiff<1e-10:
         dupgate.append(['one',ind])

   for ind in range(len(all_two_2)) :
      mat2=C2mats[ind+len(all_one_2)]
      absdiff=np.sum(np.abs(mat-mat2))
      if absdiff<1e-10:
         dupgate.append(['two',ind])

print(len(dupgate),'duplicates')
print(len(C2mats))
print(len(C1mats))

count=0
count8=0
for dup in dupgate :
   if dup[0] =='one' :
      temp=all_one_2[dup[1]]
   if dup[0] =='two' :
      temp=all_two_2[dup[1]]

   temp=temp.split('+')
   if len(temp) == 8 :
      print(temp[1],temp[3])
      count8+=1
   count+=1
print(count,count8)
