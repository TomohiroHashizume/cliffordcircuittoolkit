import sys
import numpy as np
import os
import time 
from concurrent import futures #for parallel processing

# This is a Class file for simulating clifford circuit with python.
# This algorithm is based on the algorithm by Aaronson and Gottesman. 
# Please cite:
# Aaronson, S., & Gottesman, D. (2004). Improved simulation of stabilizer circuits. In Physical Review A (Vol. 70, Issue 5). 
# American Physical Society (APS). https://doi.org/10.1103/physreva.70.052328.

# The algorithm for generating all two-qubit Clifford gates upto the phase is based on 
# the following article. Please cite:
# Selinger, P. (2015). Generators and relations for n-qubit Clifford operators. In S. Abramsky (Ed.), Logical Methods in Computer Science (Vol. 11, Issue 2). 
# Centre pour la Communication Scientifique Directe (CCSD). https://doi.org/10.2168/lmcs-11(2:10)2015.


def Apply_ACE(gate,state,site):
   for s in gate :
      if s == 'H' :
         apply_Hadamard(state,site)
      if s == 'P' :
         apply_PhaseGate(state,site)

def Apply_BD(gate,state,site1,site2) :
   for i in range(len(gate)) :
      if gate[i] == 'H' :
         if gate[i+1] == '0' :
            apply_Hadamard(state,site1)
         elif gate[i+1] == '1' :
            apply_Hadamard(state,site2)
      elif gate[i] == 'P' :
         if gate[i+1] == '0' :
            apply_PhaseGate(state,site1)
         elif gate[i+1] == '1' :
            apply_PhaseGate(state,site2)
      elif gate[i] == 'C' :
            apply_CNOT(state,site2,site1)

def apply_RandomUnitary(state,site1,site2,Allone,Alltwo) :
   numelm=(len(Allone)+len(Alltwo))
   randnum=np.random.randint(low=0,high=numelm)
   if randnum < len(Alltwo) :
      elm=Alltwo[randnum]
   else :
      elm=Allone[randnum-len(Alltwo)]

   gates=elm.split('+')
   if len(gates) == 8 :
      Apply_ACE(gates[0],state,site2)
      Apply_BD(gates[1],state,site1,site2)
      Apply_ACE(gates[2],state,site1)

      Apply_BD(gates[3],state,site1,site2)
      Apply_ACE(gates[4],state,site2)

      Apply_ACE(gates[5],state,site1)
      Apply_ACE(gates[6],state,site1)

      Apply_ACE(gates[7],state,site2)

   elif len(gates) == 7 :
      Apply_ACE(gates[0],state,site1)
      Apply_ACE(gates[1],state,site1)

      Apply_BD(gates[2],state,site1,site2)
      Apply_ACE(gates[3],state,site2)

      Apply_ACE(gates[4],state,site1)
      Apply_ACE(gates[5],state,site1)

      Apply_ACE(gates[6],state,site2)

def get_all_elms() :
   A=['I','H','HPH']

   B=['H1CH0H1CH0H1C','C0H1C','H0P0CH0H1C','H0CH0H1C']

   C=['I','HPPH']

   D=['CH0H1CH0H1CH1','H0CH0H1CH1','H0H1P1CH0H1CH1','H0H1CH0H1CH1']

   E=['I','P','PP','PPP']


   Alltwo=[]
   for Aelm in A :
      for Belm in B :
         for Celm in C :
            for Delm in D:
               for Eelm in E:
                  for Aelm1 in A :
                        for Celm1 in C :
                           for Eelm1 in E:
                              if (Belm == 'H1CH0H1CH0H1C') and (Delm == 'CH0H1CH0H1CH1') :
                                 Allone.append(Aelm+"+"+Belm+"+"+Celm+"+"+Delm+"+"+Eelm+"+"+Aelm1+"+"+Celm1+"+"+Eelm1)
                              else :
                                 Alltwo.append(Aelm+"+"+Belm+"+"+Celm+"+"+Delm+"+"+Eelm+"+"+Aelm1+"+"+Celm1+"+"+Eelm1)

   Allone=[]
   for Aelm in A :
         for Celm in C :
            for Delm in D:
               for Eelm in E:
                  for Aelm1 in A :
                        for Celm1 in C :
                           for Eelm1 in E:
                              Allone.append(Aelm+"+"+Celm+"+"+Delm+"+"+Eelm+"+"+Aelm1+"+"+Celm1+"+"+Eelm1)

   return Allone,Alltwo

def get_bin_rank(state) :
   mat=np.array(state.copy(),dtype=bool)
   nrows=mat.shape(0)
   ncols=mat.shape(1)
   rank=0

   for k in range(ncols) :
      i=-1
      for r in range(rank,nrows):
         if ( not mat[r,k] ) :
            continue
         if(i==-1) :
            i=r;
            rank+=1;
         else :
            mat[r,:] = np.not_equal(mat[r,:],mat[i,:])

      if( i>= rank) :
         a=mat[i,:].copy()
         mat[i,:] = mat[rank-1,:].copy()
         mat[rank-1,:] = a.copy()
   return rank;


def get_e_sub(state,Length,index,verbose) :
   if verbose :
      print('index',index,flush=True)

   esnow=[]
   for boundary in range(1,Length) :
      ent=0
      statenow=np.array(state[Length:-1,:-1].copy(),dtype=int)
      statenow=np.c_[statenow[:,:boundary],statenow[:,Length:Length+boundary]]
      ent = get_bin_rank(statenow)
      ent-= boundary
      esnow.append(ent)

   return np.array(esnow)

def g(x1,z1,x2,z2) :
   if (x1==z1) and (x1==0) :
      return 0
   elif (x1==z1) and (x1==1) :
      return 1*z2-1*x2
   elif (x1==1) and (z1 == 0) :
      return z2*(2*x2-1)
   elif (x1==0) and (z1 == 1) :
      return x2*(1-2*z2)
   else :
      print("error: g is not definfed!")

def rowsum(state,p,q) :
   Length=state.shape[0]//2
   tot=2*state[p,Length*2]+2*state[q,Length*2]

   for j in range(Length) :
      tot += g(state[q,j],state[q,Length+j],state[p,j],state[p,Length+j])
   if tot%4 == 0 :
      state[p,Length*2] = 0
   elif tot%4 == 2 :
      state[p,Length*2] = 1

   state[p,:Length]  =np.logical_xor(state[q,:Length],state[p,:Length])
   state[p,Length:-1]=np.logical_xor(state[q,Length:-1],state[p,Length:-1])

def apply_CNOT(state,site1,site2) :
   Length=state.shape[0]//2
   if site1 >= Length or site2 >= Length :
      print('error: site1 >= Length or site2 >= Length')

   state[:,-1] = np.logical_xor(state[:,-1], \
         np.logical_and(np.logical_and(state[:,site1],state[:,Length+site2]),\
         np.logical_or(np.logical_xor(state[:,site2],state[:,Length+site1]), \
         np.logical_xor(state[:,Length+site1],np.ones(state[:,Length+site1].shape))\
         )\
         )\
         )

   state[:,site2] = np.logical_xor(state[:,site2],state[:,site1])
   state[:,Length+site1] = np.logical_xor(state[:,Length+site1],state[:,Length+site2])

def apply_Hadamard(state,site) :
   Length=state.shape[0]//2

   if site >= Length :
      print('error: site >= Length')
   state[:,-1] = np.logical_xor(state[:,-1],np.logical_and(state[:,site],state[:,Length+site]))
   state[:,[site,Length+site]]=state[:,[Length+site,site]]

def apply_PhaseGate(state,site) :
   Length=state.shape[0]//2

   if site >= Length :
      print('error: site >= Length')

   state[:,-1] = np.logical_xor(state[:,-1],np.logical_and(state[:,site],state[:,Length+site]))
   state[:,Length+site]=np.logical_xor(state[:,Length+site],state[:,site])

def measure(state,site,r) :
   Length=state.shape[0]//2
   if site >= Length :
      print('error: site >= Length')

   ps=np.squeeze(np.nonzero(np.array(state[Length:-1,site],dtype=int))) + Length
   if ps.size > 0 :
      if ps.size == 1:
         p=ps
      else :
         p=ps[0]

      pkaris=np.squeeze(np.nonzero(np.array(state[:-1,site],dtype=int)))
      if pkaris.size > 1 :
         for i in pkaris :
            if i != p :
               rowsum(state,i,p)
      elif pkaris.size > 0 :
         if pkaris != p :
            rowsum(state,pkaris,p)

      state[p-Length,:] = state[p,:]
      state[p,:] = np.zeros(state[p,:].shape) 
      state[p,Length+site] = 1

      if r < 0 :
         if np.random.uniform() < 0.5 :
            state[p,-1] = 0
         else :
            state[p,-1] = 1
      else :
         state[p,-1] = bool(r)

      return state[p,-1]

   else :
      state[2*Length,:] = np.zeros(state[2*Length,:].shape)
      psds=np.squeeze(np.nonzero(np.array(state[:Length,site],dtype=int)))
      if psds.size > 1  :
         for i in psds :
            rowsum(state,2*Length,i+Length)
      elif psds.size > 0 :
         rowsum(state,2*Length,psds+Length)

      if r < 0 :
         pass
      else :
         state[2*Length,-1] = bool(r)

      return state[2*Length,-1]

def apply_Pmeasure(state,site) :
   Length=state.shape[0]//2
   if site >= Length :
      print('error: site >= Length')

   ps=np.squeeze(np.nonzero(np.array(state[Length:-1,site],dtype=int))) + Length
   if ps.size > 0 :
      #while ps.size > 0 :
      if ps.size == 1:
         p=ps
      else :
         p=ps[0]

      pkaris=np.squeeze(np.nonzero(np.array(state[:-1,site],dtype=int)))
      if pkaris.size > 1 :
         for i in pkaris :
            if i != p :
               rowsum(state,i,p)

      elif pkaris.size > 0 :
         if pkaris != p :
            print('applying rowsum')
            rowsum(state,pkaris,p)

      state[p-Length,:] = state[p,:]
      state[p,:] = np.zeros(state[p,:].shape) 
      state[p,Length+site] = 1

      if np.random.uniform() < 0.5 :
         state[p,-1] = 0
      else :
         state[p,-1] = 1

      #ps=np.squeeze(np.nonzero(np.array(state[Length:-1,site],dtype=int)))
   else :
      state[2*Length,:] = np.zeros(state[2*Length,:].shape)
      psds=np.squeeze(np.nonzero(np.array(state[:Length,site],dtype=int))) 
      if psds.size > 1  :
         for i in psds :
            rowsum(state,2*Length,i+Length)
      elif psds.size == 1 :
         rowsum(state,2*Length,psds+Length)

def apply_projector(state,site1,site2,r1,r2) :
   measure(state,site1,r1)
   measure(state,site2,r2)

def Pmeasure(state,site1,site2) :
   Length=state.shape[0]//2
   if site1 >= Length :
      print('error: site >= Length')

   ps=np.squeeze(np.nonzero(np.array(state[Length:-1,site1],dtype=int)))
   if ps.size > 0 :
      #up or down
      szsz=[0]
   else :
      state_=state.copy()
      #up or down
      state_[2*Length,:] = np.zeros(state_[2*Length,:].shape)
      psds=np.squeeze(np.nonzero(np.array(state_[:Length,site1],dtype=int)))
      if psds.size > 1  :
         for i in psds :
            rowsum(state_,2*Length,i+Length)
      elif psds.size > 0 :
         rowsum(state_.copy(),2*Length,psds+Length)
      if state_[2*Length,-1] == 1 :
         szsz=[0.5]
      else :
         szsz=[-0.5]

   Length=state.shape[0]//2
   if site2 >= Length :
      print('error: site >= Length')

   ps=np.squeeze(np.nonzero(np.array(state[Length:-1,site2],dtype=int)))
   if ps.size > 0 :
      #up or down
      szsz.append(0)
   else :
      state_=state.copy()
      #up or down
      state_[2*Length,:] = np.zeros(state_[2*Length,:].shape)
      psds=np.squeeze(np.nonzero(np.array(state_[:Length,site2],dtype=int)))
      if psds.size > 1  :
         for i in psds :
            rowsum(state_,2*Length,i+Length)
      elif psds.size > 0 :
         rowsum(state_,2*Length,psds+Length)
      if state_[2*Length,-1] == 1 :
         szsz.append(0.5)
      else :
         szsz.append(-0.5)

   ps=np.array([0.,0.,0.,0.])
   if szsz[0] != 0 :
      if szsz[0] > 0 :
         ps[0:2]=np.array([1,1])
      else :
         ps[2:4]=np.array([1,1])
   else :
      ps=np.array([0.5,0.5,0.5,0.5])

   if szsz[1] != 0 :
      if szsz[1] > 0 :
         ps[[0,2]] *= np.array([1.,1.])
         ps[[1,3]] *= np.array([0.,0.])
      else :
         ps[[0,2]] *= np.array([0.,0.])
         ps[[1,3]] *= np.array([1.,1.])
   else :
      ps *= np.array([0.5,0.5,0.5,0.5])

   if np.squeeze(np.nonzero(ps)).size == 0 :
      print("error: all probability is zero")
   else :
      op=np.random.choice([0,1,2,3],1,p=ps)
      if op==1 :
         apply_projector(state,site1,site2,1,1)
      if op==2 :
         apply_projector(state,site1,site2,1,0)
      if op==3 :
         apply_projector(state,site1,site2,0,1)
      if op==4 :
         apply_projector(state,site1,site2,0,0)
   
class EvolveClifford :
   def __init__(self,Length,init='Z',seed=0):
      self.Allone,self.Alltwo=get_all_elms()
      self.Length=Length

      self.init_state=np.zeros((int(2*self.Length+1),int(self.Length*2+1)),dtype=bool)
      self.init_state[:self.Length,:self.Length]=np.eye(int(self.Length),dtype=bool)
      self.init_state[self.Length:-1,self.Length:-1]=np.eye(int(self.Length),dtype=bool)

      self.seed=seed

      # start from X-polarized
      if init=='X' or init=='Y' :
         print('initial state is',init)
         print('applying Hadamard gate')

         for i in range(self.Length) :
            apply_Hadamard(self.init_state,i)

         if init == 'Y' :
            print('initial state is',init)
            print('applying phase gate')
            for i in range(self.Length) :
               apply_PhaseGate(self.init_state,i)

      # Checks 
      if (self.init_state.shape[1] != self.Length*2+1 ) :
         print("Error: Dimension of the initial state is wrong ",self.init_state.shape)

      np.random.seed(self.seed)

      # c.f. 
      # S. Aaronson and D. Gottesman, Phys. Rev. A - At. Mol. Opt. Phys. 70, (2004).

      # gate type
      # 0 : Hadamard 
      # 1 : Phase 

      # 2 : CNOT (L)
      # 3 : CNOT (R) 

      self.numgatetypes=4 # for now :)
      self.is_initialized=True

   def get_sattime(self) :
      return self.times[-1]

   def get_time(self) :
      return self.times

   def get_be(self) :
      return self.bes

   def get_entropy(self,numcpu,verbose=0) :
      self.es=np.zeros((len(self.times),self.Length-1))
      self.verbose=verbose

      future_list = []
      start_time=time.time()

      with futures.ProcessPoolExecutor(max_workers=numcpu) as executor:
         for i in range(len(self.times)) :
            future = executor.submit(get_e_sub,self.states[i,:].copy(),self.Length,i,self.verbose)
            future_list.append([future,i])
         _ = futures.as_completed(fs=future_list)
      executor.shutdown(wait=True)

      for elm in future_list :
         self.es[elm[1],:] = elm[0].result()

      end_time=time.time() - start_time
      print(end_time/float(self.numtimesteps),'seconds per loop indices')
      return np.array(self.es),self.times


   def get_states(self) :
      return self.states
