import numpy as np
import os
import sys
import itertools

def get_BetheLatticeLength(cn,numlayers) :
   coordination_number=cn
   num_shells=numlayers

   tot_sites=1
   numsites_prev=cn
   tot_sites+=numsites_prev
   for i_ind in range(2,num_shells+1) :
      numsites_prev=numsites_prev*(coordination_number-1);
      tot_sites+=numsites_prev;

   return tot_sites;

def get_BetheLatticeBranch(cn,numlayers,branch_tag='1') :
   coordination_number=cn
   num_shells=numlayers

   sites=[]
   site_labels=[]
   for ind in range(coordination_number+1) :
      sites.append(ind)
      site_labels.append(str(ind))
   
   tot_sites=1+coordination_number
   numsites_prev=coordination_number;

   for i_ind in range(2,num_shells+1) :
      for j_ind in range(numsites_prev) :
         for k_ind in range(coordination_number-1) :
            currsite=tot_sites+j_ind*(coordination_number-1)+k_ind
            sites.append(currsite)
            site_labels.append(site_labels[tot_sites-numsites_prev+j_ind]+str(k_ind))

      numsites_prev=numsites_prev*(coordination_number-1)
      tot_sites+=numsites_prev

   out_sub=[]
   for ind,site in enumerate(sites) :
      if site_labels[ind][:len(branch_tag)] == branch_tag :
         out_sub.append(site)

   return np.array(out_sub),tot_sites

def make_symplectic_prod_mat(L) :
   out=np.zeros((2*L,2*L),dtype=int)
   out[:L,L:]=np.eye(L,dtype=int)
   out[L:,:L]=-np.eye(L,dtype=int)
   return out

def get_pauli_str(stab) :
   if (stab[0]+stab[1]) == 0 :
      return "I"
   elif (stab[0]+stab[1]) == 2 :
      return "Y"
   elif stab[0] == 1 :
      return "X"
   elif stab[1] == 1 : 
      return "Z"


def state_to_str(state) :
   L=state.shape[1]//2
   out=[] 

   for ii in range(state.shape[0]) :
      print(ii)
      print(state[ii,:])
      print(state[ii,0],state[ii,L+0])
      out_str=""
      for jj in range(L) :
         out_str+=get_pauli_str(state[ii,[jj,L+jj]])
      out.append(out_str)
   return out


def get_rank(mat_in,verbose=False) :
   mat=mat_in.copy()
   nrows=mat.shape[0]
   ncols=mat.shape[1]
   rank=0;

   for k in range(ncols) :
      i=-1;
      r=rank
      for r in range(rank,nrows) :
         if( not mat[r,k] ) :
            continue;
         if(i==-1) :
            i=r
            rank+=1
         else :
            for j in range(ncols) :
               mat[r,j] = (mat[r,j] != mat[i,j]);

            if (verbose) :
               print(mat)
      if( i>= rank) :
         mat[[i,rank-1],:]=mat[[rank-1,i],:]
         if (verbose) :
            print(mat)
   return rank;

def ge_gf2(mat_in,verbose=False) :
   mat=mat_in.copy()
   nrows=mat.shape[0]
   ncols=mat.shape[1]

   rank=0
   for row in range(nrows) :
      if np.sum(mat[row,:]) == 0 :
         for j in range(row+1,nrows) :
            if np.sum(mat[j,:]) != 0 :
               mat[[row,j],:]=mat[[j,row],:]
               break

   currrow=0
   currcol=0
   while (currrow < nrows) and (currcol < ncols) :
      if not mat[currrow,currcol] :
         found=False
         for j in range(currrow+1,nrows) :
            if mat[j,currcol] :
               mat[[currrow,j],:]=mat[[j,currrow],:]
               found=True
               break

      else :
         found=True

      if found :
         for j in range(currrow+1,nrows) :
            if mat[j,currcol] :
               mat[j,:]=np.logical_xor(mat[j,:],mat[currrow,:])

         currrow+=1

      currcol+=1 
   return mat,currrow

def get_symplectic(mat_in) :
   mat,rank=ge_gf2(mat_in)
   mat=mat[:rank,:]

   nrows=mat.shape[0]
   ncols=mat.shape[1]
   L=int(ncols/2)

   for r in range(nrows) :
      if not mat[r,r] :
         for c in range(r+1,L) :
            if mat[r,c] :
               mat[:,[r,c]]=mat[:,[c,r]]
               mat[:,[L+r,L+c]]=mat[:,[L+c,L+r]]

               break

   km1=0
   for c in range(L) : 
      if mat[c,c] :
         km1=c
         for rr in range(c) :
            if mat[rr,c] :
               mat[rr,:] = mat[rr,:] != mat[c,:]

   k=km1+1
   mat=mat[:k,:]
   return mat,k

def get_symplectic_2(mat_in) :
   mat,rank=ge_gf2(mat_in)
   mat=mat[:rank,:]

   nrows=mat.shape[0]
   ncols=mat.shape[1]
   L=int(ncols/2)

   for r in range(nrows) :
      found=True
      if not mat[r,r] :
         found=False
         for c in range(r+1,L) :
            if mat[r,c] :
               mat[:,[r,c]]=mat[:,[c,r]]
               mat[:,[L+r,L+c]]=mat[:,[L+c,L+r]]
               found=True
               break
      if not found :
         apply_Hadamard(mat,r)
         mat,k=get_symplectic_2(mat)

   km1=0
   for c in range(L) : 
      if mat[c,c] :
         km1=c
         for rr in range(c) :
            if mat[rr,c] :
               mat[rr,:] = mat[rr,:] != mat[c,:]
   k=km1+1
   return mat,k

def apply_PhaseGate(state,site) :
   Length=state.shape[1]//2
   if site >= Length :
      print('error: site >= Length')

   state[:,Length+site]=np.logical_xor(state[:,Length+site],state[:,site])

def apply_Hadamard(state,site) :
   Length=state.shape[1]//2
   if site >= Length :
      print('error: site >= Length')

   state[:,[site,Length+site]]=state[:,[Length+site,site]]


def get_min_symplectic_weight(mat_in) :
   nrows=mat_in.shape[0]
   ncols=mat_in.shape[1]
   L=int(ncols/2)
   minsd=np.Inf
   for i in range(nrows) :
      if np.sum(mat_in[i,:]) != 0  :
         minsd=min(sum(mat_in[i,:L] | mat_in[i,L:]),minsd)

   return minsd

def gf_2_inner(mat1,mat2) :
   dim1_1=mat1.shape[0]
   dim2_1=mat1.shape[1]

   dim1_2=mat2.shape[0]
   dim2_2=mat2.shape[1]

   out = np.zeros(dim1_1,dim2_2)
   for i in range(dim1_1) :
      for j in range(dim2_2) :
         out[i,j] = np.sum(mat1[i,:]*mat2[:,j])%2 

   return out 

def get_null_gf2(A,g,invg) :
   dim0=A.shape[0]
   dim1=A.shape[1]
   N=0;
   return np.array([0,0])

def get_subsys_full(mat,sub) :
   sub=np.array(sub,dtype=int)
   out=np.hstack((mat[:,sub],mat[:,np.array(int(mat.shape[0]//2)+sub,dtype=int)]))
   return out 

def get_subsys_stab(mat,sub) :
   sub=np.array(sub,dtype=int)
   out=np.hstack((mat[:,sub],mat[:,np.array(int(mat.shape[0])+sub,dtype=int)]))
   return out 

def mutual_info(matA,matB) :
   LA=matA.shape[1]//2
   LB=matB.shape[1]//2
   mat=np.hstack((matA,matB))
   out=get_rank(matA)+get_rank(matB)-get_rank(mat)
   return out

def get_entropy(state,subr) :
   subr=np.array(list(set(subr)),dtype=int)
   return get_rank(np.hstack((state[:,subr],state[:,int(state.shape[0])+subr])))-len(subr)

def g(x1,z1,x2,z2) :
   if (x1==z1) and (x1==0) :
      return 0
   elif (x1==z1) and (x1==1) :
      return 1*z2-1*x2
   elif (x1==1) and (z1 == 0) :
      return z2*(2*x2-1)
   elif (x1==0) and (z1 == 1) :
      return x2*(1-2*z2)
   else :
      print("error: g is not definfed!")

def rowsum(state,p,q) :
   Length=state.shape[0]//2
   tot=2*state[p,Length*2]+2*state[q,Length*2]

   for j in range(Length) :
      tot += g(state[q,j],state[q,Length+j],state[p,j],state[p,Length+j])
   if tot%4 == 0 :
      state[p,Length*2] = 0
   elif tot%4 == 2 :
      state[p,Length*2] = 1

   state[p,:Length]  =np.logical_xor(state[q,:Length],state[p,:Length])
   state[p,Length:-1]=np.logical_xor(state[q,Length:-1],state[p,Length:-1])

def rowsum_np(state,p,q) : #no phase
   Length=state.shape[0]//2
   state[p,:Length] =np.logical_xor(state[q,:Length],state[p,:Length])
   state[p,Length:] =np.logical_xor(state[q,Length:],state[p,Length:])

def apply_Pmeasure(state,site) :
   Length=state.shape[0]//2
   print(state.shape)
   if site >= Length :
      print('error: site >= Length')

   ps=np.squeeze(np.nonzero(np.array(state[Length:-1,site],dtype=int))) + Length
   if ps.size > 0 :
      #while ps.size > 0 :
      if ps.size == 1:
         p=ps
      else :
         p=ps[0]

      pkaris=np.squeeze(np.nonzero(np.array(state[:-1,site],dtype=int)))
      if pkaris.size > 1 :
         for i in pkaris :
            if i != p :
               rowsum(state,i,p)

      elif pkaris.size > 0 :
         if pkaris != p :
            print('applying rowsum')
            rowsum(state,pkaris,p)

      state[p-Length,:] = state[p,:]
      state[p,:] = np.zeros(state[p,:].shape) 
      state[p,Length+site] = 1

      if np.random.uniform() < 0.5 :
         state[p,-1] = 0
      else :
         state[p,-1] = 1

      #ps=np.squeeze(np.nonzero(np.array(state[Length:-1,site],dtype=int)))
   else :
      state[2*Length,:] = np.zeros(state[2*Length,:].shape)
      psds=np.squeeze(np.nonzero(np.array(state[:Length,site],dtype=int))) 
      if psds.size > 1  :
         for i in psds :
            rowsum(state,2*Length,i+Length)
      elif psds.size == 1 :
         rowsum(state,2*Length,psds+Length)

def apply_Pmeasure_np(state,site) : #no phase
   Length=state.shape[0]//2
   if site >= Length :
      print('error: site >= Length')

   ps=np.squeeze(np.nonzero(np.array(state[Length:,site],dtype=int))) + Length
   if ps.size > 0 :
      #while ps.size > 0 :
      if ps.size == 1:
         p=ps
      else :
         p=ps[0]

      pkaris=np.squeeze(np.nonzero(np.array(state[:,site],dtype=int)))
      if pkaris.size > 1 :
         for i in pkaris :
            if i != p :
               rowsum_np(state,i,p)

      elif pkaris.size > 0 :
         if pkaris != p :
            print('applying rowsum')
            rowsum_np(state,pkaris,p)

      state[p-Length,:] = state[p,:]
      state[p,:] = np.zeros(state[p,:].shape) 
      state[p,Length+site] = 1

   else :
      pass
