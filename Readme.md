# Clifford Circuit Toolkit: C++ Implementation for Simulating Clifford Circuits

The software implements numerical simulation of a set of quantum states called stabilizer states. 

Stabilizer states are simultaneous eigenstate of Pauli strings. 
The Clifford gates, on the other hand map Pauli strings to other Pauli strings. 
The evolution of these states with Clifford gates can be efficiently simulated with a classical computer in polynomial time,
while capturing the quantum features such as entanglement. 
The detailed construction is given by Aaronson and Gottesman (2004). 

This toolkit can:
* Simulate Clifford circuit with or without measurements.
* Generate random two-site gates from Clifford group.
* Calculate entanglement entropy.
* Perform teleportation experiments (Hayden and Preskill, Yoshida and Yao)
* Perform entanglement entropy calculations in 2-adic (Monna mapped) geometry (Gubser et al.).

## Cite 
Please cite the following:

The software is based on:
* S. Aaronson and D. Gottesman. (2004). [Phys. Rev. A 70, 052328](https://doi.org/10.1103/PhysRevA.70.052328).

Algorithm for generating random Clifford gates is based on
* Peter Selinger. (2015). [Logical Methods in Computer Science, Volume 11, Issue 2 (June 19, 2015) lmcs:1570](https://doi.org/10.2168/LMCS-11%282%3A10%292015).

Entropy Calculation is based on:
* Michael J. Gullans and David A. Huse. (2020). [Phys. Rev. X 10, 041020](https://doi.org/10.1103/PhysRevX.10.041020). 
* Adam Nahum, Jonathan Ruhman, Sagar Vijay, and Jeongwan Haah. (2017). [Phys. Rev. X 7, 031016](https://doi.org/10.1103/PhysRevX.7.031016).

Yoshida-Yao Teleportation protocol (`EvolveClifford_YY.h`) is based on:
* Beni Yoshida and Norman Y. Yao. (2018). [Phys. Rev. X 9, 011006](https://doi.org/10.1103/PhysRevX.9.011006).

Hayden and Preskill experiment is based on:
* Patrick Hayden and John Preskill. (2007). [JHEP 0709:120,2007](https://doi.org/10.1088/1126-6708/2007/09/120).
* Efficient decoding for the Hayden-Preskill protocol. Beni Yoshida and Alexei Kitaev. (2017). [arXiv:1710.03363](https://doi.org/10.48550/arXiv.1710.03363).

2-adic geometry (Monna map):
* Steven S. Gubser, Christian Jepsen, Ziming Ji, Brian Trundy. (2018). 	[Phys. Rev. D 98, 045009](https://doi.org/10.1103/PhysRevD.98.045009). 

Cite this software as:
* Tomohiro Hashizume. (2022). Clifford Circuit Toolkit 0.1. [https://doi.org/10.5281/zenodo.7245605](https://doi.org/10.5281/zenodo.7245605).

Please include the date visited. 

Results obtained from this his and older versions of the software are Published in:
* Tomohiro Hashizume, Gregory Bentsen, Sebastian Weber, Andrew J. Daley. (2021). [Phys. Rev. Lett. 126, 200603](https://doi.org/10.1103/PhysRevLett.126.200603).
* Tomohiro Hashizume, Gregory Bentsen, Andrew J. Daley. (2022). [Phys. Rev. Research 4, 013174](https://doi.org/10.1103/PhysRevResearch.4.013174).
* Tomohiro Hashizume, Sridevi Kuriyattil, Andrew J. Daley, Gregory Bentsen. (2022). [Symmetry 2022, 14(4), 666](https://doi.org/10.3390/sym14040666).

## TODO

Complete this read me...

## Usage

For `C++`, go to `testscripts` and execute `make` and try running `.sh` files for results.

For `python`, `debug` contains simulation files for random unitary circuits. 

See `Sample_Job` for the examples.

## Contact

* Tomohiro Hashizume (original developer) tomohiro.hashizume.overseas@gmail.com

## License

MIT License

Copyright (c) [2022] [Tomohiro Hashizume]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Project status
Ongoing...
